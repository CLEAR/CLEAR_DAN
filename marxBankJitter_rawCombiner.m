function marxBankJitter_rawCombiner(datasets_id, datasets_legend, datasets_colors, datasets_transparency, curr_ylim,time_xlim, datasets_removeSteps, curr_ylim_zoom,time_xlim_zoom, t_beam, smoothsteps)
    %MARXBANKJITTER_RAWCOMBINER Plot current traces for multiple datasets as one
    %   Detailed explanation goes here

    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);

    scopename_in ='DIS_CUR_IN';
    scopename_out='DIS_CUR_OUT';

    beam_off_current_limit = 400; % (A)

    % Figure 1: curr_in
    figure(1)
    clf()
    hold on;
    ax11 = gca();
    set(ax11,'TickLabelInterpreter', 'latex')
    %box(ax11,'on');
    ax12 = axes('Position',[.55 .55 .35 .35]);
    hold on;
    set(ax12,'TickLabelInterpreter', 'latex')
    box(ax12,'on');
    
    %Figure 2: curr_out
    figure(2)
    clf()
    hold on
    ax21 = gca();
    set(ax21,'TickLabelInterpreter', 'latex')
    %box(ax21,'on');
    ax22 = axes('Position',[.55 .55 .35 .35]);
    hold on;
    set(ax22,'TickLabelInterpreter', 'latex')
    box(ax22,'on');
    
    if t_beam < 0
        t_shift = -t_beam;
        t_beam  = -t_beam;
    else
        t_shift = 0;
        t_beam  = t_beam;
    end

    % Loop over datasets
    for dIdx = 1:length(datasets_id)
        % load the dataset
        [dataset, ~] = getDataset(datasets_id(dIdx));
        datasetInfo(dataset);
        
        
        num_in  = size(dataset.scopes.(scopename_in).dat,2);
        num_out = size(dataset.scopes.(scopename_out).dat,2);
        assert (num_in == num_out)
        
        steps   = unique(dataset.scalars.step_num.dat);
        offsets = unique(dataset.scalars.step_value.dat);
        
        %keepPoints = ones(1,length(dataset.scalars.step_num.dat));
        keepPoints = ones(1,num_in);
        for j = 1:numel(steps)
            step_num = steps(j);
            if ismember(j,datasets_removeSteps{dIdx})
                fprintf('Skipping step # %i, position = %g\n', j, offsets(j))
                
                keepPoints(dataset.scalars.step_num.dat == step_num) = 0;
                continue
            end
        end
        max_in  = [];
        max_out = [];
        max_in_tb  = [];
        max_out_tb = [];
        %Raw data
        counter        = 0;
        counterPlotIn  = 0;
        counterPlotOut = 0;
        for i=1:num_in
            if ~keepPoints(i)
                continue
            end
            counter = counter+1;
            
            % Load the data
            curr_in  = dataset.scopes.(scopename_in).dat{i};
            time_in  = dataset.scopes.(scopename_in).time_ns{i};
            curr_out = dataset.scopes.(scopename_out).dat{i};
            time_out = dataset.scopes.(scopename_out).time_ns{i};
            
            if max(curr_in) < beam_off_current_limit
                fprintf('curr_in killed by limit, I=%f, step=%i, i=%i\n', max(curr_in), dataset.scalars.step_num.dat(i), i)
            else
                h_in   = plot(ax11, time_in-t_shift, curr_in,  'Color', datasets_colors{dIdx});
                h_in.Color(4) = datasets_transparency;
                
                h_inz  = plot(ax12, time_in-t_shift, curr_in,  'Color', datasets_colors{dIdx});
                h_inz.Color(4) = datasets_transparency;
                
                counterPlotIn = counterPlotIn+1;
            end
            
            if max(curr_out) < beam_off_current_limit
                fprintf('curr_out killed by limit, I=%f, step=%i, i=%i\n', max(curr_out), dataset.scalars.step_num.dat(i), i)
            else
                h_out  = plot(ax21, time_out-t_shift,curr_out, 'Color', datasets_colors{dIdx});
                h_out.Color(4) = datasets_transparency;
                
                h_outz = plot(ax22, time_out-t_shift, curr_out,  'Color', datasets_colors{dIdx});
                h_outz.Color(4) = datasets_transparency;
                
                 counterPlotOut = counterPlotOut+1;
            end
            
            %Tiny bit of statistics
            max_in (counter) = max(curr_in);
            max_out(counter) = max(curr_out);
            
            idx_tb = find(time_in >= t_beam,1);
            max_in_tb(counter)  = curr_in(idx_tb);
            max_out_tb(counter) = curr_out(idx_tb);
        end
        disp(['Data for: ', datasets_legend{dIdx}])
        disp(['max_in     = ', num2str(mean(max_in)),     ' (A), std = ', num2str(std(max_in))]);
        disp(['max_out    = ', num2str(mean(max_out)),    ' (A), std = ', num2str(std(max_out))]);
        disp(['max_in_tb  = ', num2str(mean(max_in_tb)),  ' (A), std = ', num2str(std(max_in_tb))]);
        disp(['max_out_tb = ', num2str(mean(max_out_tb)), ' (A), std = ', num2str(std(max_out_tb))]);
        disp(['Nshots     = ', num2str(counter)]);
        disp(['NshotsIn   = ', num2str(counterPlotIn), ' (plotted)']);
        disp(['NshotsOut  = ', num2str(counterPlotOut), ' (plotted)']);
        disp(' ')

        %Smoothed
        if ~isnan(smoothsteps)
            for i=1:num_in
                if ~keepPoints(i)
                    continue
                end

                % Load the data
                curr_in  = smooth( dataset.scopes.(scopename_in).dat{i},   smoothsteps );
                time_in  = dataset.scopes.(scopename_in).time_ns{i};
                curr_out = smooth( dataset.scopes.(scopename_out).dat{i},  smoothsteps);
                time_out = dataset.scopes.(scopename_out).time_ns{i};

                if max(curr_in) < beam_off_current_limit
                    fprintf('curr_in killed by limit, I=%f, step=%i, i=%i\n', max(curr_in), dataset.scalars.step_num.dat(i), i)
                else
                    h_in   = plot(ax11, time_in-t_shift, curr_in,  'Color', 1-datasets_colors{dIdx});
                    %h_in.Color(4) = datasets_transparency;

                    h_inz  = plot(ax12, time_in-t_shift, curr_in,  'Color', 1-datasets_colors{dIdx});
                    %h_inz.Color(4) = datasets_transparency;
                end

                if max(curr_out) < beam_off_current_limit
                    fprintf('curr_out killed by limit, I=%f, step=%i, i=%i\n', max(curr_out), dataset.scalars.step_num.dat(i), i)
                else
                    h_out  = plot(ax21, time_out-t_shift,curr_out, 'Color', 1-datasets_colors{dIdx});
                    h_out.Color(4) = datasets_transparency;

                    h_outz = plot(ax22, time_out-t_shift, curr_out,  'Color', 1-datasets_colors{dIdx});
                    h_outz.Color(4) = datasets_transparency;
                end

            end
        end
        
        disp(' ')
    end
    
    figure(1)

    xlim(ax11, time_xlim-t_shift)
    ylim(ax11, curr_ylim)
    if t_shift == 0
        xlabel(ax11, 'Time (ns)', 'Interpreter', 'latex')
    else
        xlabel(ax11, 'Time relative to $t_{beam}$ (ns)', 'Interpreter', 'latex')
    end
    ylabel(ax11, '$I_{in}(t)$ (A)', 'Interpreter', 'latex')
    
    %plot(ax11, [t_beam-t_shift, t_beam-t_shift], curr_ylim_zoom, 'k:')
    xline(ax11,t_beam-t_shift, 'k--')
    xline(ax12,t_beam-t_shift, 'k--')
    
    rectangle(ax11,'Position',[time_xlim_zoom(1)-t_shift, curr_ylim_zoom(1),...
                               time_xlim_zoom(2)-time_xlim_zoom(1), curr_ylim_zoom(2)-curr_ylim_zoom(1)], ...
                   'LineStyle', ':')
    
    addLegend(datasets_colors,datasets_legend, ~isnan(smoothsteps))
    
    xlim(ax12, time_xlim_zoom-t_shift)
    ylim(ax12, curr_ylim_zoom)
    %xlabel(ax12, 'Time (ns)')
    %ylabel(ax12, 'I_{in}(t) (A)')

    figure(2)
    
    xlim(ax21, time_xlim-t_shift)
    ylim(ax21, curr_ylim)
    if t_shift == 0
        xlabel(ax21, 'Time (ns)', 'Interpreter', 'latex')
    else
        xlabel(ax21, 'Time relative to $t_{beam}$ (ns)', 'Interpreter', 'latex')
    end
    ylabel(ax21, '$I_{out}(t)$ (A)', 'Interpreter', 'latex')
    
    %plot(ax21, [t_beam-t_shift, t_beam-t_shift], curr_ylim_zoom, 'k:')
    xline(ax21,t_beam-t_shift, 'k--')
    xline(ax22,t_beam-t_shift, 'k--')

    rectangle(ax21,'Position',[time_xlim_zoom(1)-t_shift, curr_ylim_zoom(1),...
                               time_xlim_zoom(2)-time_xlim_zoom(1), curr_ylim_zoom(2)-curr_ylim_zoom(1)], ...
                   'LineStyle', ':')

    addLegend(datasets_colors,datasets_legend, ~isnan(smoothsteps))
    
    xlim(ax22, time_xlim_zoom-t_shift)
    ylim(ax22, curr_ylim_zoom)
    %xlabel(ax22, 'Time (ns)')
    %ylabel(ax22, 'I_{out}(t) (A)')
    
end

function addLegend(legCol, legText,doSmooth)
    xShift = 0.05;
    yShift = 0.0;
    for i = 1:length(legText)
        if doSmooth
            annotation('line', [0.25,0.27]+xShift,[0.2,0.2]+i*0.05+yShift,'Color', legCol{i})
            annotation('line', [0.275,0.3]+xShift,[0.2,0.2]+i*0.05+yShift,'Color', 1-legCol{i})
        else
            annotation('line', [0.25,0.3]+xShift, [0.2,0.2]+i*0.05+yShift,'Color', legCol{i})
        end
        annotation('textbox', [0.3+xShift,0.2-0.05/2+i*0.05+yShift,0.25,0.05], 'String', legText(i), 'Linestyle', 'none', 'Interpreter', 'latex')
    end

    i = length(legText)+1;
    annotation('line', [0.275,0.275]+xShift, [0.173,0.227]+0.05*i+yShift, 'Color', 'k', 'LineStyle', '--')
    annotation('textbox', [0.3+xShift,0.2-0.05/2+i*0.05+yShift,0.25,0.05], 'String', '$t_{beam}$', 'Linestyle', 'none', 'Interpreter', 'latex')
    
end