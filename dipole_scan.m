function [] = dipole_scan(dataset_id, screenID, visu)
    
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % get dataset 
    [dataset, dataset_id] = getDataset(dataset_id);
    nshots = dataset.metadata.numShots;
    
    % Try to autodetect screenID
    if ~exist('screenID','var')
        screenID=930;
        downstream=true;
    else
        if ~isnumeric(screenID)
            error('screenID must be integer number')
        end
        if ~(screenID==420 || screenID==440 || screenID==930)
            error('screenID must be 420, 440, or 930')
        end
        if screenID == 930
            downstream = true;
        else
            downstream = false;
        end
    end
    
    disp(['Using screenID = ', num2str(screenID), ', downstream = ',num2str(downstream)])
    
    %Default value for VISU
    if ~exist('visu','var')
        visu = false;
    end
    
    % prep figure for fitting routines
    figure(1);
    set(gcf, 'color', 'w');
    
    % loop shots
    [Is_dipole, muxs, sigxs] = deal(zeros(nshots, 1));
    for i = 1:nshots
        
        % get dipole current steps
        Is_dipole(i) = dataset.scalars.step_value.dat(i); % [A]
        
        % get spectrometer screen images
        [muxs(i), ~, sigxs(i), ~] = image_anal_SPC(dataset, i, visu, screenID);
        
    end
    
    
    %% STATISTICS
    prc = 95;
    clean = @(x) x(x > prctile(x, 100-prc) & x < prctile(x, prc));
    
    Is_uniq = unique(Is_dipole);
    [muxs_avg, muxs_std, sigxs_avg, sigxs_std] = deal(zeros(size(Is_uniq)));
    for i = 1:numel(Is_uniq)
        mask = Is_dipole == Is_uniq(i);
        
        step_muxs = muxs(mask & ~isnan(muxs));
        muxs_avg(i) = mean(clean(step_muxs));
        muxs_std(i) = std(clean(step_muxs));
        
        step_sigxs = sigxs(mask & ~isnan(sigxs));
        sigxs_avg(i) = mean(clean(step_sigxs));
        sigxs_std(i) = std(clean(step_sigxs));
        
    end
    
    
    %% TREND LINE
    
    % perform line fit
    px = polyfit(muxs_avg, Is_uniq, 1);
    I0 = px(2);
    dIdx = abs(px(1));
    
    
    
    SI_c = 299792458; %??[m/s]
    L_dip = 0.4711; % [m] effective dipole length at 60 A.
    if downstream
        theta0 = 22.3*pi/180; % [rad]
        B_per_I = 0.51457/60; % [T/A]
        L_arm = 1.163; % [m] for the energy-from-slope
    else
        theta0 = 17.5;
        %error('Not implemented')
    end
    
    % energy from zero crossing current
    L_dip_corr = L_dip*theta0/(2*sin(theta0/2)); % [m] corrected length (curved path inside dipole)
    E_zc = SI_c*L_dip_corr*B_per_I*I0/theta0; % [eV]
    
    disp(['Energy from zero crossing: ' num2str(E_zc/1e6) ' MeV']);
    
    % energy from slope
    E_sl = SI_c*L_dip_corr*B_per_I*L_arm*dIdx;
    
    disp(['Energy from slope: ' num2str(E_sl/1e6) ' MeV']);
    
    % trend line for plotting
    pI = polyfit(Is_uniq, muxs_avg, 1);
    
    
    %% PLOT
    
    % prep figure for fitting routines
    figure(2);
    set(gcf, 'color', 'w');
    
    subplot(2,1,1);
    plot(Is_dipole, muxs*1e3, 'o', 'Color', [1 1 1]*0.7)
    hold on;
    errorbar(Is_uniq, muxs_avg*1e3, muxs_std*1e3, 'k+', 'LineWidth', 1);
    plot(Is_uniq, (Is_uniq*pI(1) + pI(2))*1e3, 'r-');
    hold off;
    xlabel('Dipole current (A)');
    ylabel('Offset on screen (mm)');
    title(['Dipole scan beam offsets (dataset ' dataset_id ')']);
    legend('Measurement', 'Cleaned statistics', 'Trendline')
    
    % TODO: use angle to calibrate energy spread
    subplot(2,1,2);
    plot(Is_dipole, sigxs*1e3, 'o', 'Color', [1 1 1]*0.7)
    hold on;
    errorbar(Is_uniq, sigxs_avg*1e3, sigxs_std*1e3, 'k+', 'LineWidth', 1);
    hold off;
    xlabel('Dipole current (A)');
    ylabel('Beam size, rms (mm)');
    title(['Dipole scan energy spread, upper bound (dataset ' dataset_id ')']);
    legend('Measurement', 'Cleaned statistics')
    
end

