function beamPositionJitter(dataset_id,camName, visu, cropX, cropY)
%BEAMPOSITIONJITTER Summary of this function goes here
%   Detailed explanation goes here
    
    % visualize if desired
    numWorkers = 0;
    if ~exist('visu', 'var')
        visu = false;
    else
        if visu
            numWorkers = 0;
        end
    end
    % parfor setup
    if ~visu
        numWorkers = gcp().NumWorkers;
    end

    % check variables
    if (exist('cropY','var') && ~exist('cropX','var')) || (~exist('cropY','var') && exist('cropX','var'))
        error('Must specify both cropY and cropX, or none of them.')
    end
    if ~exist('cropX','var')
        cropX = NaN;
    end
    if ~exist('cropY','var')
        cropY = NaN;
    end
    
    %% add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    %Attempt to create folder for plots
    plotFolder = 'camJitterPlots';
    mkdir(plotFolder)
        
    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset)
        return; 
    end
    datasetInfo(dataset);
    disp(' ');
    
    % select measurement screen (OTR, spectrometer)
    camFields = fields(dataset.images);
    if numel(camFields) > 1
        if ~exist('camName','var')
            disp('Please provide camera name as argument');
            return;
        end
    else
        camName = camFields{1};
    end
    camStruct = dataset.images.(camName);
    %camDesc = camStruct.desc;

    % get common camera variables
    if strcmp(camName, 'FAR_OTR')
        calib_x = 0.0153; % mm/px
        calib_y = 0.0167; % mm/px
    elseif strcmp(camName, 'PLE_OTR')
        %From OCT19 ->
        calib_x = 0.0395;
        calib_y = -0.0413;
    elseif strcmp(camName, 'ELT_OTR')
        calib_x = 0.01081;
        calib_y = 0.01081;
    else
        disp(["Warning, camName '", camName, "' not recognized"])
        calib_x = 1.0;
        calib_y = 1.0;
    end
    

    bg_image = NaN; % TODO
    
    %% ANALYZE THE SCAN
    
    % cycle all scan shots
    numShots = dataset.metadata.numShots;
    [muxs, muys, sigxs, sigys] = deal(nan(1,numShots));
    
    %Better to split variables outside of PARFOR
    camStruct_dat = camStruct.dat;

    parfor (i = 1:numShots, numWorkers)
        
        %% IMAGE ANALYSIS
        
        % progress text (not parfor)
        %if mod(i-1,50)==0 && i ~= 1
        %    disp(' ');
        %end
        
        % get image
        if visu
            figure(4);
        end
        %image = camStruct.dat{i};
        image = camStruct_dat{i};
        
        % analyse image
        if ~isempty(image)
            if (~isnan(cropY(1)) && ~isnan(cropX(1))) %#ok<PFBNS>
                image = image(cropY,cropX);
            end

            toptitle = sprintf(', shot = %d', i);
            [mux, sigx, muy, sigy] = image_anal(image, visu, bg_image, toptitle);
        else
            disp(['BadCam!', num2str(i)]);
            mux = NaN;
            muy = NaN;
            sigx = NaN;
            sigy = NaN;
        end

        % calibrated beam positions
        muxs(i) = mux*calib_x; % [mm]
        muys(i) = muy*calib_y; % [mm]
        
        % calibrated beam sizes
        sigxs(i) = sigx*calib_x; % [mm]
        sigys(i) = sigy*calib_y; % [mm]
        
        % display progress
        fprintf('.');
        
    end
    
    disp(' ');
    clear camStruct_dat;
    
    %% DO STATISTICS
    
    for i = 1:numShots
        if isempty(camStruct.dat{1})
            continue
        end
        minIdx_x = 1;
        maxIdx_x = size(camStruct.dat{1}, 2);
        
        minIdx_y = 1;
        maxIdx_y = size(camStruct.dat{1}, 1);
        break
    end
    if (~isnan(cropY(1)) && ~isnan(cropX(1)))
        minIdx_x = cropX(1);
        maxIdx_x = cropX(end);
        
        minIdx_y = cropY(1);
        maxIdx_y = cropY(end);
    end
    
    muxs = muxs + (minIdx_x-1)*calib_x;
    muys = muys + (minIdx_y-1)*calib_y;
    
    %
    x_ave = mean(muxs);
    y_ave = mean(muys);
    x_std = std(muxs);
    y_std = std(muys);
    
    sx_ave = mean(sigxs);
    sy_ave = mean(sigys);
    sx_std = std(sigxs);
    sy_std = std(sigys);
    
    %
    disp(['x_ave  = ', num2str(x_ave),  ' [mm]'])
    disp(['y_ave  = ', num2str(y_ave),  ' [mm]'])
    disp(['x_std  = ', num2str(x_std),  ' [mm]'])
    disp(['y_std  = ', num2str(y_std),  ' [mm]'])
    
    disp(['sx_ave = ', num2str(sx_ave), ' [mm]'])
    disp(['sy_ave = ', num2str(sy_ave), ' [mm]'])
    disp(['sx_std = ', num2str(sx_std), ' [mm]'])
    disp(['sy_std = ', num2str(sy_std), ' [mm]'])
    
    %% FINAL PLOTTING
    if calib_x > 0
        min_x = minIdx_x*calib_x;
        max_x = maxIdx_x*calib_x;
    else
        min_x = maxIdx_x*calib_x;
        max_x = minIdx_x*calib_x;
    end
    if calib_y > 0
        min_y = minIdx_y*calib_y;
        max_y = maxIdx_y*calib_y;
    else
        min_y = maxIdx_y*calib_y;
        max_y = minIdx_y*calib_y;
    end
    
    figure(10)
    clf()
    plot(muxs,muys, '+')
    xlim([min_x,max_x])
    ylim([min_y,max_y])
    daspect([1 1 1])
    title('Position history')
    xlabel('x [mm]')
    ylabel('y [mm]')
    
    figure(11)
    clf()
    hold on;
    t=linspace(0,2*pi);
    for i=1:numShots
        % Draw ellipse
        X = muxs(i) + sigxs(i)*cos(t);
        Y = muys(i) + sigys(i)*sin(t);
        plot(X,Y)
    end
    xlim([min_x,max_x])
    ylim([min_y,max_y])
    daspect([1 1 1])
    title('Position history with size')
    xlabel('x [mm]')
    ylabel('y [mm]')
    hold off
    
    figure(20)
    subplot(2,1,1)
    histogram(muxs)
    title('Position histogram')
    xlabel('x [mm]')
    subplot(2,1,2)
    histogram(muys)
    xlabel('y [mm]')
    
    figure(21)
    subplot(2,1,1)
    histogram(sigxs)
    title('Beam size histogram')
    xlabel('x [mm]')
    subplot(2,1,2)
    histogram(sigys)
    xlabel('y [mm]')
    
end

