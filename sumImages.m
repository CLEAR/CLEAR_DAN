function [ totalImage, numShots ] = sumImages(dataset_id,camName, doPlot)
%SUMIMAGE Collects and displays the sum of the images from a dataset
%
%   Arguments:
%    dataset_id   : ID number or filename of dataset
%                   Can also be a cell array, e.g. {123,'asdf'}, in which
%                   case sumImages will sum over all the datafiles.
%    camName      : Which camera in the dataset to use.
%                   If only one camera is available,
%                   the script will automatically pick that one.
%                   Must be specified if dataset_id is a cellarray.
%    doPlot       : Show the plot or just return the data?
%                   The default is true
%
%   Returns:
%    totalImage   : The sum of all the images
%    numShots     : The number of images used for the sum
%
    
    %% add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);

    if ~exist('doPlot','var')
        doPlot = true;
    end
    
    if iscell(dataset_id)
        % Sum over multiple datasets
        [totalImage,ns] = sumImages(dataset_id{1},camName,false);
        totalImage = double(totalImage)/ns;
        for i = 2:numel(dataset_id)
            [totIm,ns] = sumImages(dataset_id{i},camName,false);
            totalImage = totalImage + double(totIm)/ns;
        end
        numShots = numel(dataset_id);

    else
        % Load a single dataset
        [dataset, dataset_id] = getDataset(dataset_id);
        if ~isstruct(dataset)
            return; 
        end
        datasetInfo(dataset);
        disp(' ');
        
        % select measurement screen (OTR, spectrometer)
        numShots = dataset.metadata.numShots;
        camFields = fields(dataset.images);
        if numel(camFields) > 1
            if ~exist('camName','var')
                disp('Please provide camera name as argument');
                return;
            end
        else
            camName = camFields{1};
        end
        camStruct = dataset.images.(camName);
        %camDesc = camStruct.desc;
        
        totalImage = camStruct.dat{1};
        for i = 2:numShots
           totalImage = totalImage + camStruct.dat{i}; 
        end
    end

    if doPlot
        figure()
        clf()
        imagesc(totalImage)
        axis equal
        colorbar
        if ~iscell(dataset_id)
            title(['dataset_id=', dataset_id],'Interpreter', 'none')
            subtitle(dataset.metadata.comment,'Interpreter', 'none')
        end
    end

end

