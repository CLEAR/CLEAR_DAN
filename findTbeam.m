function [] = findTbeam(dataset_id,dataset_id_ref, xy, tbeam_min, tbeam_max, tbeam_dt, smoothOrder, inOut, holdIt, dataset_id_bg, cropY, cropX, visu)
%FINDTBEAM
%   Produce a plot that compares the beam offset with the current,
%   for various beam arrival times on the scopes.
%   Inputs:
%    - A timing scan dataset ID
%    - A dataset ID for a position reference scan, OR [xref, yref], OR 0
%      (take the reference to [0.0, 0.0]
%    - A string 'x' or 'y'
%    - tbeam_min, tbeam_max, tbeam_dt: Where to look for the peak
%    - smoothOrder: Integer, how many points to smooth the current over
%      (1 -> no smoothing)
%    - inOut: Which current probe to use, 'in' or 'out'
%    - HoldIt: A string to labling the plot when plotting several
%      correlations on top of each other, or set to false to disable.
%
%    Optional arguments:
%    - dataset_id_bg : Dataset id for background image
%                      (NaN to ignore)
%    - cropX, cropY  : Crop indices start:stop in pixels for x,y
%                      (1D int index array)
%                      Set both or none to NaN to disable
%    - visu          : Enable visualization of the fitting?
%                      (bool, default = false)

    if exist('dataset_it_bg', 'var')
        %Not yet supported
        assert(isnan(dataset_id_bg))
    end

    if ~exist('visu', 'var')
        visu = false;
    end

    %% Configuration
    plotScopeTraces = true;
    %camName = 'PLE_OTR';
    camName = 'PLE_DIG';
    % get common camera variables
    if strcmp(camName, 'FAR_OTR')
        calib_x = 0.0153; % mm/px
        calib_y = 0.0167; % mm/px
    elseif strcmp(camName, 'PLE_OTR')
        %Valid until MAY19
        %calib_x = 0.0286; % mm/px
        %calib_y = -0.0305; % mm/px
        
        %From MAY19 ->
        calib_x = 0.0368;
        calib_y = -0.0389;
        
        L_otr = 0.2895; % [m]
    elseif strcmp(camName, 'PLE_DIG')
        %Rough assumption
        %calib_x = 0.02;
        %calib_y = 0.02;
        
        %Load from datafile
        calib_x = NaN;
        calib_y = NaN;

        L_otr = 0.2895; % [m]
    else
        disp('camName must be either "FAR_OTR" or "PLE_OTR", got:')
        disp(camName)
        return
    end
    
    if ~(strcmp(xy,'x') || strcmp(xy,'y'))
        disp('xy must be either x or y')
        return
    end
    
    if strcmp(inOut , 'out')
        scopeName = 'DIS_CUR_OUT';
    elseif strcmp(inOut, 'in')
        scopeName = 'DIS_CUR_IN';
    else
        disp('inOut must be either "in" or "out", got:')
        disp(inOut)
        return
    end
    
    global hold_IDs
    if holdIt
        if isempty(hold_IDs)
            hold_IDs = {};
        end
        holdTextLabel = '';
        if ischar(holdIt)
            holdTextLabel = [' (' holdIt ')'];
        end
        hold_IDs = [hold_IDs, {[num2str(dataset_id) '-' char(inOut) ':' num2str(smoothOrder) holdTextLabel]}];
        %disp(hold_IDs)
    else
        hold_IDs = {};
    end
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % cleaning helper function
    prc_cut = 90;
    prc_mask = @(m) m < prctile(m, prc_cut) & m > prctile(m, 100-prc_cut);
    
    %% load the reference dataset used for computing position offsets
    ref_fromDataset = NaN;
    if length(dataset_id_ref) == 2
        mux0 = dataset_id_ref(1);
        muy0 = dataset_id_ref(2);
        ref_fromDataset = false;

    elseif dataset_id_ref ~= 0
        [dataset_ref, dataset_id_ref] = getDataset(dataset_id_ref);
        if ~isstruct(dataset_ref)
            return; 
        end
        datasetInfo(dataset_ref);
        ref_fromDataset = true;

        % grab same image structure from the reference dataset
        camStruct_ref = dataset_ref.images.(camName);
        if isnan(calib_x)
            [calib_x,calib_y] = getCameraCalib_fromDataset(dataset_ref,camName);
        end
        % cycle all scan shots
        numShots_ref = dataset_ref.metadata.numShots;
        [muxs_ref, muys_ref] = deal(nan(1,numShots_ref));
        for i = 1:numShots_ref
            % progress text
            if mod(i,50)==0
                disp(' ');
            end

            if visu
                figure(1);
                %set(gcf, 'color', 'w');
                %colormap(mod_jet());
            end
            
            %% IMAGE ANALYSIS

            % get image
            image_ref = camStruct_ref.dat{i};
            
            % analyse image
            if ~isempty(image_ref)
                %crop image
                if exist('cropX','var') && exist('cropY','var')
                    if ~isnan(cropX) && ~isnan(cropY)
                        image_ref = image_ref(cropX,cropY);
                    end
                end
                [mux_ref, ~, muy_ref, ~] = image_anal(image_ref, visu);        
            else
                disp(['BadCam!', num2str(i)]);
                mux_ref = NaN;
                muy_ref = NaN;
            end

            % calibrated beam positions
            muxs_ref(i) = mux_ref*calib_x; % [mm]
            muys_ref(i) = muy_ref*calib_y; % [mm]

            % display progress
            fprintf('.');
        end
        disp(' ')

        % find the reference positions
        mux0 = mean(muxs_ref(prc_mask(muxs_ref))); % [mm]
        muy0 = mean(muys_ref(prc_mask(muys_ref))); % [mm]
    else
        mux0 = 0.0;
        muy0 = 0.0;
        ref_fromDataset = false;
    end
    disp(['mux0=', num2str(mux0,16)])
    disp(['muy0=', num2str(muy0,16)])
    disp(' ')
    
    %% load the main dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset)
        return; 
    end
    datasetInfo(dataset);
    
    if isempty(strfind(dataset.metadata.scanName, 'Discharge fine timing'))
        disp('Not a timing scan!');
        return
    end

    currentStruct = dataset.scopes.(scopeName);
    if smoothOrder > 1
        for i = 1:length(currentStruct.dat)
            currentStruct.dat{i} = smooth(currentStruct.dat{i},smoothOrder);
        end
    end
    % select measurement screen (OTR, spectrometer)
    %camFields = fields(dataset.images);
    camStruct = dataset.images.(camName);
    if isnan(calib_x)
        [calib_x,calib_y] = getCameraCalib_fromDataset(dataset_ref,camName);
    end
    
    %Loop over the data in the dataset and extract \Delta y and I(tBeam)
    numShots    = dataset.metadata.numShots;
    scope_time = [];
    for i=1:numShots
        if ~isempty(currentStruct.time_ns{i})
            scope_time = currentStruct.time_ns{i};
            break
        end
    end
    assert ( ~isempty(scope_time) )
    
    %if ref_fromDataset
    assert(scope_time(1) < tbeam_min);
    %end
    
    Iall        = zeros(dataset.metadata.numShots,length(scope_time));
    %Iall_smooth = zeros(dataset.metadata.numShots,length(scope_time));

    muxs        = zeros(dataset.metadata.numShots,1);
    muys        = zeros(dataset.metadata.numShots,1);
    
    for i=1:numShots
        %% IMAGE ANALYSIS
        
        % progress text
        if mod(i,50)==0
            disp(' ');
        end
        
        % get image
        image = camStruct.dat{i};
        
        if visu
            figure(4);
        end
        
        % analyse image
        if ~isempty(image)
            if exist('cropX','var') && exist('cropY','var')
                if ~isnan(cropX) && ~isnan(cropY)
                    image = image(cropX,cropY);
                end
            end
            [mux, ~, muy, ~] = image_anal(image,visu);
        else
            disp(['BadCam!', num2str(i)]);
            mux = NaN;
            muy = NaN;
        end

        % calibrated beam positions
        muxs(i) = mux*calib_x - mux0; % [mm]
        muys(i) = muy*calib_y - muy0; % [mm]
        
        %Current data
        if i < length(currentStruct.dat) && ~isempty(currentStruct.dat{i})
            assert(isequal(scope_time,currentStruct.time_ns{i}));
            Iall(i,:) = currentStruct.dat{i};
        else
            disp(['NoScope! i=' num2str(i)]);
            Iall(i,:) = NaN;
        end
        
        % display progress
        fprintf('.'); 
    end
    disp(' ')
    
    %% Plots!
    
    % Draw plot showing the raw currents, useful for debugging
    if plotScopeTraces
        figure(1)
        for i = 1:length(currentStruct.dat)
            plot(scope_time,currentStruct.dat{i})
            if i == 1
                hold on;
            end
        end
    
        plot([tbeam_min,tbeam_min],[-150,max(currentStruct.dat{1}*1.2)], 'LineWidth', 3, 'Color', 'k')
        plot([tbeam_max,tbeam_max],[-150,max(currentStruct.dat{1}*1.2)], 'LineWidth', 3, 'Color', 'k')
    
        xlabel('Time [ns]')
        ylabel('Current [A]')
        title('Current curves and window for correlation plot')
        hold off;
    end
    
    if ref_fromDataset
        % Find indices for the timings
        timeIndices = [];
        tFind = tbeam_min;
        for i = 1:length(scope_time)
            if scope_time(i) > tFind
                timeIndices(length(timeIndices)+1) = i;
                tFind = tFind+tbeam_dt;
            end
            if scope_time(i) > tbeam_max
                break;
            end
        end

        %Draw scatter plots of Delta y VS I(t)
        figure(2)
        fits   = zeros(length(timeIndices),2);
        colors = zeros(length(timeIndices),3);

        i = 0;
        for ii = timeIndices
            i = i+1;
            goodTimes = find(isnan(Iall(:,ii))==0);
            if     xy == 'x'
                splot       = scatter(Iall(goodTimes,ii), muxs(goodTimes));
                colors(i,:) = splot.CData;
                fits  (i,:) = polyfit(Iall(goodTimes,ii), muxs(goodTimes),1);
            elseif xy == 'y'
                splot       = scatter(Iall(goodTimes,ii), muys(goodTimes));
                colors(i,:) = splot.CData;
                fits  (i,:) = polyfit(Iall(goodTimes,ii), muys(goodTimes),1);
            end
            if ii == timeIndices(1)
                hold on
            end
        end
        for i = 1:length(fits)
            plot(Iall(goodTimes,timeIndices(i)),polyval(fits(i,:),Iall(goodTimes,timeIndices(i))),'Color',colors(i,:))
        end
        xlabel('I(t_{beam}) [A]');
        ylabel('\Delta y [mm]');
        legend(string(round(scope_time(timeIndices))));
        %if ~holdIt
        title(['Correlations for different t_{beam} [ns] \rm(dataset ' dataset_id ', reference ' dataset_id_ref ')']);
        hold off;
        %else
        %    title(['Correlations for different t_{beam} [ns]']);
        %end
    end
    
    %Draw a goodness-of-fit plot
    figure(10)
    if ~holdIt
        clf()
    end
    normOfFitResiduals = zeros(length(scope_time),1);
    for i = 1:length(scope_time)
        goodTimes = find(isnan(Iall(:,i))==0);
        if xy == 'x'
            [~,fitS] = polyfit(Iall(goodTimes,i), muxs(goodTimes),1);
        elseif xy == 'y'
            [~,fitS] = polyfit(Iall(goodTimes,i), muys(goodTimes),1);
        end
        
        normOfFitResiduals(i) = fitS.normr;
    end
    
    plot(scope_time, normOfFitResiduals)
    xlabel('t_{ref} [ns]')
    ylabel('Residuals norm [mm]')
    if ~holdIt
        title(['Residuals vs. t_{ref} \rm(dataset ' dataset_id ', reference ' dataset_id_ref ')']);
        hold off
    else
        title('Residuals vs. t_{ref}');
        legend(hold_IDs);
        hold on
    end
    
    [~, minResIdx] = min(normOfFitResiduals);
    fprintf("Minimum for dataset %s at t=%d\n\n", dataset_id, round(scope_time(minResIdx)));
    
end

