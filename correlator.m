function [] = correlator(dataset_id, cameras, scopes, scalars)
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);

    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset); return; end
    datasetInfo(dataset);
    
    % defaults
    if ~exist('scopes','var')
        scopes = {};
    end
    if ~exist('scalars','var')
        scalars = {};
    end
    
    % shots and steps
    numShots = dataset.metadata.numShots;
    
    % prepare figure
    figure(4);
    clf(4);
    set(gcf, 'color', 'w');
    
    % make subplot setup
    ncams = numel(cameras);
    nscopes = numel(scopes);
    nscalars = numel(scalars);
    nvars = ncams + nscopes + nscalars;
    nplots = nvars*(nvars-1)/2;
    nrows = ceil(sqrt(nplots));
    ncols = ceil(nplots/nrows);
    
    % cycle cameras, scopes and scalars
    values = zeros(nplots, numShots);
    names = cell(1, nplots);
    funs = cell(1, nplots);
    for i = 1:nvars
        
    % make camera or scope structures
        if i <= ncams % camera
            name = cameras{i}{1};
            fun = cameras{i}{2};
            signalStruct = dataset.images.(name);
            isScalar = false;
        elseif i <= ncams + nscopes % scopes
            i_eff = i - ncams;
            name = scopes{i_eff}{1};
            fun = scopes{i_eff}{2};
            signalStruct = dataset.scopes.(name);
            isScalar = false;
        else % scalars
            i_eff = i - ncams - nscopes;
            name = scalars{i_eff}{1};
            if numel(scalars{i_eff}) > 1
                fun = scalars{i_eff}{2};
            else
                fun = @(x) x; % default dummy function
            end
            signalStruct = dataset.scalars.(name);
            isScalar = true;
        end
        
        % save name
        names{i} = name;
        funs{i} = fun;
        
        % cycle shots
        for j = 1:numShots
            if isScalar
                signal = signalStruct.dat(j);
            else
                signal = signalStruct.dat{j};
            end
            values(i,j) = fun(signal);
        end
        
    end
    
    n = 1;
    for i = 1:nvars % cycle vertical axis
        for j = 1:nvars % cycle horizontal axis
            
            if i==j % name text box
                
                axis on;
                subplot(nvars, nvars, n);
                text(0.5*[1 1], 0.5*[1 1], esc_uscore(names{j}), 'HorizontalAlignment','center', 'FontSize', 13, 'FontWeight', 'bold');
                text(0.5*[1 1], 0.2*[1 1], func2str(funs{j}), 'HorizontalAlignment','center', 'FontSize', 9, 'FontWeight', 'normal');
                axis off;
                
            else % correlation plots
                
                % plot
                subplot(nvars, nvars, n);
                plot(values(j,:), values(i,:), '.');
                xlabel(esc_uscore(names{j}));
                ylabel(esc_uscore(names{i}));

                % adjusting limits
                xmin = min(values(j,:));
                xmax = max(values(j,:));
                xlim([xmin - (xmax-xmin)*0.05, xmax + (xmax-xmin)*0.05]);
                ymin = min(values(i,:));
                ymax = max(values(i,:));
                ylim([ymin - (ymax-ymin)*0.05, ymax + (ymax-ymin)*0.05]);
            end
            
            % increment subplot counter
            n = n + 1;
            
        end 
    end
    
    
end