function [stepNum, posX,posY, beamX, beamY, beamX0, beamY0] = offsetscan_simple(dataset_id, dataset_id_ref, visu, showPlots, removeSteps, camName, cropY, cropX)
    %OFFSETSCAN_SIMPLE Simple offsetscan processing tool
    %
    % Input parameters:
    %  - dataset_id     : The id of the main dataset // int
    %  - dataset_id_ref : The id of the dataset to use for
    %                     position reference // int
    % Optional input parameters:
    %  - visu           : Visualize the data analysis step-by-step // bool
    %                     Default = false
    %  - showPlots      : Show diagnostic mini-analysis at the end // bool
    %                     Default = false
    %  - removeSteps    : Range of steps numbers to throw away (no beam etc.)
    %                     // int array
    %                     Default = []
    %  - camName        : Which camera to use for the analysis // string
    %                     Default = 'PLE_DIG';
    %  - cropY, cropX   : Range of pixels to not throw away for the analysis.  
    %                     // int array
    %                     Default = NaN (disable cropping)
    % Returns:
    %  - posX:   Gauge position X (incl. callibration) of mover [mm]
    %            // double array
    %  - posY:   Gauge position Y (incl. callibration) of mover [mm]
    %            // double array
    %  - beamX: Beam position on screen X [mm]
    %            // double array
    %  - beamX: Beam position on screen X [mm]
    %            // double array
    %  - beamX0: Reference beam position on screen in X [mm] // double
    %  - beamY0: Reference beam position on screen in Y [mm] // double
    %  - stepNum: Step number index // int array
    %
    % TODO: Add handling of scope for current estimation and exclusion of
    %       shots with CMB off. This is already in offsetscan.m.
    
    %% HANDLE INPUT ARGUMENTS

    if ~exist('visu', 'var')
        visu = false;
    end

    if ~exist('showPlots', 'var')
        showPlots = false;
    end

    if ~exist('removeSteps', 'var')
        removeSteps = [];
    end

    if ~exist('camName','var')
        camName = 'PLE_DIG';
    end

    if ~exist('cropX','var')
        cropX = NaN;
    else
        if (length(cropX) == 1 && ~isnan(cropX))
            error('CropX must be an array or NaN')
        end 
    end
    if ~exist('cropY','var')
        cropY = NaN;
    else
        if (length(cropY) == 1 && ~isnan(cropY))
            error('CropY must be an array or NaN')
        end 
    end

    %% VARIOUS DEFINITIONS

    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);

    % cleaning helper function
    prc_cut = 90;
    prc_mask = @(m) m < prctile(m, prc_cut) & m > prctile(m, 100-prc_cut);
    
    %% POSITION REFERENCE / SPARK OFF DATASET
    
    % load the reference dataset
    disp('Loading reference dataset...')
    dataset_ref = getDataset(dataset_id_ref);
    if ~isstruct(dataset_ref)
        error('Could not load reference dataset')
    end
    datasetInfo(dataset_ref);
    disp(' ');
    
    % grab image structure from the reference dataset
    camStruct_ref = dataset_ref.images.(camName);
    [calib_x,calib_y] = getCameraCalib_fromDataset(dataset_ref,camName);

    % cycle all scan shots in the reference dataset
    numShots_ref = dataset_ref.metadata.numShots;
    [muxs_ref, muys_ref] = deal(nan(1,numShots_ref));
    %Better to split variables outside of PARFOR
    camStruct_ref_dat = camStruct_ref.dat;

    for i = 1:numShots_ref
        
        % progress text (not parfor)
        if mod(i-1,50)==0 && i ~= 1
            disp(' ');
        end
        
        % get image
        image_ref = camStruct_ref_dat{i};
        
        %crop image
        if (~isnan(cropY(1)) && ~isnan(cropX(1))) && ~isempty(image_ref)
            image_ref = image_ref(cropY,cropX);
        end
        
        % analyse image
        if visu
            figure(1);
            %set(gcf, 'color', 'w');
            %colormap(mod_jet());
        end
        if ~isempty(image_ref)
            [mux_ref, ~, muy_ref, ~] = image_anal(image_ref, visu);
        else
            disp(['BadCam!', num2str(i)]);
            mux_ref = NaN;
            muy_ref = NaN;
        end
        
        % calibrated beam positions
        muxs_ref(i) = mux_ref*calib_x; % [mm]
        muys_ref(i) = muy_ref*calib_y; % [mm]
        
        % display progress
        fprintf('.');
    end

    disp(' ');
    clear camStruct_ref_dat;
    
    % find the reference beam positions
    beamX0 = mean(muxs_ref(prc_mask(muxs_ref))); % [mm]
    beamY0 = mean(muys_ref(prc_mask(muys_ref))); % [mm]
    fprintf('mux0 = %d [mm], muy0 = %d [mm]\n\n', beamX0, beamY0);
    
    %% ANALYZE THE ACTUAL SCAN 

    % load the dataset
    disp('Loading main dataset...')
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset)
        error('Could not load main dataset')
    end
    datasetInfo(dataset);
    disp(' ');

    camStruct = dataset.images.(camName);
    
    % cycle all scan shots
    numShots = dataset.metadata.numShots;
    [muxs, muys] = deal(nan(1,numShots));
    %Better to split variables outside of PARFOR
    camStruct_dat = camStruct.dat;
    step_num_dat = dataset.scalars.step_num.dat;
    for i = 1:numShots
        
        % IMAGE ANALYSIS
        
        % progress text (not parfor)
        if mod(i-1,50)==0 && i ~= 1
            disp(' ');
        end
        
        %Skip fitting censored data
        if ismember(step_num_dat(i), removeSteps)
            fprintf('C')
            muxs(i) = NaN;
            muys(i) = NaN;
            continue
        end

        % get image
        if visu
            figure(4);
        end
        %image = camStruct.dat{i};
        image = camStruct_dat{i};
        
        % analyse image
        if ~isempty(image)
            if (~isnan(cropY(1)) && ~isnan(cropX(1)))
                image = image(cropY,cropX);
            end
            toptitle = sprintf(', step = %d, shot = %d', step_num_dat(i), i);
            [mux, ~, muy, ~] = image_anal(image, visu, NaN, toptitle);
        else
            disp([' BadCam!', num2str(i)]);
            mux = NaN;
            muy = NaN;
        end

        % calibrated beam positions
        muxs(i) = mux*calib_x; % [mm]
        muys(i) = muy*calib_y; % [mm]
                
        % display progress
        fprintf('.');
        
    end

    disp(' ');
    clear camStruct_dat step_num_dat;
    
    
    %% MAIN OUTPUT

    stepNum = dataset.scalars.step_num.dat;
    beamX   = muxs;
    beamY   = muys;
    
    posX = dataset.scalars.PLE_GAUGE_X.dat;
    posY = dataset.scalars.PLE_GAUGE_Y.dat;

    %Filter by removeSteps
    steps = unique(dataset.scalars.step_num.dat);     %Step index
    offsets = unique(dataset.scalars.step_value.dat); %Offset setpoint
    keepPoints = ones(1,length(dataset.scalars.step_num.dat)); %Array of True/False indicating wether to keep a given shot
    %Loop over unique steps, not shots
    for j = 1:numel(steps)
        this_step_num = steps(j);
        
        %Untagging data from steps in removeSteps
        if ismember(j,removeSteps)
            fprintf('Skipping step # %i, position = %g\n', j, offsets(j))
            keepPoints(dataset.scalars.step_num.dat == this_step_num) = 0;
            continue
        end
        
        % TODO Add other removal criteria here, e.g. no good fit, no
        % discharge, no beam, ...
    end

    %Delete to-be-removed points from output data
    stepNum = stepNum(keepPoints==1);
    beamX   = beamX(keepPoints==1);
    beamY   = beamY(keepPoints==1);
    posX    = posX(keepPoints==1);
    posY    = posY(keepPoints==1);
    
    %% A few simple plots
    if showPlots
        %Lens movement
        figure()
        scatter(posX,posY,[],stepNum)
        axis equal
        hcb = colorbar();
        hcb.Title.String = 'StepNum';
        xlabel('Lens position X [mm]')
        ylabel('Lens position Y [mm]')
        title(['Lens position \rm(dataset ' dataset_id ')']);
        
        %Beam movement on screen
        figure()
        scatter(beamX-beamX0,beamY-beamY0,[],stepNum)
        hcb = colorbar();
        hcb.Title.String = 'StepNum';
        axis equal
        xlabel('Beam shift X [mm]')
        ylabel('Beam shift Y [mm]')
        title(['Beam offset \rm(dataset ' dataset_id ')']);

        %Correlation matrix plot
        figure()
        subplot(2,2,1)
        scatter(posX,beamX-beamX0,[],stepNum)
        ylabel('Beam shift X [mm]')

        subplot(2,2,2)
        scatter(posY,beamX-beamX0,[],stepNum)

        subplot(2,2,3)
        scatter(posX,beamY-beamY0,[],stepNum)
        ylabel('Beam shift Y [mm]')
        xlabel('Lens position X [mm]')

        subplot(2,2,4)
        scatter(posY,beamY-beamY0,[],stepNum)
        xlabel('Lens position Y [mm]')
        
        sgtitle(['Beam reaction \rm(dataset ' dataset_id ')']);

    end
end

