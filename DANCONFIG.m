function [ config ] = DANCONFIG()

    %% CLEAR_DAQ CONFIG

    % declare configuration struct
    config = struct();

    config.DAQPATH = '/clear/data/MatLab/Experiments/CLEAR_DAQ';
    %config.DAQPATH = '/clear/data/MatLab/Experiments/PlasmaLens/clindstr/CLEAR_DAQ'
    %config.DAQPATH = '/home/kyrsjo/code/CLEAR_DAQ'
end