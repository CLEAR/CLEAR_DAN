function [dataset_id, ...
          capWidth_cam,  capCenter_cam,  beamSigma_cam, ...
          capWidth_loss, capCenter_loss, beamSigma_loss     ] = ...
          fitAperture(dataset_id, visu, scopeRange, camRangeY, camRangeX, rCap, hasScope)
    % fitAperture
    % Input parameters:
    % - dataset_id : ID of the dataset
    % - rCap       : Initial guess for capillary radius
    %
    % Semi-optional input parameters:handles.dataset
    % - scopeRange:
    %                Index array start:stop specifying where to re13.6ad the
    %                radiation monitor scope.
    %                If not provided, a plot with all the scope traces
    %                will be produced and the script is stopped.
    % - camRangeY, camRangeX:
    %                Index array specifying what part of the camera image to use.
    %                If not provided, one of the images will be showed
    %                and the script is stopped.
    %
    % Optional input parameters:
    % - visu       : Show plots
    %                Default: true
    % - hasScope   : Can be used to disable processing of loss monitor data
    %                Default: true


    
    if ~exist('visu','var')
        visu=true;
    end
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset)
        return;
    end
    datasetInfo(dataset);
    
    % determine type of scan
    if ~dataset.metadata.isScan
        disp('Not a scan!');
        return;
    end

    %Config
    %camname = 'PLE_OTR';
    camname = 'PLE_DIG';
    scopename = 'PMT_RAD';
    
    if ~exist('hasScope', 'var')
        hasScope=true;
    end
    
    % shots and steps
    numShots = dataset.metadata.numShots;
    numSteps = dataset.metadata.numSteps;
    step_values = dataset.scalars.step_value.dat;
    uniq_steps = unique(step_values);
    
    %% Display a typical shot, to help set camera- and scope ranges
    if visu
        figure(1)
        clf(1)
        if hasScope
            for j = 1:numShots
                %disp(j)
                %disp(dataset.scopes.(scopename).dat{j})
                if j < length(dataset.scopes.(scopename).dat) && ...
                     ~isempty(dataset.scopes.(scopename).dat{j})

                    plot(dataset.scopes.(scopename).dat{j})

                else
                    disp(['NoScope!' num2str(j)]);
                end

                if j == 1
                    hold on;
                end
            end
            title('Scope signals')
            xlabel('Time index')
            ylabel('Signal strength [V]')
        end
    end
    if hasScope
        if ~exist('scopeRange','var')
            error('please set a scopeRange')
        else
            if visu
                xline(min(scopeRange));
                xline(max(scopeRange));
            end
        end
    end
    
    midImg = round(numShots/2+1);
    if visu
        figure(2)
        clf(2)
        subplot(2,1,1)
        
        %disp(dataset.images.(camname).dat{midImg})
        imagesc(dataset.images.(camname).dat{midImg})
        
        title('Camera signals (full image)')
        xlabel('x position [px]')
        ylabel('y position [px]')
        
        if exist('camRangeX', 'var')
            xline(min(camRangeX));
            xline(max(camRangeX));
        end
        if exist('camRangeY', 'var')
            yline(min(camRangeY));
            yline(max(camRangeY));
        end 
    end
    if ~exist('camRangeX','var')
        error('please set a camRangeX')
    elseif ~exist('camRangeY','var')
        error('please set a camRangeY')
    else
        if visu
            subplot(2,1,2)
            imagesc(dataset.images.(camname).dat{midImg}(camRangeY,camRangeX));

            title('Camera signals (zoomed)')
            xlabel('x position [px]')
            ylabel('y position [px]')
        end
    end
    
    %% Cycle shots to get data
    cameraIntegral   = zeros(1,numShots);
    beamlossIntegral = zeros(1,numShots);
    for j = 1:numShots
       %Camera
       if ~isempty(dataset.images.(camname).dat{j})
        img = dataset.images.(camname).dat{j}(camRangeY,camRangeX);
        cameraIntegral(j) = mean(mean(img));
       else
        disp(['BadCam!', num2str(j)]);
        cameraIntegral(j) = NaN;
       end

       if hasScope
           %Scope
           if j < length(dataset.scopes.(scopename).dat) && ...
                ~isempty(dataset.scopes.(scopename).dat{j})

               sig = dataset.scopes.(scopename).dat{j};
               beamlossIntegral(j) = -sum(sig(scopeRange));
           else
               beamlossIntegral(j) = NaN;
           end
       end           
    end
    
    
    %% Do statistics
    [cam_mean, cam_std]   = deal(zeros(1,numel(uniq_steps)));
    [loss_mean, loss_std] = deal(zeros(1,numel(uniq_steps)));
    
    for j = 1:numel(uniq_steps)
        
        stepmask = (step_values==uniq_steps(j));
        
        camInt_j  = cameraIntegral(stepmask);
        if hasScope
            lossInt_j = beamlossIntegral(stepmask);
        end
        
        % Kill outliers
        if sum(stepmask)>8
            camInt_j  = camInt_j (camInt_j  > prctile(camInt_j,  25) &  camInt_j  < prctile(camInt_j,  75));
            if hasScope
                lossInt_j = lossInt_j(lossInt_j > prctile(lossInt_j, 25) &  lossInt_j < prctile(lossInt_j, 75));
            end
        elseif sum(stepmask)>4
            camInt_j  = camInt_j (camInt_j  > prctile(camInt_j,  10) &  camInt_j  < prctile(camInt_j,  90));
            if hasScope
                lossInt_j = lossInt_j(lossInt_j > prctile(lossInt_j, 10) &  lossInt_j < prctile(lossInt_j, 90));
            end
        end
        
        cam_mean(j) = mean(camInt_j);
        cam_std(j)  = std (camInt_j);
        
        if hasScope
            loss_mean(j) = mean(lossInt_j, 'omitnan');
            loss_std(j)  = std (lossInt_j, 'omitnan');
        end
    end
    
    %% Plot before fit, in case of trouble
    function anot(b_left, b_right, yoff)
        xline(b_left(1), 'Color', 'black');
        xline(b_right(1),'Color', 'black');

        %These don't work because MATLAB :(
        % No way to specify data units!!!
        
        %halfway = b_left(1)+0.5*b_left(2);
        %annotation('doublearrow',[b_left(1), b_right(1)], [halfway, halfway]);
        
        %pos = get(gca, 'Position');
        %left  = (b_left(1)  + abs(min(xlim)))/diff(xlim) * pos(3) + pos(1);
        %right = (b_right(1) + abs(min(xlim)))/diff(xlim) * pos(3) + pos(1);
        %halfway = 0.5*pos(4)+pos(1);
        %annotation(ax, 'doublearrow', [left, right], [halfway, halfway]);
        annotation('textbox', [0.35,yoff, 0.3,0.12], 'String', ...
            {['width=', num2str(round(b_right(1)-b_left(1))),      ' [um]'],...
             ['center=',num2str(round((b_right(1)+b_left(1))/2.0)),' [um]'],...
             ['\sigma_L=' num2str(round(b_left(2))), ' [um], \sigma_R=', num2str(round(b_right(2))), ' [um]']},...
             'HorizontalAlignment', 'center', 'FontSize', 8)
        
    end
    
    if visu
        figure(3);
        clf(3);
        
        if hasScope
            ax1 = subplot(2,1,1);
        end
        errorbar(uniq_steps, cam_mean, cam_std);
        
        title(['dataset ' esc_uscore(dataset_id), ', ', num2str(numSteps) ' steps, ' num2str(numShots) ' shots']);
        ylabel('Camera integral [a.u.]')
        
        if hasScope
            ax2 = subplot(2,1,2);
            errorbar(uniq_steps, loss_mean, loss_std);
        
            ylabel('PMT integral [a.u.]')
        end
        
        if numel(dataset.metadata.stepUnit) > 0
            xlabel([dataset.metadata.scanName ' (' dataset.metadata.stepUnit ')']);
        else
            xlabel(dataset.metadata.scanName);
        end
    
        if hasScope
            linkaxes([ax1,ax2],'x')
        end
        xmin = min(uniq_steps);
        xmax = max(uniq_steps);
        if ~hasScope
            ax2 = gca();
        end
        xlim(ax2, [xmin - (xmax-xmin)/numSteps*0.5, xmax + (xmax-xmin)/numSteps*0.5]);
        
    end
    
    if ~exist('rCap','var')
        error('Please provide a capillary radius [um]')
    end
    
    %% Fit
    % 1. Define fit functions
    fcnLeft  = @(b,x) b(4) + b(3) *    normcdf(x, b(1), b(2));
    fcnRight = @(b,x) b(4) + b(3) * (1-normcdf(x, b(1), b(2)));
    
    % 2. Estimate parameters initial 
    b0_camLeft  = [-rCap, 100, max(cam_mean)-min(cam_mean), min(cam_mean)];
    b0_camRight = [ rCap, 100, max(cam_mean)-min(cam_mean), min(cam_mean)];
    
    if hasScope
        b0_lossLeft  = [-rCap, 100, max(-loss_mean)-min(-loss_mean), min(-loss_mean)];
        b0_lossRight = [ rCap, 100, max(-loss_mean)-min(-loss_mean), min(-loss_mean)];
    end
    
    %3. Find the data cropping point
    jCenter = 1;
    stepMin = abs(uniq_steps(1));
    for j = 2:numel(uniq_steps)
        if abs(uniq_steps(j)) < abs(stepMin)
            stepMin = uniq_steps(j);
            jCenter = j;
        end
    end
    disp(['Found closest point to x=0 at j=', num2str(jCenter), ', x = ', num2str(stepMin)])
    
    % 3. Run the solver!
    % TODO: In may be better to use nlinfit,
    % which can actually take weights...
    fitOpts = optimset('Display','off');
    stepsLeft = uniq_steps(1:jCenter);
    stepsRight = uniq_steps(jCenter:end);
    
    [b_camLeft,~,~,exitflag,~]  = lsqcurvefit(fcnLeft, b0_camLeft, stepsLeft,  cam_mean(1:jCenter),   [], [], fitOpts);
    if ~(exitflag > 0)
        disp('fitting of camLeft failed')
        b_camLeft = [NaN, NaN, NaN, NaN];
    end
    
    [b_camRight,~,~,exitflag,~] = lsqcurvefit(fcnRight,b0_camRight, stepsRight,cam_mean(jCenter:end), [], [], fitOpts);
    if ~(exitflag > 0)
        disp('fitting of camRight failed')
        b_camRight = [NaN, NaN, NaN, NaN];
    end
    
    if hasScope
        [b_lossLeft,~,~,exitflag,~]  = lsqcurvefit(fcnLeft, b0_lossLeft, stepsLeft,  -loss_mean(1:jCenter),   [], [], fitOpts);
        if ~(exitflag > 0)
            disp('fitting of lossLeft failed')
            b_lossLeft = [NaN, NaN, NaN, NaN];
        end

        [b_lossRight,~,~,exitflag,~] = lsqcurvefit(fcnRight,b0_lossRight,stepsRight,-loss_mean(jCenter:end), [], [], fitOpts);
        if ~(exitflag > 0)
            disp('fitting of lossRight failed')
            b_lossRight = [NaN, NaN, NaN, NaN];
        end
    else
        b_lossLeft  = [NaN, NaN, NaN, NaN];
        b_lossRight = [NaN, NaN, NaN, NaN];
    end
    
    %Data for return
    capWidth_cam   =      b_camRight(1) -b_camLeft(1);
    capCenter_cam  = 0.5*(b_camRight(1) +b_camLeft(1));
    beamSigma_cam  = [b_camLeft(2),  b_camRight(2)];
    
    capWidth_loss  =      b_lossRight(1)-b_lossLeft(1);
    capCenter_loss = 0.5*(b_lossRight(1)+b_lossLeft(1));
    beamSigma_loss = [b_lossLeft(2), b_lossRight(2)];
    
    
    %% More plotting, including the fits
    if visu
        xFitLeft  = linspace(min(uniq_steps),stepMin);
        xFitRight = linspace(stepMin,max(uniq_steps));
        
        figure(3);
        
        if hasScope
            subplot(2,1,1);
        end
        hold on;
        plot(xFitLeft, fcnLeft (b_camLeft, xFitLeft), 'Color', 'red');
        plot(xFitRight,fcnRight(b_camRight,xFitRight),'Color', 'red');
        
        anot(b_camLeft,  b_camRight, 0.65);
        
        if hasScope
            subplot(2,1,2);
            hold on;
            plot(xFitLeft, -fcnLeft (b_lossLeft, xFitLeft), 'Color', 'red');
            plot(xFitRight,-fcnRight(b_lossRight,xFitRight),'Color', 'red');

            anot(b_lossLeft, b_lossRight, 0.3);
        end                
    end
    
    %% Text outputs
    disp(' ');
    disp(['Capillary width from camera = ', num2str(capWidth_cam),  ' [um]']);
    if hasScope
        disp(['Capillary width from losses = ', num2str(capWidth_loss), ' [um]']);
    end
    disp(' ');
    disp(['Capillary center from camera = ', num2str(capCenter_cam),  ' [um]']);
    if hasScope
        disp(['Capillary center from losses = ', num2str(capCenter_loss), ' [um]']);
    end
    disp(' ');
    disp(['Sigma from camera: Left = ', num2str(beamSigma_cam(1)), ', right = ', num2str(beamSigma_cam(2)),  ' [um]']);
    if hasScope
        disp(['Sigma from losses: Left = ', num2str(beamSigma_loss(1)),', right = ', num2str(beamSigma_loss(2)), ' [um]']);
    end
    
    %Preparation for return
    dataset_id = str2num(dataset_id);
end

