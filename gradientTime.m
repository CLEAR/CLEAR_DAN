function gradientTime(dataset_id, dataset_id_ref, E_MeV, xy, Roff, visu, I_uniform, t_beam, R, holdLegend, inOut, t0, cropY, cropX, timeOffset_err, nMC, extraPlotPoints_t, extraPlotPoints_genh, showInfoBoxes)
    %gradientTime
    %   Produce a plot of the gradient enhancement ratio as a function of time,
    %   found by comparing the theoretical gradient expected from the current
    %   with the one found from the (single-point) offset.
    %
    % Input parameters:
    %  - dataset_id     : The id of the main dataset // int
    %  - dataset_id_ref : The id of the dataset to use for
    %                     position reference // int
    %  - E_MeV          : Beam energy in MeV,
    %                     to convert deflection to field // float
    %  - xy             : Horizontal ('x') or vertical ('y') offset beam? // str
    %  - Roff           : How far is the beam offset in um? // float
    %  - t_beam         : Scope-time of beam arrival //int
    %                     as found using findTbeam.m
    %
    % Optional input parameters:
    %  - visu           : Visualize the data analysis step-by-step // bool
    %                     Default = false
    %
    %  - I_uniform      : Current to use for the gradient plot // int
    %                     If > 0  : Use this current
    %                     If <= 0 : take the current from t_beam, and scale by -1*I_beam
    %                     Default = -1
    %
    %  - R              : Capillary radius in um // float
    %                     Default = 500.0
    %                     Also possible to specify [Rx, Ry]
    %
    %  - holdLegend     : Legend for this data in the final plot // str;
    %                     if not present or NaN make a new plot.
    %
    %  - inOut          : Which current probe to use, 'in' or 'out. // str;
    %                     Defaults to 'out' (for reproducibility reasons)
    %
    %  - t0             : Number to subtract from the x-axis,
    %                     for readability // double
    %
    %  - cropY, cropX   : Range of pixels to not throw away for the
    %                     analysis.
    %
    %  - timeOffset_err : Assumed error in t_beam [ns] // float
    %
    %
    %  - nMC            : Number of samples to use for monte-carlo
    %                     error estimate (set to 0 to deactivate) // int
    %                     Default = 1000
    %
    %  - extraPlotPoints_t, extraPlotPoints_genh :
    %                     Position of extra gradient enhancement points
    %                     (found by offsetscans) to plot.
    %                     Default: NaN, NaN
    %
    %  - showInfoBoxes  : Show extra info boxes in plots (useful for dev.)
    %                     Default: false
    % Returns:
    
    % parfor setup
    numWorkers = gcp().NumWorkers;
    
    % visualize if desired
    if ~exist('visu', 'var')
        visu = false;
    else
        if visu
            numWorkers = 0;
        end
    end
    disp(['numWorkers = ', num2str(numWorkers)]);

    if ~exist('t_beam','var')
        t_beam = -1;
    end
    if ~exist('I_uniform','var')
        I_uniform = -1;
    end
    if ~exist('R','var')
        R = 500.0; % [um]
    end
    if length(R) == 1
        Rx = R;
        Ry = R;
    elseif length(R) == 2
        Rx = R(1);
        Ry = R(2);
    else
        error('R must be a scalar or a length 2 array [Rx,Ry]');
    end

    if ~exist('t0', 'var')
        t0 = 0.0;
    end
    
    if (exist('cropY','var') && ~exist('cropX','var')) || (~exist('cropY','var') && exist('cropX','var'))
        error('Must specify both cropY and cropX, or none of them.')
    end
    if ~exist('cropX','var')
        cropX = NaN;
    end
    if ~exist('cropY','var')
        cropY = NaN;
    end
    
    if ~exist('extraPlotPoints_t','var')
        extraPlotPoints_t = NaN;
    end
    if ~exist('extraPlotPoints_genh','var')
        extraPlotPoints_genh = NaN;
    end
    if isnan(extraPlotPoints_t)
        assert(isnan(extraPlotPoints_genh))
    end
    if isnan(extraPlotPoints_genh)
        assert(isnan(extraPlotPoints_t))
    end
    
    if ~exist('showInfoBoxes', 'var')
        showInfoBoxes = false;
    end
    
    camName = 'PLE_OTR';
    
    figure(99); %colororder command creates a figure if none is there
    MPL_COLORS = {'#1f77b4','#ff7f0e'}; %MATPLOTLIB 2.0 default colors
    MPL_COLORS = colororder(MPL_COLORS); %#ok<NASGU>
    
    MATLAB_COLORS=colororder('default');
    
    MY_COLORS = MATLAB_COLORS; %#ok<NASGU>
    %MY_COLORS2 = [ MATLAB_COLORS(2,:); MATLAB_COLORS(1,:) ]; %Used for two-axis plots
    MY_COLORS2 = [ MATLAB_COLORS(1,:); MATLAB_COLORS(2,:) ];
    close(99);
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % cleaning helper function
    prc_cut = 90;
    prc_mask = @(m) m < prctile(m, prc_cut) & m > prctile(m, 100-prc_cut);
    
    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset)
        return; 
    end
    datasetInfo(dataset);
    disp(' ');
    
    % select measurement screen (OTR, spectrometer)
    camFields = fields(dataset.images);
    if numel(camFields) > 1
        if ~exist('camName','var')
            disp('Please provide camera name as argument');
            return;
        end
    else
        camName = camFields{1};
    end
    camStruct = dataset.images.(camName);
    %camDesc = camStruct.desc;
    
    % select current waveform
    if ~exist('inOut','var')
        scopeName = 'DIS_CUR_OUT';
    else
        if strcmp(inOut, 'in')
            scopeName = 'DIS_CUR_IN';
        elseif strcmp(inOut, 'out')
            scopeName = 'DIS_CUR_OUT';
        else
            disp("Expected inOut either 'in' or 'out'");
            return;
        end
    end
    fprintf('Using scopeName = %s\n\n', scopeName);
    currentStruct = dataset.scopes.(scopeName);
    
    I_thresh = 300.0;
    
%     beam_off_current_limit   = 400;  % [A]
    no_current_at_beam_limit =  10;  % [A]
    
    if ~exist('timeOffset_err', 'var')
        timeOffset_err = 1.0; % [ns] Assumed error in t_beam
    end
    
    if ~exist('nMC', 'var')
        nMC = 1000;              % Number of monte-carlo samples to run for error estimation on thick-lens gradient enhancement (0 to disable)
    end
    
    % get common camera variables
    if strcmp(camName, 'FAR_OTR')
        calib_x = 0.0153; % mm/px
        calib_y = 0.0167; % mm/px
    elseif strcmp(camName, 'PLE_OTR')
        %Valid until MAY19
        %calib_x = 0.0286; % mm/px
        %calib_y = -0.0305; % mm/px
        
        %From MAY19 ->
        %calib_x = 0.0368;
        %calib_y = -0.0389;
        
        %From OCT19 ->
        calib_x = 0.0395;
        calib_y = -0.0413;
        
        L_otr = 0.2895; % [m]
    end
    
    % color scheme (vertical / horizontal)
%     hcol = [0 0.447 0.741];
%     vcol = [0.85 0.325 0.098];
%     fcol = [0.5, 1.0, 0.5];
%     
    if isempty(strfind(dataset.metadata.scanName, 'Discharge fine timing'))
        disp('Not a timing scan!');
        return
    end
    
    % horizontal or vertical scan
    if strcmp(xy,'x')
        horscan = true;
        verscan = false;
    elseif strcmp(xy,'y')
        horscan = false;
        verscan = true;
    else
        disp('xy must be either x or y!');
        return;
    end
    
    
    %% POSITION REFERENCE OFF DATASET
    
    % load the reference dataset
    dataset_ref = getDataset(dataset_id_ref);
    if ~isstruct(dataset_ref)
        return; 
    end
    datasetInfo(dataset_ref);
    disp(' ');
    
    % grab same image structure from the reference dataset
    camStruct_ref = dataset_ref.images.(camName);
        
    % cycle all scan shots in the reference dataset
    numShots_ref = dataset_ref.metadata.numShots;
    [muxs_ref, muys_ref] = deal(nan(1,numShots_ref));
    parfor (i = 1:numShots_ref, numWorkers)
        
        %% IMAGE ANALYSIS
        
        % progress text
        if mod(i-1,50)==0 && i ~= 1
            disp(' ');
        end
        
        % get image
        image_ref = camStruct_ref.dat{i};
        
        %crop image
        if (~isnan(cropY(1)) && ~isnan(cropX(1))) && ~isempty(image_ref) %#ok<PFBNS>
            %assert (length(cropX) == 1)
            %assert (length(cropY) == 1)
            image_ref = image_ref(cropY,cropX);
        end
        
        % analyse image
        if visu
            figure(1);
            %set(gcf, 'color', 'w');
            %colormap(mod_jet());
        end
        [mux_ref, ~, muy_ref, ~] = image_anal(image_ref, visu);
        
        % calibrated beam positions
        muxs_ref(i) = mux_ref*calib_x; % [mm]
        muys_ref(i) = muy_ref*calib_y; % [mm]
        
        % display progress
        fprintf('.');
    end
    disp(' ');
    
    % find the reference beam positions
    mux0 = mean(muxs_ref(prc_mask(muxs_ref))); % [mm]
    muy0 = mean(muys_ref(prc_mask(muys_ref))); % [mm]
    fprintf('mux0 = %d [mm], muy0 = %d [mm]\n\n', mux0, muy0);
    
    %% ANALYZE THE SCAN
    
    % cycle all scan shots
    numShots = dataset.metadata.numShots;
    [muxs, muys, Ibeams, Imax, t_Ithresh] = deal(nan(1,numShots));
    parfor (i = 1:numShots, numWorkers)
        
        %% IMAGE ANALYSIS
        
        % progress text
        if mod(i-1,50)==0 && i ~= 1
            disp(' ');
        end
        
        % get image
        if visu
            figure(4);
        end
        image = camStruct.dat{i};
        
        % analyse image
        if ~isempty(image)
            if (~isnan(cropY(1)) && ~isnan(cropX(1))) %#ok<PFBNS>
                image = image(cropY,cropX);
            end

            [mux, ~, muy, ~] = image_anal(image, visu);
        else
            disp(['BadCam!', num2str(i)]);
            mux = NaN;
            muy = NaN;
        end

        % calibrated beam positions
        muxs(i) = mux*calib_x; % [mm]
        muys(i) = muy*calib_y; % [mm]
        
        % current analysis
        if visu
            figure(3);
        end
        if i < length(currentStruct.dat) && ~isempty(currentStruct.dat{i})
            [Ibeams(i), Imax(i), t_Ithresh(i)] = current_anal(currentStruct, i, visu, t_beam, I_thresh);
        else
            disp(['NoScope!' num2str(i)]);
            Ibeams(i) = NaN;
            Imax(i) = 0.0;
            t_Ithresh(i) = NaN;
        end
        
        % display progress
        fprintf('.');
        
    end
    disp(' ');
    
    
    %% DO STATISTICS
    
    % Position on screen
    steps = unique(dataset.scalars.step_num.dat);
    timeOffsets = unique(dataset.scalars.step_value.dat);
    [muxs_mean, muxs_err, muxs_num, muys_mean, muys_err, muys_num, Ibeams_mean, Ibeams_err, dIbeamsDt] = deal(zeros(1, numel(steps)));
    for j = 1:numel(steps)
        
        step_num = steps(j);
        %Select the shots that correspond to our step and are of OK quality
        step_mask = (dataset.scalars.step_num.dat == step_num & ~isnan(Ibeams));
        
        % get beam centroid positions in x,
        % filtering out bad shots (NaN) and outliers (percentile mask)
        step_muxs = muxs(~isnan(muxs) & step_mask);
        prcmask_x = prc_mask(step_muxs);
        muxs_mean(j) = mean(step_muxs(prcmask_x)); % [mm]
        muxs_err(j)  = std( step_muxs(prcmask_x)); % [mm]
        muxs_num(j)  = length(step_muxs(prcmask_x));
        
        % get beam centroid positions in y,
        % filtering out bad shots (NaN) and outliers (percentile mask)
        step_muys = muys(~isnan(muys) & step_mask);
        prcmask_y = prc_mask(step_muys);
        muys_mean(j) = mean(step_muys(prcmask_y)); % [mm]
        muys_err(j)  = std( step_muys(prcmask_y)); % [mm]
        muys_num(j)  = length(step_muys(prcmask_y));
        
        %Get currents for each step (step_mask should already be enough)
        step_Ibeams    = Ibeams(step_mask);
        %prcmask_Ibeams = prc_mask(step_Ibeams)
        prcmask_Ibeams = ones(1,length(step_Ibeams)); %Disable prc mask here for now
        Ibeams_mean(j) = mean(step_Ibeams(prcmask_Ibeams));
        Ibeams_err(j)  = std(step_Ibeams(prcmask_Ibeams));
    end
    
    % 1st order 1st derivatives to find errors from time offsets
    % (needs to look forward and backwards -> separate loop)
    for j = 1:numel(steps)
        if j == 1
            dIbeamsDt(j) = (Ibeams_mean(j+1) - Ibeams_mean(j)) ./ ( timeOffsets(j+1) - timeOffsets(j) );
        elseif j == numel(steps)
            dIbeamsDt(j) = (Ibeams_mean(j) - Ibeams_mean(j-1)) ./ ( timeOffsets(j) - timeOffsets(j-1) );
        else
            dIbeamsDt(j) = (Ibeams_mean(j+1) - Ibeams_mean(j-1)) ./ ( timeOffsets(j+1) - timeOffsets(j-1) );
        end
    end

    %drop data where the current is too low to compute gradient enhancement
%     timeOffsets_mask = abs(Ibeams_mean) > no_current_at_beam_limit;
    
%     timeOffsets = timeOffsets(timeOffsets_mask);
%     
%     muxs_mean   = muxs_mean(timeOffsets_mask);
%     muxs_err    = muxs_err(timeOffsets_mask);
%     muys_mean   = muys_mean(timeOffsets_mask);
%     muys_err    = muys_err(timeOffsets_mask);
%     
%     Ibeams_mean = Ibeams_mean(timeOffsets_mask);
%     Ibeams_err  = Ibeams_err(timeOffsets_mask);
%     dIbeamsDt   = dIbeamsDt(timeOffets_mask);
%     
    %% MAGNETIC FIELD CALIBRATION
    
    SI_c = 299792458; % [m/s] light speed
    SI_mu0 = 4*pi*1e-7; 
    
    % calculate the magnetic field calibration
    E = E_MeV;
    L_lens = 0.015;
    B_per_offset = 1e-3*E*1e6/(L_lens*L_otr*SI_c); % [T/mm]
    

    
    %% FINAL PLOTTING AND SCALING (THIN LENS ANALYSIS)
    
    % prep figure 10
    figure(10)
    global hObjs;
    global hLegends;
    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend))
        clf();
        f10=gcf();
        f10.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
        set(gcf,'color','w');
        
        clear global hObjs
        clear global hLegends
    else
        if isempty(hObjs)
            clf();
        end
    end
    hold on;
    set(gca(),'TickLabelInterpreter', 'latex')
    
    %Offset
    if horscan
        kickedPos = (muxs_mean-mux0);
        errPos    = muxs_err;
        numPos    = muxs_num;
    elseif verscan
        kickedPos = (muys_mean-muy0);
        errPos    = muys_err;
        numPos    = muys_num;
    else
        disp('WTF');
        return;
    end
    
    %Compute gradient from offset
    if Rx ~= Ry
        %Ramanujan approx of ellipse circumference
        circumferece = pi*( 3*(Rx+Ry)-sqrt((3*Rx+Ry)*(Rx+3*Ry)) ); %[um]
        if xy == 'x'
            Rthisplane = Rx;
        elseif xy == 'y'
            Rthisplane = Ry;
        end
    else
        circumferece = 2*pi*R;
        Rthisplane = R;
    end
    
    gKick    = kickedPos*B_per_offset/(Roff*1e-6); % [T/m]
    gCurrent = SI_mu0 * Ibeams_mean*(-I_uniform) / (Rthisplane * circumferece * 1e-12); %Circular apperture: (2 * pi * (R*1e-6)^2)
    gEnh     = gKick./gCurrent;
    
    colororder(MY_COLORS2);

    yyaxis left;
    p1 = plot(timeOffsets-t0, gEnh, 'LineWidth', 1.5);
    
    ylabel('Gradient enhancement','Interpreter','latex');
    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend)) || isempty(hObjs)
        %annotation('textarrow',[0.85,0.15],[0.15,0.15], 'String', 'Time')
        set(gca, 'XDir','reverse')
        
        yline(1.0,'--','Color', p1.Color);
        
        if t0 ~= 0.0
            xlabel(sprintf('CMB trigger delay (ns) - %.0f (ns)',t0),'Interpreter','latex')
        else
            xlabel(sprintf('CMB trigger delay (ns)'),'Interpreter','latex')
        end
    end
    
    yyaxis right;
    p2 = errorbar(timeOffsets-t0, Ibeams_mean, Ibeams_err, 'LineStyle', p1.LineStyle, 'LineWidth', 1.5);
    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend)) || isempty(hObjs)
        yline(0.0,'--');
        
        ylabel('Current at $t_{beam}$ [A]', 'Interpreter','latex');
    end
    
    yyaxis left;

    
    %Make the legend
    if strcmp(xy, 'x')
        hv = 'H';
    else
        hv = 'V';
    end
    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend))
        legend([p1, p2], ...
               {'Gradient enhancement', ...
                '$I\left(t_{beam}\right)$ [A]'}, ...
               'Location', 'northeast','Interpreter','latex');
        title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));
    else
        if strcmp(holdLegend, '') %Autolegend
            holdLegend = sprintf('%s (%s), R_{off} = %.0f, %s', dataset_id, hv,Roff, inOut);
        end
        
        if isempty(hObjs)
            hObjs = [p1];
            hLegends = {holdLegend};
        else
            hObjs(length(hObjs)+1) = p1;
            hLegends(length(hLegends)+1) = {holdLegend};
        end
        legend(hObjs, hLegends, 'Location', 'northeast','Interpreter','latex');
    end

    %% THICK LENS ANALYSIS
    optimset_quiet_options = optimset('Display','off');
    
    % Linear-in-parameters fit (fig 21)                                    %%%%%%%%%%%%%%%%%%%%
    disp(' ')
    gExp = SI_mu0 * Ibeams_mean*(-I_uniform) / (Rthisplane*circumferece*1e-12); % [T/m]
    sqK = sqrt(gExp*SI_c/(E_MeV*1e6));                             % [1/m]
    A = cos(sqK*L_lens) - sqK.*(L_otr-L_lens/2).*sin(sqK*L_lens);
    B = sin(sqK*L_lens)./sqK + (L_otr-L_lens/2).*cos(sqK*L_lens);
    M = [A',B'];
    thickFit = mldivide(M, 1e-3*(kickedPos)'); % [m,rad]
    
    disp(['Thick fit / unweighted linear:'])
    disp(['Fitted y_i         = ', num2str(thickFit(1)*1e6), ' [um]'])
    disp(['Fitted y''_i        = ', num2str(thickFit(2)*1e3), ' [mrad]'])
    disp(['Fitted y_i at Lq/2 = ', num2str((thickFit(1)+thickFit(2)*L_lens/2)*1e6), ' [um]'])
    disp(['Fitted y_f at I=0  = ', num2str((thickFit(1)+thickFit(2)*(L_lens/2+L_otr))*1e3), ' [mm]'])
    
    figure(21)
    clf()
    f21=gcf();
    f21.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
    colororder('default');
    hold on;
    set(gca(),'TickLabelInterpreter', 'latex')

    p1 = errorbar(gExp,kickedPos, errPos./sqrt(numPos), '+');
    
    xl = xlim();
    %I_scan    = xl(1):10:xl(2);
    %gScan     = SI_mu0 * I_scan / (Rthisplane*circumferece*1e-12); % [T/m]
    gScan     = xl(1):10:xl(2);
    sqK_scan  = sqrt(gScan * SI_c / (E_MeV*1e6) );
    A_scan    = cos(sqK_scan*L_lens) - sqK_scan.*(L_otr-L_lens/2).*sin(sqK_scan*L_lens);
    B_scan    = sin(sqK_scan*L_lens)./sqK_scan + (L_otr-L_lens/2).*cos(sqK_scan*L_lens);
    predictedPos = A_scan*thickFit(1) + B_scan*thickFit(2);
    p2 = plot(gScan, predictedPos*1e3);
    
    % Note: In reverse order, to match "Time" in previous plot
    addArrows(gExp(end:-1:1),kickedPos(end:-1:1));

    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));
    xlabel('Expected gradient [T/m] from current and geometry','Interpreter','latex')
    ylabel('Displacement on screen $\Delta y$ [mm]', 'Interpreter','latex')
      
    h = legend([p1,p2], ...
        {'Measurement',sprintf('%.0f [um] * $A(g(I))$ + %.2f [mrad] * $B(g(I))$',thickFit(1)*1e6,thickFit(2)*1e3)}, ...
        'Location', 'northwest','Interpreter','latex');
    h.Box='off';
    annotation('arrow', [h.Position(1)+0.01, h.Position(1)+0.08], [h.Position(2),h.Position(2)]-0.025)
    annotation('textbox', [h.Position(1)+0.08, h.Position(2)-0.05, 0.25, 0.05], 'String', 'Time', 'LineStyle', 'none', 'Interpreter','latex');

    if (showInfoBoxes)
        annotation('textbox', [0.6, 0.25, 0.1, 0.1], 'String', sprintf(' y_0 free\n y_0'' free\n g_{enh} = 1.0\n UNWEIGHTED'));
    end
    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));
    
    %Diagnostic plot -- what does really A(I) and B(I) look like?
    figure(41)
    clf()
    f41=gcf();
    f41.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
    set(gca(),'TickLabelInterpreter', 'latex')
    
    yyaxis left;
    plot(gScan,A_scan)
    ylabel('$A(I)$ [mm/mm]','Interpreter','latex')
    yyaxis right;
    plot(gScan,B_scan)
    ylabel('$B(I)$ [mm/mrad]', 'Interpreter','latex')
    yyaxis left
    xlabel('Expected gradient [T/m] from current and geometry','Interpreter','latex')
    %NOTE: See that effect of angle is TINY -- on the order of 0.1 mm/mrad
    %      deviation from the reference
    
    % Weighted version of above (fig 22)                                   %%%%%%%%%%%%%%%%%%%%
    disp(' ')
    thickFit2 = lscov(M, 1e-3 * kickedPos', 1./((errPos./sqrt(numPos)).^2)); % [m,rad]; use error-on-mean for weights
    
    disp( 'Thick fit / weighted linear:' )
    disp(['Fitted y_i         = ', num2str(thickFit2(1)*1e6), ' [um]'])
    disp(['Fitted y''_i        = ', num2str(thickFit2(2)*1e3), ' [mrad]'])
    disp(['Fitted y_i at Lq/2 = ', num2str((thickFit2(1)+thickFit2(2)*L_lens/2)*1e6), ' [um]'])
    disp(['Fitted y_f at I=0  = ', num2str((thickFit2(1)+thickFit2(2)*(L_lens/2+L_otr))*1e3), ' [mm]'])
    
    figure(22)
    clf()
    f22=gcf();
    f22.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
    colororder(MY_COLORS2);
    set(gcf,'color','w');
    hold on
    set(gca(),'TickLabelInterpreter', 'latex')
    
    p1 = errorbar(gExp,kickedPos, errPos./sqrt(numPos), '+');
    
    predictedPos = A_scan*thickFit2(1) + B_scan*thickFit2(2);
    p2 = plot(gScan, predictedPos*1e3);
    
    % Note: In reverse order, to match "Time" in previous plot
    addArrows(gExp(end:-1:1),kickedPos(end:-1:1));

    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));
    xlabel('Expected gradient (T/m) from current and geometry','Interpreter','latex')
    ylabel('$\Delta y$ (mm)','Interpreter','latex')
      
    h = legend([p1,p2], ...
        {'Measurement',...
        ... %sprintf('%.0f ($\\mu$m) * $A(g(I))$ + %.2f (mrad) * $B(g(I))$',thickFit2(1)*1e6,thickFit2(2)*1e3)}, ...
        sprintf("Fit: $y = %.0f$ ($\\mu$m), $y' = %.2f$ (mrad); $g_{enh} \\equiv 1.0$", thickFit2(1)*1e6, thickFit2(2)*1e3)
        }, ...
        'Location', 'northwest','Interpreter','latex');
    h.Box='off';
    annotation('arrow', [h.Position(1)+0.01, h.Position(1)+0.08], [h.Position(2),h.Position(2)]-0.025, 'LineWidth', 0.25, 'HeadWidth', 5)
    annotation('textbox', [h.Position(1)+0.08, h.Position(2)-0.05, 0.25, 0.05], 'String', 'Time', 'LineStyle', 'none', 'Interpreter','latex');

    if (showInfoBoxes)
        annotation('textbox', [0.6, 0.2, 0.1, 0.1], 'String', sprintf(' y_0 free\n y_0'' free\n g_{enh} = 1.0'));
    end
    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));

    figure (32) % Residuals                                                --------------------
    clf()
    colororder(MY_COLORS2);
    set(gcf,'color','w');
    set(gca(),'TickLabelInterpreter', 'latex')
    
    yyaxis left;
    dyKick = kickedPos/1e3-(A*thickFit2(1) + B*thickFit2(2)); %[m]
    p1 = errorbar(timeOffsets-t0, -dyKick*1e3, errPos,'+');
    yline(0.0,'--','Color', p1.Color);
    ylabel('Residual [mm]','Interpreter','latex');
    xlabel(sprintf('CMB trigger delay (ns) - %.0f (ns)',t0),'Interpreter','latex')

    yyaxis right;
    p2 = errorbar(timeOffsets-t0, Ibeams_mean, Ibeams_err);
    ylabel('Current at $t_{beam}$ (A)','Interpreter','latex');

    if (showInfoBoxes)
        annotation('textbox', [0.35, 0.2, 0.1, 0.1], 'String', sprintf(' y_0 free\n y_0'' free\n g_{enh} = 1.0'));
    end
    %annotation('textarrow',[0.85,0.15],[0.15,0.15], 'String', 'Time')
    set(gca, 'XDir','reverse')
    
    % Gradient enhancement (fig 12)                                        --------------------
    function rem = root_gEnh_thick(sqK_,kickedPos_)
        A = cos(sqK_*L_lens) - sqK_.*(L_otr-L_lens/2).*sin(sqK_*L_lens);
        B = sin(sqK_*L_lens)./sqK_ + (L_otr-L_lens/2).*cos(sqK_*L_lens);
        yf_ = A*thickFit2(1) + B*thickFit2(2);
        rem = kickedPos_ - yf_*1e3; %[mm]
    end
    g_fit        = zeros(1,length(Ibeams_mean));
    gEnh_fit     = zeros(1,length(Ibeams_mean));
    g_fit_err    = zeros(1,length(Ibeams_mean));
    for i=1:length(Ibeams_mean) %Can't use PARFOR because it doesn't support nested functions...
        [sqK_fit, ~, fsolve_exitflag] = fsolve(@(sqK_)root_gEnh_thick(sqK_,kickedPos(i)), sqK(i), optimset_quiet_options);
        if fsolve_exitflag > 0
            g_fit(i) = sqK_fit.^2 * E_MeV*1e6 / SI_c;
        else
            disp(['Bad solve at i=',num2str(i), '/', num2str(length(Ibeams_mean)), ', exitflag=',num2str(fsolve_exitflag)])
            g_fit(i) = NaN;
        end
        gEnh_fit(i) = g_fit(i)/gExp(i);
        
        %Error estimation of g_fit through MC
        if isnan(g_fit(i))
            g_fit_err(i) = NaN;
        else
            if nMC > 0
                disp(['Running MC error estimate for g_fit, i=',num2str(i),', nMC=',num2str(nMC)])
                g_fit_MCsamples = zeros(1,nMC);

                for k = 1:nMC
                    kickedPos_rnd = normrnd(kickedPos(i),errPos(i)./sqrt(numPos(i))); %Use estimated-error-on-mean
                    [sqK_fit, ~, fsolve_exitflag] = fsolve(@(sqK_)root_gEnh_thick(sqK_,kickedPos_rnd), sqK(i), optimset_quiet_options);
                    if fsolve_exitflag > 0
                        g_fit_MCsamples(k) = sqK_fit.^2 * E_MeV*1e6 / SI_c;
                    else
                        disp(['Bad solve at i=',num2str(i),'/',num2str(length(Ibeams_mean)),', k=',num2str(k), '/', num2str(nMC), ', exitflag=',num2str(fsolve_exitflag)])
                        g_fit_MCsamples(k) = NaN;
                    end
                end
                
                g_fit_err(i) = std(g_fit_MCsamples);
            end
        end
                
    end
    
    %Error of gradient enhancement due to time offset in the current + error due to error in pos
    gEnh_fit_err = sqrt( (gEnh_fit ./ Ibeams_mean*(-I_uniform) .* dIbeamsDt * timeOffset_err).^2 ...
        + (gEnh_fit./g_fit.*g_fit_err).^2 );
    
    %Average of all time measurements -- mixture distribution
    gEnh_fit_good = ~isnan(gEnh_fit);
    gEnh_fit_ave = mean(gEnh_fit(gEnh_fit_good));
    gEnh_fit_var = sum(gEnh_fit_err(gEnh_fit_good).^2 ...
        + gEnh_fit(gEnh_fit_good).^2)/sum(gEnh_fit_good)-gEnh_fit_ave^2;
    disp(['Average gEnh = ', num2str(gEnh_fit_ave), ' sigma = ', num2str(sqrt(gEnh_fit_var))]);
    
    %Average of all time measurements -- weighted mean
    gEnh_fit_ave2 = sum(gEnh_fit(gEnh_fit_good) ./ gEnh_fit_err(gEnh_fit_good).^2 ) / ...
                            sum(1.0 ./ gEnh_fit_err(gEnh_fit_good).^2 );
    gEnh_fit_var2 = sqrt( 1.0 / sum(1.0 ./ gEnh_fit_err(gEnh_fit_good).^2 ) );
    disp(['Average gEnh = ', num2str(gEnh_fit_ave2), ' sigma = ', num2str(sqrt(gEnh_fit_var2)), ' (weighted mean)']);
    
    figure(12)
    clf()
    f12=gcf();
    f12.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
    colororder(MY_COLORS2);
    set(gcf,'color','w');
    set(gca(),'TickLabelInterpreter', 'latex')
    
    legend_cont_12 = [];
    
    yyaxis left;
    %p1 = plot(timeOffsets-t0,gEnh_fit);
    p1 = errorbar(timeOffsets-t0, gEnh_fit, gEnh_fit_err, '+', 'DisplayName', '$g_{enh}(t)$', 'LineWidth', 1.0);
    legend_cont_12(1) = p1;
    yline(1.0,'--','Color', p1.Color);
    
    ylabel('Gradient enhancement $g_{enh}(t)$','Interpreter','latex');
    xlabel(sprintf('CMB trigger delay (ns) - %.0f (ns)',t0),'Interpreter','latex')
    
    ylim([0.6,1.4])
    
    yyaxis right;
    p2 = plot(timeOffsets-t0, Ibeams_mean, 'DisplayName', '$I\left(t_{beam}\right)$');
    %p2 = errorbar(timeOffsets-t0, Ibeams_mean, Ibeams_err,'-+', 'DisplayName', '$I\left(t_{beam}\right)$');
    legend_cont_12(2) = p2;
    ylabel('Current seen by beam $I(t_{beam})$ [A]','Interpreter','latex');

    if (showInfoBoxes)
        annotation('textbox', [0.35, 0.15, 0.1, 0.1], 'String', sprintf(' y_0 = %.0f [um]\n y_0'' = %.2f [mrad]', thickFit2(1)*1e6, thickFit2(2)*1e3));
    end
    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));
    
    set(gca, 'XDir','reverse')
    
    yyaxis left;
    if ~isnan(extraPlotPoints_t)
        hold on
        p3 = plot(extraPlotPoints_t, extraPlotPoints_genh, '*', 'DisplayName', 'Offset scan','MarkerSize', 10, 'LineWidth', 2);
        legend_cont_12(3) = p3;
        hold off
    end
    
    h = legend(legend_cont_12,'Location','northwest','Interpreter','latex');
    h.Box = 'off';
    h.Position(1) = 0.5;
       
    % Now try to unlock the gradient enhancement, while keeping the        %%%%%%%%%%%%%%%%%%%%
    % position nominal. This requires a NONLINEAR FIT                      %%%%%%%%%%%%%%%%%%%%
    disp(' ')
    function [res] = thickFit_residuals1(xIn)
        sqK_ = sqrt(xIn(2)*gExp*SI_c/(E_MeV*1e6));                             % [1/m]
        A = cos(sqK_*L_lens) - sqK_.*(L_otr-L_lens/2).*sin(sqK_*L_lens);
        B = sin(sqK_*L_lens)./sqK_ + (L_otr-L_lens/2).*cos(sqK_*L_lens);
        yf_ = A*(-Roff*1e-6) + B*xIn(1);
        
        res = kickedPos - yf_*1e3; % [mm]
        res = res./errPos;
    end
    thickFit_nonlin1 = lsqnonlin(@thickFit_residuals1, [thickFit(2),1.0], [-20e-3,0.01],[20e-3,2.0], optimset_quiet_options);
    
    disp(['Thick fit / weighted nonlinear:'])
    disp(['Assumed y_i         = ', num2str(Roff), ' [um]'])
    disp(['Fitted y''_i         = ', num2str(thickFit_nonlin1(1)*1e3), ' [mrad]'])
    disp(['Fitted gEnh         = ', num2str(thickFit_nonlin1(2))])
    disp(['Fitted y_f at I=0   = ', num2str((Roff/1e6+thickFit_nonlin1(1)*(L_lens+L_otr))*1e3), ' [mm]'])
    
    % Plot
    figure(23)
    clf()
    f23=gcf();
    f23.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
    %colororder('default');
    colororder(MY_COLORS2);
    set(gcf,'color','w');
    hold on;
    set(gca(),'TickLabelInterpreter', 'latex')

    p1 = errorbar(Ibeams_mean, kickedPos, errPos./sqrt(numPos), '+');
    
    xl = xlim();
    I_scan    = xl(1):10:xl(2);
    gScan     = SI_mu0 * I_scan / (Rthisplane*circumferece*1e-12); % [T/m]
    sqK_scan  = sqrt(thickFit_nonlin1(2)*gScan * SI_c / (E_MeV*1e6) );
    A_scan    = cos(sqK_scan*L_lens) - sqK_scan.*(L_otr-L_lens/2).*sin(sqK_scan*L_lens);
    B_scan    = sin(sqK_scan*L_lens)./sqK_scan + (L_otr-L_lens/2).*cos(sqK_scan*L_lens);
    predictedPos = A_scan*(-Roff*1e-6) + B_scan*thickFit_nonlin1(1);
    p2 = plot(I_scan, predictedPos*1e3);
    
    addArrows(Ibeams_mean(end:-1:1),kickedPos(end:-1:1));

    xlabel('$I_{beam}$ (A)','Interpreter','latex')
    ylabel('$\Delta y_f$ (mm)','Interpreter','latex')
    
    h = legend([p1,p2], ...
        {'Measurement', ...
        ... %sprintf('%.0f ($\\mu$m) * $A(%0.2f * g(I))$ + %.2f (mrad) * $B(%0.2f * g(I))$',Roff*sign(thickFit(1)),thickFit_nonlin1(2), thickFit_nonlin1(1)*1e3,thickFit_nonlin1(2))}, ...
        ... %sprintf("Fit: $y'=%.2f$ (mrad), $g_{enh}=%0.2f$; $y \\equiv %0.f$ ($\\mu$m)", thickFit_nonlin1(2), thickFit_nonlin1(1)*1e3, Roff*sign(thickFit(1))) }, ...
        'Fitted nonlinear model'}, ...
        'Location', 'northwest','Interpreter','latex');
    h.Box='off';
    annotation('arrow', [h.Position(1)+0.01, h.Position(1)+0.07], [h.Position(2),h.Position(2)]-0.03, 'LineWidth', 0.25, 'HeadWidth', 5);
    ha = annotation('textbox', [h.Position(1)+0.07, h.Position(2)-0.055, 0.25, 0.05], 'String', 'Time', 'LineStyle', 'none', 'Interpreter','latex');
    ha.FontSize=h.FontSize;
    
    if (showInfoBoxes)
        annotation('textbox', [0.6, 0.2, 0.25, 0.2], 'String', sprintf(' y_0 = %.0f [um]\n y_0'' free\n g_{enh} free', Roff*sign(thickFit(1))));
    end
    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));

    figure (33) % Residuals                                                --------------------
    clf()
    colororder(MY_COLORS2);
    set(gcf,'color','w');
    f33=gcf();
    f33.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
    set(gca(),'TickLabelInterpreter', 'latex')
    
    yyaxis left;
    sqK = sqrt(thickFit_nonlin1(2)*gExp*SI_c/(E_MeV*1e6));                             % [1/m]
    A = cos(sqK*L_lens) - sqK.*(L_otr-L_lens/2).*sin(sqK*L_lens);
    B = sin(sqK*L_lens)./sqK + (L_otr-L_lens/2).*cos(sqK*L_lens);
    yf = A*(-Roff*1e-6) + B*thickFit_nonlin1(1);
    res = kickedPos - yf*1e3; %[mm]
    p1 = errorbar(timeOffsets-t0, -res, errPos,'+');
    yline(0.0,'--','Color', p1.Color);
    ylabel('Residual [mm]','Interpreter','latex');
    xlabel(sprintf('CMB trigger delay (ns) - %.0f (ns)',t0),'Interpreter','latex')

    yyaxis right;
    p2 = errorbar(timeOffsets-t0, Ibeams_mean, Ibeams_err);
    ylabel('Current at $t_{beam}$ [A]','Interpreter','latex');

    if (showInfoBoxes)
        annotation('textbox', [0.35, 0.2, 0.25, 0.2], 'String', sprintf(' y_0 = %.0f [um]\n y_0'' free\n g_{enh} free', Roff*sign(thickFit(1))));
    end
    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));
    
    %annotation('textarrow',[0.85,0.15],[0.15,0.15], 'String', 'Time')
    set(gca, 'XDir','reverse')
    
    % Gradient enhancement (fig 13)                                        --------------------
    function rem = root_gEnh_thick_nonlin1(sqK_,kickedPos_)
        A = cos(sqK_*L_lens) - sqK_.*(L_otr-L_lens/2).*sin(sqK_*L_lens);
        B = sin(sqK_*L_lens)./sqK_ + (L_otr-L_lens/2).*cos(sqK_*L_lens);
        yf_ = A*(-Roff*1e-6) + B*thickFit_nonlin1(1);
        rem = kickedPos_ - yf_*1e3; %[mm]
    end
    g_fit_nonlin1     = zeros(1,length(Ibeams_mean));
    gEnh_fit_nonlin1  = zeros(1,length(Ibeams_mean));
    g_fit_err_nonlin1 = zeros(1,length(Ibeams_mean));
    sqK = sqrt(gExp*SI_c/(E_MeV*1e6));                             % [1/m]
    for i=1:length(Ibeams_mean)
        [sqK_fit, ~, fsolve_exitflag] = fsolve(@(sqK_)root_gEnh_thick_nonlin1(sqK_,kickedPos(i)), sqK(i),optimset_quiet_options);
        if fsolve_exitflag > 0
            g_fit_nonlin1(i) = sqK_fit.^2 * E_MeV*1e6 / SI_c;
        else
            g_fit_nonlin1(i) = NaN;
        end
        gEnh_fit_nonlin1(i) = g_fit_nonlin1(i)/gExp(i);
        
        %Error estimation of g_fit through MC
        if isnan(g_fit(i))
            g_fit_err_nonlin1(i) = NaN;
        else
            if nMC > 0
                disp(['Running MC error estimate for g_fit, i=',num2str(i),', nMC=',num2str(nMC)])
                g_fit_MCsamples = zeros(1,nMC);

                for k = 1:nMC
                    kickedPos_rnd = normrnd(kickedPos(i),errPos(i)./sqrt(numPos(i))); %Use estimated-error-on-mean
                    [sqK_fit, ~, fsolve_exitflag] = fsolve(@(sqK_)root_gEnh_thick_nonlin1(sqK_,kickedPos_rnd), sqK(i),optimset_quiet_options);
                    if fsolve_exitflag > 0
                        g_fit_MCsamples(k) = sqK_fit.^2 * E_MeV*1e6 / SI_c;
                    else
                        disp(['Bad solve at i=',num2str(i),'/',num2str(length(Ibeams_mean)),', k=',num2str(k), '/', num2str(nMC), ', exitflag=',num2str(fsolve_exitflag)])
                        g_fit_MCsamples(k) = NaN;
                    end
                end
                
                g_fit_err_nonlin1(i) = std(g_fit_MCsamples);
            end
        end
    end
    
    %Error of gradient enhancement due to time offset in the current + error due to error in pos
    gEnh_fit_err_nonlin1 = sqrt( (gEnh_fit_nonlin1 ./ Ibeams_mean*(-I_uniform) .* dIbeamsDt * timeOffset_err).^2 ...
        + (gEnh_fit_nonlin1./g_fit_nonlin1.*g_fit_err_nonlin1).^2 );
    
    %Average of all time measurements -- mixture distribution
    gEnh_fit_nonlin1_good = ~isnan(gEnh_fit_nonlin1);
    gEnh_fit_nonlin1_ave = mean(gEnh_fit_nonlin1(gEnh_fit_nonlin1_good));
    gEnh_fit_nonlin1_var = sum(gEnh_fit_err_nonlin1(gEnh_fit_nonlin1_good).^2 ...
        + gEnh_fit_nonlin1(gEnh_fit_nonlin1_good).^2)/sum(gEnh_fit_nonlin1_good)-gEnh_fit_nonlin1_ave^2;
    disp(['Average gEnh = ', num2str(gEnh_fit_nonlin1_ave), ' sigma = ', num2str(sqrt(gEnh_fit_nonlin1_var)), ' (mixture distribution)']);
    
    %Average of all time measurements -- weighted mean
    gEnh_fit_nonlin1_ave2 = sum(gEnh_fit_nonlin1(gEnh_fit_nonlin1_good) ./ gEnh_fit_err_nonlin1(gEnh_fit_nonlin1_good).^2 ) / ...
                            sum(1.0 ./ gEnh_fit_err_nonlin1(gEnh_fit_nonlin1_good).^2 );
    gEnh_fit_nonlin1_var2 = sqrt( 1.0 / sum(1.0 ./ gEnh_fit_err_nonlin1(gEnh_fit_nonlin1_good).^2 ) );
    disp(['Average gEnh = ', num2str(gEnh_fit_nonlin1_ave2), ' sigma = ', num2str(sqrt(gEnh_fit_nonlin1_var2)), ' (weighted mean)']);
    
    figure(13) 
    clf()
    f13=gcf();
    f13.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
    colororder(MY_COLORS2);
    set(gcf,'color','w');
    set(gca(),'TickLabelInterpreter', 'latex')
    
    legend_cont_13 = [];

    yyaxis left;
    % p1 = plot(timeOffsets-t0,gEnh_fit_nonlin1);
    p1 = errorbar(timeOffsets-t0, gEnh_fit_nonlin1, gEnh_fit_err_nonlin1, '+', 'DisplayName', '$g_{enh}(t)$', 'LineWidth', 1.0);
    legend_cont_13(1) = p1;
    yline(1.0,'--','Color', p1.Color);
    ylabel('Gradient enhancement $g_{enh}(t)$','Interpreter','latex');
    xlabel(sprintf('CMB trigger delay (ns) - %.0f (ns)',t0),'Interpreter','latex')
    
    ylim([0.6,1.4])
    
    yyaxis right;
    p2 = plot(timeOffsets-t0, Ibeams_mean, '-', 'DisplayName', '$I\left(t_{beam}\right)$');
    %p2 = errorbar(timeOffsets-t0, Ibeams_mean,Ibeams_err, '+-', 'DisplayName', '$I\left(t_{beam}\right)$');
    legend_cont_13(2) = p2;
    ylabel('Current seen by beam $I(t_{beam})$ [A]','Interpreter','latex');

    if (showInfoBoxes)
        annotation('textbox', [0.35, 0.15, 0.1, 0.1], 'String', sprintf(' y_0 = %.0f [um]\n y_0'' = %.2f [mrad]', Roff*sign(thickFit(1)), thickFit_nonlin1(1)*1e3));
    end
    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));
    
    %annotation('textarrow',[0.85,0.15],[0.15,0.15], 'String', 'Time')
    set(gca, 'XDir','reverse')
    
    yyaxis left;
    if ~isnan(extraPlotPoints_t)
        hold on
        p3 = plot(extraPlotPoints_t, extraPlotPoints_genh, '*', 'DisplayName', 'Offset scan','MarkerSize', 10, 'LineWidth', 2);
        legend_cont_13(3) = p3;
        hold off
    end
    
    h = legend(legend_cont_13,'Location','northwest','Interpreter','latex');
    h.Box = 'off';
    h.Position(1) = 0.5;
    
    % Now try to also unlock the initial position.                         %%%%%%%%%%%%%%%%%%%%
    disp(' ')
    function [res] = thickFit_residuals2(xIn)
        sqK = sqrt(xIn(3)*gExp*SI_c/(E_MeV*1e6));                             % [1/m]
        A = cos(sqK*L_lens) - sqK.*(L_otr-L_lens/2).*sin(sqK*L_lens);
        B = sin(sqK*L_lens)./sqK + (L_otr-L_lens/2).*cos(sqK*L_lens);
        yf_ = A*xIn(1) + B*xIn(2);
        
        res = kickedPos - yf_*1e3;
        res = res./errPos;
    end
    thickFit_nonlin2 = lsqnonlin(@thickFit_residuals2, [Roff*1e-6*sign(thickFit(1)), thickFit(2),1.0], [-1.5e-3,-20e-3,0.1],[1.5e-3,20e-3,2.0], optimset_quiet_options);

    disp(['Thick fit / weighted nonlinear #2:'])
    disp(['Fitted y_i          = ', num2str(thickFit_nonlin2(1)*1e6), ' [um]'])
    disp(['Fitted y''_i         = ', num2str(thickFit_nonlin2(2)*1e3), ' [mrad]'])
    disp(['Fitted gEng         = ', num2str(thickFit_nonlin2(3))])
    disp(['Fitted y_f at I=0  = ', num2str((thickFit_nonlin2(1)+thickFit_nonlin2(2)*(L_lens+L_otr))*1e3), ' [mm]'])
    
    % Plot
    figure(24)
    clf()
    f24=gcf();
    f24.Renderer='opengl'; %Prevent extra space stupidity when exporting to PDF
    colororder('default');
    hold on;
    set(gca(),'TickLabelInterpreter', 'latex')
    
    p1 = errorbar(Ibeams_mean, kickedPos, errPos./sqrt(numPos), '+');

    xl = xlim();
    I_scan    = xl(1):10:xl(2);
    gScan     = SI_mu0 * I_scan / (Rthisplane*circumferece*1e-12); % [T/m]
    sqK_scan  = sqrt(thickFit_nonlin2(3)*gScan * SI_c / (E_MeV*1e6) );
    A_scan    = cos(sqK_scan*L_lens) - sqK_scan.*(L_otr-L_lens/2).*sin(sqK_scan*L_lens);
    B_scan    = sin(sqK_scan*L_lens)./sqK_scan + (L_otr-L_lens/2).*cos(sqK_scan*L_lens);
    predictedPos = A_scan*thickFit_nonlin2(1) + B_scan*thickFit_nonlin2(2);
    p2 = plot(I_scan, predictedPos*1e3);

    addArrows(Ibeams_mean(end:-1:1),kickedPos(end:-1:1));

    xlabel('$I_{beam}$ [A]','Interpreter','latex')
    ylabel('$\Delta y_f$ [mm]','Interpreter','latex')
    
    h = legend([p1,p2], ...
        {'Measurement',sprintf('%.0f [um] * $A(%0.2f * g(I))$ + %.2f [mrad] * $B(%0.2f * g(I))$',thickFit_nonlin2(1)*1e6,thickFit_nonlin2(3), thickFit_nonlin2(2)*1e3,thickFit_nonlin2(3))}, ...
        'Location', 'northwest','Interpreter','latex');
    h.Box='off';
    annotation('arrow', [h.Position(1)+0.01, h.Position(1)+0.08], [h.Position(2),h.Position(2)]-0.025, 'LineWidth', 0.25, 'HeadWidth', 5)
    annotation('textbox', [h.Position(1)+0.08, h.Position(2)-0.05, 0.25, 0.05], 'String', 'Time', 'LineStyle', 'none', 'Interpreter','latex');
    
    if (showInfoBoxes)
        annotation('textbox', [0.6, 0.2, 0.1, 0.1], 'String', sprintf(' y_0 free\n y_0'' free\n g_{enh} free'));
    end
    title(sprintf('Scan %s (%s), R_{off} = %.0f [um], %s', dataset_id, hv,Roff, inOut));
end

function addArrows(vX, vY)
    % FRAGILE: Depends on plot xlim/ylim!
    rMag  = 1.0; % How long should the arrow be (fraction of point distance)?

    xl = xlim();
    yl = ylim();

    axisPos = findobj(gcf,'Type','axes').Position;
    
    xScale = 1.0/(xl(2)-xl(1));
    yScale = 1.0/(yl(2)-yl(1));

    hold on;
    for i=1:(length(vX)-1)
        dx = vX(i+1) - vX(i);
        dy = vY(i+1) - vY(i);

        %Scale to 0..1
        Xv = ([vX(i), vX(i)+dx*rMag]-xl(1))*xScale;
        Yv = ([vY(i), vY(i)+dy*rMag]-yl(1))*yScale;
        
        %Scale to within axis box
        Xv = Xv*axisPos(3)+axisPos(1);
        Yv = Yv*axisPos(4)+axisPos(2);
        
        annotation('arrow', Xv, Yv, 'LineWidth', 0.15, 'HeadWidth', 3);
    end
    
end

