function [num_shots, ...
    var_thresh_in, var_thresh_out, var_cumsum_in, var_cumsum_out, ...
    mean_max_in, mean_max_out, mean_max_in_smooth, mean_max_out_smooth, ...
    var_max_in, var_max_out, max_max_in, max_max_out, med_max_in, med_max_out, ...
    thresh_in_array, thresh_out_array, cumsum_in_array, cumsum_out_array,...
    max_in_array, max_out_array, max_in_smooth_array, max_out_smooth_array, ...
    dataset] ...
    = marxBankJitter(dataset_id, doPlot, curr_ylim, hist_nbins, time_xlim, holdOn, I_threshold_in, dataset_legend, doDump)
    % MARXBANKJITTER Function to calculate current jitter related quantities for a single dataset.
    %
    % INPUT:
    %     dataset_id     : Numerical ID of the dataset to load
    %
    % OPTIONAL INPUT:
    %     doPlot         : Draw the plots or just generate return values
    %                      (bool, default = true)
    %                      Alternative: List of figures to actually show
    %                      Plots:
    %                      Fig.  1 : 'Raw current signals'
    %                      Fig.  2 : 'Smoothed current signals'
    %                      Fig.  3 : 'Raw current signals (alligned to threshold)'
    %                      Fig.  4 : 'Raw current signals (alligned to cumsum 50%)'
    %                      Fig. 10 : 'Threshold time [ns]'
    %                      Fig. 11 : 'Cumsum 50% time [ns]'
    %                      Fig. 12 : 'Peak current (raw max) [A]'
    %                      Fig. 13 : 'Peak current (raw max)'      VS shot idx
    %                      Fig. 14 : 'Threshold time'              VS shot idx
    %                      Fig. 15 : 'Cumsum 50% time'             VS shot idx
    %
    %     curr_ylim      : Y-limits for current plots
    %                      (length 2 vector, default: autoscale)
    %
    %     hist_nbins     : Number of bins for histogramming.
    %                      NaN for autobinning
    %                      (int, default = auto)
    %
    %     time_xlim      : X-limits for current plots
    %                      (length 2 vector, default: autoscale)
    %
    %     holdOn         : Do not clear figures
    %                      (bool, default = false, clear all figures)
    %
    %     I_threshold_in : Current threshold for alligment
    %                      (float, default = 300)
    %
    %     dataset_legend : Legend to use for this dataset;
    %                      only makes sense if holdOn is true.
    %                      Forces all the current curves in a dataset to
    %                      the same color. Set to NaN to ignore.
    %                      (string)
    %                      Note: It is reccommended to call
    %                      marxBankJitter_fixHistScales afterwards to
    %                      equalize the ranges of figure 10 and onwards.
    %
    %     doDump         : If true, dump the data as a set of ascii files.
    %                      (bool, default = false)

    showPlotsList = NaN;
    if ~exist('doPlot','var')
        doPlot = true;
    else
        if ~isa(doPlot,'logical')
            showPlotsList = doPlot;
            doPlot = true;
        end
    end
    
    
    if ~exist('hist_nbins','var')
        hist_nbins = NaN; %Autobinning
    end
    if ~exist('doDump', 'var')
        doDump = false;
    end
    if ~exist('holdOn','var')
        holdOn = false;
    end
    if ~holdOn
        if exist('dataset_legend','var')
            if ~(isnumeric(dataset_legend) && isnan(dataset_legend))
                error('No holdOn, but dataset_legend exist and is not NaN?')
            end
        end
        clear dataset_legend
    end
    
    plotFolder = 'marxBankJitterPlots';
    if doPlot == true
        %Attempt to create folder for plots
        mkdir(plotFolder)
    end
    if doDump
        %Format:
        % Row 1  : Time [ns]
        % Row 2-N: I    [A]
        % Will be transposed before output!
        dumpMatrix_in  = NaN;
        dumpMatrix_out = NaN;
    end
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % load the dataset
    [dataset, ~] = getDataset(dataset_id);
    datasetInfo(dataset);
    
    global hAllLegends;
    global hCurrRaw;
    global hCurrSmooth;
    global hCurrAllignThresh;
    global hCurrAllignCumsum;
    global hHistThresh;
    global hHistCumsum;
    global hHistPeak;
    global hPlotPeak;
    global hPlotThresh;
    global hPlotCumsum;
    if ~exist('dataset_legend', 'var')
       clear global hAllLegends;
       clear global hCurrRaw;
       clear global hCurrSmooth;
       clear global hCurrAllignThresh;
       clear global hCurrAllignCumsum;
       clear global hHistThresh;
       clear global hHistCumsum;
       clear global hHistPeak;
       clear global hPlotPeak;
       clear global hPlotThresh;
       clear global hPlotCumsum;
    else
        if isempty(hAllLegends)
            hAllLegends = {};
        end
        hAllLegends(length(hAllLegends)+1) = {dataset_legend};
        
        dataSetColor = NaN;
    end
    
    if ~isstruct(dataset)
        num_shots            = 0;
        
        var_thresh_in        = NaN;
        var_thresh_out       = NaN;
        var_cumsum_in        = NaN;
        var_cumsum_out       = NaN;
        
        mean_max_in          = NaN;
        mean_max_out         = NaN;
        mean_max_in_smooth   = NaN;
        mean_max_out_smooth  = NaN;
        
        var_max_in           = NaN;
        var_max_out          = NaN;
        max_max_in           = NaN;
        max_max_out          = NaN;
        med_max_in           = NaN;
        med_max_out          = NaN;
        
        thresh_in_array      = [];
        thresh_out_array     = [];
        cumsum_in_array      = [];
        cumsum_out_array     = [];
        
        max_in_array         = [];
        max_out_array        = [];
    
        max_in_smooth_array  = [];
        max_out_smooth_array = [];
        
        return; 
    end
    datasetInfo(dataset);
    
    %disp('data loaded')
    
    % CONFIGURATION
    %Get the in-/out-going currents
    scopename_in ='DIS_CUR_IN';
    scopename_out='DIS_CUR_OUT';
    I_threshold = 300.0;
    if exist('I_threshold_in','var')
        I_threshold = I_threshold_in;
    end
    smoothsamples = 10;
    time_varwindow = 300; %[ns]
    
    if size(fieldnames(dataset.scopes),1) == 0
        disp('No scopes!');
        
        num_shots            = 0;
        
        var_thresh_in        = NaN;
        var_thresh_out       = NaN;
        var_cumsum_in        = NaN;
        var_cumsum_out       = NaN;
        
        mean_max_in          = NaN;
        mean_max_out         = NaN;
        mean_max_in_smooth   = NaN;
        mean_max_out_smooth  = NaN;
        
        var_max_in           = NaN;
        var_max_out          = NaN;
        max_max_in           = NaN;
        max_max_out          = NaN;
        med_max_in           = NaN;
        med_max_out          = NaN;
        
        thresh_in_array      = [];
        thresh_out_array     = [];
        cumsum_in_array      = [];
        cumsum_out_array     = [];
        
        max_in_array         = [];
        max_out_array        = [];
    
        max_in_smooth_array  = [];
        max_out_smooth_array = [];
        
        return
    end
    if ~isfield(dataset.scopes, scopename_in) || ~isfield(dataset.scopes, scopename_out)
        disp('Missing scope!');
        
        num_shots            = 0;
        
        var_thresh_in        = NaN;
        var_thresh_out       = NaN;
        var_cumsum_in        = NaN;
        var_cumsum_out       = NaN;
        
        mean_max_in          = NaN;
        mean_max_out         = NaN;
        mean_max_in_smooth   = NaN;
        mean_max_out_smooth  = NaN;
        
        var_max_in           = NaN;
        var_max_out          = NaN;
        max_max_in           = NaN;
        max_max_out          = NaN;
        med_max_in           = NaN;
        med_max_out          = NaN;
        
        thresh_in_array      = [];
        thresh_out_array     = [];
        cumsum_in_array      = [];
        cumsum_out_array     = [];
        
        max_in_array         = [];
        max_out_array        = [];
    
        max_in_smooth_array  = [];
        max_out_smooth_array = [];
        
        return
    end
    if ~isfield(dataset.scopes.(scopename_in), 'dat') || ~isfield(dataset.scopes.(scopename_out), 'dat')
        disp('Crashed scope')
        num_shots            = 0;
        
        var_thresh_in        = NaN;
        var_thresh_out       = NaN;
        var_cumsum_in        = NaN;
        var_cumsum_out       = NaN;
        
        mean_max_in          = NaN;
        mean_max_out         = NaN;
        mean_max_in_smooth   = NaN;
        mean_max_out_smooth  = NaN;
        
        var_max_in           = NaN;
        var_max_out          = NaN;
        max_max_in           = NaN;
        max_max_out          = NaN;
        med_max_in           = NaN;
        med_max_out          = NaN;
        
        thresh_in_array      = [];
        thresh_out_array     = [];
        cumsum_in_array      = [];
        cumsum_out_array     = [];
        
        max_in_array         = [];
        max_out_array        = [];
    
        max_in_smooth_array  = [];
        max_out_smooth_array = [];
        
        return
    end
    
    num_in  = size(dataset.scopes.(scopename_in).dat,2);
    num_out = size(dataset.scopes.(scopename_out).dat,2);
    
    if (num_in ~= num_out)
        disp('num_in = ', num_in, '!=', num_out, 'num_out');
        
        num_shots            = 0;
        
        var_thresh_in        = NaN;
        var_thresh_out       = NaN;
        var_cumsum_in        = NaN;
        var_cumsum_out       = NaN;
        
        mean_max_in          = NaN;
        mean_max_out         = NaN;
        mean_max_in_smooth   = NaN;
        mean_max_out_smooth  = NaN;
        
        var_max_in           = NaN;
        var_max_out          = NaN;
        max_max_in           = NaN;
        max_max_out          = NaN;
        med_max_in           = NaN;
        med_max_out          = NaN;
        
        thresh_in_array      = [];
        thresh_out_array     = [];
        cumsum_in_array      = [];
        cumsum_out_array     = [];
        
        max_in_array         = [];
        max_out_array        = [];
    
        max_in_smooth_array  = [];
        max_out_smooth_array = [];
        
        return
    end
    
    if (num_in == 0)
        disp('num_in = num_out = 0')
        
        num_shots            = 0;
        
        var_thresh_in        = NaN;
        var_thresh_out       = NaN;
        var_cumsum_in        = NaN;
        var_cumsum_out       = NaN;
        
        mean_max_in          = NaN;
        mean_max_out         = NaN;
        mean_max_in_smooth   = NaN;
        mean_max_out_smooth  = NaN;
        
        var_max_in           = NaN;
        var_max_out          = NaN;
        max_max_in           = NaN;
        max_max_out          = NaN;
        med_max_in           = NaN;
        med_max_out          = NaN;
        
        thresh_in_array      = [];
        thresh_out_array     = [];
        cumsum_in_array      = [];
        cumsum_out_array     = [];
        
        max_in_array         = [];
        max_out_array        = [];
    
        max_in_smooth_array  = [];
        max_out_smooth_array = [];
    
        return
    end
    
    %Plotting and calculation
    if doPlot
        if sum(sum(isnan(showPlotsList))) || ismember(1,showPlotsList)
            fig1 = figure(1);
            if exist('holdOn', 'var') && ~holdOn
                clf(1);
            end
        end

        if sum(sum(isnan(showPlotsList))) || ismember(2,showPlotsList)
            fig2 = figure(2);
            if exist('holdOn', 'var') && ~holdOn
                clf(2);
            end
        end

        if sum(sum(isnan(showPlotsList))) || ismember(3,showPlotsList)
            fig3 = figure(3);
            if exist('holdOn', 'var') && ~holdOn
                clf(3);
            end
        end
        
        if sum(sum(isnan(showPlotsList))) || ismember(4,showPlotsList)
            fig4 = figure(4);
            if exist('holdOn', 'var') && ~holdOn
                clf(4);
            end
        end
    end
    
    thresh_in_array      = zeros(num_in,1);
    thresh_out_array     = zeros(num_out,1);
    
    cumsum_in_array      = zeros(num_in,1);
    cumsum_out_array     = zeros(num_out,1);
    
    max_in_array         =  zeros(num_out,1);
    max_out_array        =  zeros(num_out,1);
    
    max_in_smooth_array  = zeros(num_out,1);
    max_out_smooth_array = zeros(num_out,1);
    
    num_shots = 0;
    
    for i=1:num_in
        % Load the data
        curr_in  = dataset.scopes.(scopename_in).dat{i};
        time_in  = dataset.scopes.(scopename_in).time_ns{i};
        curr_out = dataset.scopes.(scopename_out).dat{i};
        time_out = dataset.scopes.(scopename_out).time_ns{i};
        
        if doDump
            if i == 1
                dumpMatrix_in = zeros(num_in, length(time_in));
                dumpMatrix_in(1,:) = time_in;
                
                dumpMatrix_out = zeros(num_in, length(time_out));
                dumpMatrix_out(1,:) = time_out;
            end
            
            dumpMatrix_in (i+1,:) = curr_in;
            dumpMatrix_out(i+1,:) = curr_out;
        end
        
        %Smoothed current
        curr_in_smooth  = smooth(curr_in,smoothsamples);
        curr_out_smooth = smooth(curr_out,smoothsamples);
        
        if doPlot
            if sum(sum(isnan(showPlotsList))) || ismember(1,showPlotsList)
                %figure(1)
                set(0,'currentFigure',fig1)
                subplot(2,1,1);
                if i == 1
                    hold on;
                    grid ON;
                    grid MINOR
                    if exist('curr_ylim','var')
                        ylim(curr_ylim)
                    end
                end
                if ~exist('dataset_legend','var')
                    plot(time_in, curr_in);
                else
                    if isnan( dataSetColor )
                        hLine = plot(time_in, curr_in);
                        colorOrder = get(gca, 'ColorOrder');
                        %dataSetColor = hLine.Color;
                        dataSetColor = colorOrder(mod(length(hAllLegends)-1,size(colorOrder,1))+1,:);
                        if isempty(hCurrRaw)
                            hCurrRaw = [hLine];
                        else
                            hCurrRaw(length(hCurrRaw)+1) = hLine;
                        end
                    else
                        plot(time_in, curr_in, 'Color', dataSetColor);
                    end
                end

                subplot(2,1,2);
                if i == 1
                    hold on;
                    grid ON;
                    grid MINOR
                    if exist('curr_ylim','var')
                        ylim(curr_ylim)
                    end
                end
                if ~exist('dataset_legend','var')
                    plot(time_out, curr_out);
                else
                    plot(time_out, curr_out, 'Color', dataSetColor);
                end
            end
            
            if sum(isnan(showPlotsList)) || ismember(2,showPlotsList)
                %figure(2)
                set(0,'currentFigure',fig2)
                subplot(2,1,1);
                if i == 1
                    hold on;
                    grid ON;
                    grid MINOR
                    if exist('curr_ylim','var')
                        ylim(curr_ylim)
                    end
                end
                if ~exist('dataset_legend','var')
                    plot(time_in, curr_in_smooth);
                else
                    hLine = plot(time_in, curr_in_smooth, 'Color', dataSetColor);
                    if length(hCurrSmooth) < length(hCurrRaw)
                        if isempty(hCurrSmooth)
                            hCurrSmooth = [hLine];
                        else
                            hCurrSmooth(length(hCurrSmooth)+1) = hLine;
                        end
                    end
                end

                subplot(2,1,2);
                if i==1
                    hold on
                    grid ON;
                    grid MINOR
                    if exist('curr_ylim','var')
                        ylim(curr_ylim)
                    end
                end
                if ~exist('dataset_legend','var')
                    plot(time_out, curr_out_smooth);
                else
                    plot(time_out, curr_out_smooth, 'Color', dataSetColor);
                end
            end
        end
        
        %No data?
        nodata_in = false;
        if((size(curr_in,2) == 0) || (max(curr_in) < I_threshold))
            nodata_in = true;
        end
        nodata_out = false;
        if ((size(curr_out,2) == 0) || (max(curr_out) < I_threshold))
            nodata_out = true;
        end
        
        if (nodata_in || nodata_out)
            thresh_in = NaN;
            thresh_out = NaN;
            
            cumsum_in_halftime = NaN;
            cumsum_out_halftime = NaN;
            
            max_in = NaN;
            max_out = NaN;
            
            max_in_smooth = NaN;
            max_out_smooth = NaN;
        else
            num_shots = num_shots + 1;
            
            %Estimate the time of discharge -- threshold
            thresh_in = find(curr_in > I_threshold);
            thresh_in = time_in(thresh_in(1));
        
            thresh_out = find(curr_out > I_threshold);
            thresh_out = time_out(thresh_out(1));
            
            %Estimate the time of discharge -- 50% integral
            cumsum_in = cumsum(curr_in);
            cumsum_in_halftime = find(cumsum_in > max(cumsum_in)/2.0);
            cumsum_in_halftime = time_in(cumsum_in_halftime(1));
            
            cumsum_out = cumsum(curr_out);
            cumsum_out_halftime = find(cumsum_out > max(cumsum_out)/2.0);
            cumsum_out_halftime = time_out(cumsum_out_halftime(1));
            
            %Estimate the peak current -- max()
            max_in  = max(curr_in);
            max_out = max(curr_out);
            
            max_in_smooth  = max(curr_in_smooth);
            max_out_smooth = max(curr_out_smooth);
            
        end
        
        thresh_in_array(i)  = thresh_in;
        thresh_out_array(i) = thresh_out;
        
        cumsum_in_array(i)  = cumsum_in_halftime;
        cumsum_out_array(i) = cumsum_out_halftime;
        
        max_in_array(i)  = max_in;
        max_out_array(i) = max_out;
        
        max_in_smooth_array(i)  = max_in_smooth;
        max_out_smooth_array(i) = max_out_smooth;
        
        if doPlot
            if sum(isnan(showPlotsList)) || ismember(3,showPlotsList)
                %figure(3)
                set(0,'currentFigure',fig3)
                subplot(2,1,1);
                if i == 1
                    hold on;
                    grid ON;
                    grid MINOR
                    if exist('curr_ylim','var')
                        ylim(curr_ylim)
                    end
                end
                if ~exist('dataset_legend','var')
                    plot(time_in-thresh_in, curr_in);
                else
                    hLine = plot(time_in-thresh_in, curr_in, 'Color', dataSetColor);
                    if length(hCurrAllignThresh) < length(hCurrRaw)
                        if isempty(hCurrAllignThresh)
                            hCurrAllignThresh = [hLine];
                        else
                            hCurrAllignThresh(length(hCurrAllignThresh)+1) = hLine;
                        end
                    end
                end

                subplot(2,1,2);
                if i == 1
                    hold on;
                    grid ON;
                    grid MINOR
                    if exist('curr_ylim','var')
                        ylim(curr_ylim)
                    end
                end
                if ~exist('dataset_legend','var')
                    plot(time_out-thresh_out, curr_out);
                else
                    plot(time_out-thresh_out, curr_out, 'Color', dataSetColor);
                end
            end
            
            if sum(isnan(showPlotsList)) || ismember(4,showPlotsList)
                %figure(4)
                set(0,'currentFigure',fig4)
                subplot(2,1,1);
                if i == 1
                    hold on;
                    grid ON;
                    grid MINOR
                    if exist('curr_ylim','var')
                        ylim(curr_ylim)
                    end
                end
                if ~exist('dataset_legend','var')
                    plot(time_in-cumsum_in_halftime, curr_in);
                else
                    hLine = plot(time_in-cumsum_in_halftime, curr_in, 'Color', dataSetColor);
                    if length(hCurrAllignCumsum) < length(hCurrRaw)
                        if isempty(hCurrAllignCumsum)
                            hCurrAllignCumsum = [hLine];
                        else
                            hCurrAllignCumsum(length(hCurrAllignCumsum)+1) = hLine;
                        end
                    end
                end

                subplot(2,1,2);
                if i == 1
                    hold on;
                    grid ON;
                    grid MINOR
                    if exist('curr_ylim','var')
                        ylim(curr_ylim)
                    end
                end
                if ~exist('dataset_legend','var')
                    plot(time_out-cumsum_out_halftime, curr_out);
                else
                    plot(time_out-cumsum_out_halftime, curr_out, 'Color', dataSetColor);
                end
            end
        end
    end
    
    if (num_shots == 0)
        disp('num_shots = 0')
        
        num_shots            = 0;
        var_thresh_in        = NaN;
        var_thresh_out       = NaN;
        var_cumsum_in        = NaN;
        var_cumsum_out       = NaN;
        mean_max_in          = NaN;
        mean_max_out         = NaN;
        mean_max_in_smooth   = NaN;
        mean_max_out_smooth  = NaN;
        
        var_max_in           = NaN;
        var_max_out          = NaN;
        max_max_in           = NaN;
        max_max_out          = NaN;
        med_max_in           = NaN;
        med_max_out          = NaN;
        
        return
    end
    
    if doPlot
        if sum(isnan(showPlotsList)) || ismember(1,showPlotsList)
            %set(0,'currentFigure',fig1)
            fig1=figure(1);
            ax1=subplot(2,1,1);
            %line(get(gca(),'XLim'),[I_threshold I_threshold],'Color',[0 0 0])
            ylabel('I_{in}(t) [A]')
            if ~exist('holdOn','var') || ~holdOn
                title(['Raw current signals (dataset ', num2str(dataset_id), ')']);
            else
                title('Raw current signals');
            end
            ax2=subplot(2,1,2);
            %line(get(gca(),'XLim'),[I_threshold I_threshold],'Color',[0 0 0])
            ylabel('I_{out}(t) [A]')
            xlabel('Time [ns]')
            linkaxes([ax1,ax2],'x')
            if exist('time_xlim','var')
                xlim(time_xlim)
            end
            if exist('dataset_legend','var')
                legend(hCurrRaw, hAllLegends, 'Location', 'best');
            end
            if ~exist('dataset_legend', 'var')
                saveas(fig1,[plotFolder, '/', 'currentRaw_', num2str(dataset_id), '.png']);
            end
            refresh(fig1)
        end
        
        if sum(isnan(showPlotsList)) || ismember(2,showPlotsList)
            %set(0,'currentFigure',fig2)
            fig2=figure(2);
            ax1=subplot(2,1,1);
            %line(get(gca(),'XLim'),[I_threshold I_threshold],'Color',[0 0 0])
            ylabel('I_{in}(t) [A]')
            if ~exist('holdOn','var') || ~holdOn
                title(['Smoothed current signals (dataset ', num2str(dataset_id), ')'])
            else
                title('Smoothed current signals')
            end
            ax2=subplot(2,1,2);
            %line(get(gca(),'XLim'),[I_threshold I_threshold],'Color',[0 0 0])
            ylabel('I_{out}(t) [A]')
            xlabel('Time [ns]')
            linkaxes([ax1,ax2],'x')
            if exist('time_xlim','var')
                xlim(time_xlim)
            end
            if exist('dataset_legend','var')
                legend(hCurrSmooth, hAllLegends, 'Location', 'best');
            end
            if ~exist('dataset_legend', 'var')
                saveas(fig2,[plotFolder, '/', 'currentSmooth_', num2str(dataset_id), '.png']);
            end
            refresh(fig2)
        end
        
        if sum(isnan(showPlotsList)) || ismember(3,showPlotsList)
            %set(0,'currentFigure',fig3)
            fig3 = figure(3);
            ax1=subplot(2,1,1);
            line(get(gca(),'XLim'),[I_threshold I_threshold],'Color',[0 0 0])
            line([0 0],get(gca(),'YLim'),'Color',[1 0 0])
            ylabel('I_{in}(t) [A]')
            if ~exist('holdOn','var') || ~holdOn
                title(['Raw current signals (alligned to threshold) (dataset ', num2str(dataset_id), ')'])
            else
                title('Raw current signals (alligned to threshold)')
            end
            ax2=subplot(2,1,2);
            line(get(gca(),'XLim'),[I_threshold I_threshold],'Color',[0 0 0])
            line([0 0],get(gca(),'YLim'),'Color',[1 0 0])
            ylabel('I_{out}(t) [A]')
            xlabel('Time [ns]')
            linkaxes([ax1,ax2],'x')
            if exist('time_xlim','var')
                xlim(time_xlim-0.5*(nanmean(thresh_in_array)+nanmean(thresh_out_array)))
            end
            if exist('dataset_legend','var')
                legend(hCurrAllignThresh, hAllLegends, 'Location', 'best');
            end
            if ~exist('dataset_legend', 'var')
                saveas(fig3,[plotFolder, '/', 'currentRawAllignedThreshold_', num2str(dataset_id), '.png']);
            end
            refresh(fig3)
        end

        if sum(isnan(showPlotsList)) || ismember(4,showPlotsList)
            fig4=figure(4);
            ax1=subplot(2,1,1);
            %line(get(gca(),'XLim'),[I_threshold I_threshold],'Color',[0 0 0])
            ylabel('I_{in}(t) [A]')
            if ~exist('holdOn','var') || ~holdOn
                title(['Raw current signals (alligned to cumsum 50%) (dataset ', num2str(dataset_id), ')'])
            else
                title('Raw current signals (alligned to cumsum 50%)')
            end
            line([0 0],get(gca(),'YLim'),'Color',[1 0 0])
            ax2=subplot(2,1,2);
            %line(get(gca(),'XLim'),[I_threshold I_threshold],'Color',[0 0 0])
            line([0 0],get(gca(),'YLim'),'Color',[1 0 0])
            ylabel('I_{out}(t) [A]')
            xlabel('Time [ns]')
            linkaxes([ax1,ax2],'x')
            if exist('time_xlim','var')
                xlim(time_xlim-0.5*(nanmean(cumsum_in_array)+nanmean(cumsum_out_array)))
            end
            if exist('dataset_legend','var')
                legend(hCurrAllignCumsum, hAllLegends, 'Location', 'best');
            end
            if ~exist('dataset_legend', 'var')
                saveas(fig4,[plotFolder, '/', 'currentRawAllignedCumsum50_', num2str(dataset_id), '.png']);
            end
            refresh(fig4)
        end
        
        %%% Histograms
        
        if sum(isnan(showPlotsList)) || ismember(10,showPlotsList)
            fig10=figure(10);
            if exist('holdOn', 'var') && ~holdOn
                clf(10);
            end

            ax1=subplot(2,1,1);
            hold on
            if ~exist('dataset_legend','var')
                if ~isnan(hist_nbins)
                    histogram(thresh_in_array,hist_nbins)
                else
                    histogram(thresh_in_array)
                end
                title(['From input current (dataset ', num2str(dataset_id), ')'])
            else
                if ~isnan(hist_nbins)
                    hHist = histogram(thresh_in_array, hist_nbins, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                else
                    hHist = histogram(thresh_in_array, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                end
                if isempty(hHistThresh)
                    hHistThresh = [hHist];
                else
                    hHistThresh(length(hHistThresh)+1) = hHist;
                end

                title('From input current')
                legend(hHistThresh,hAllLegends, 'Location', 'best')
            end

            ax2=subplot(2,1,2);
            hold on
            if ~exist('dataset_legend','var')
                if ~isnan(hist_nbins)
                    histogram(thresh_out_array,hist_nbins)
                else
                    histogram(thresh_out_array)
                end

            else
                if ~isnan(hist_nbins)
                    histogram(thresh_out_array,hist_nbins, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                else
                    histogram(thresh_out_array, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                end
            end
            title('From output current')
            xlabel('Threshold time [ns]')
            %linkaxes([ax1,ax2],'x')
            if ~exist('dataset_legend', 'var')
                saveas(fig10,[plotFolder, '/', 'timeThreshold_', num2str(dataset_id), '.png']);
            end
            refresh(fig10)
        end

        if sum(isnan(showPlotsList)) || ismember(11,showPlotsList)
            fig11=figure(11);
            if exist('holdOn', 'var') && ~holdOn
                clf(11);
            end
            ax1=subplot(2,1,1);
            hold on;
            if ~exist('dataset_legend','var')
                if ~isnan(hist_nbins)
                    histogram(cumsum_in_array,hist_nbins)
                else
                    histogram(cumsum_in_array)
                end
                title(['From input current (dataset ', num2str(dataset_id), ')'] )

            else
                if ~isnan(hist_nbins)
                    hHist = histogram(cumsum_in_array, hist_nbins, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                else
                    hHist = histogram(cumsum_in_array, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                end
                if isempty(hHistCumsum)
                    hHistCumsum = [hHist];
                else
                    hHistCumsum(length(hHistCumsum)+1) = hHist;
                end

                title('From input current')
                legend(hHistCumsum,hAllLegends, 'Location', 'best')
            end

            ax2=subplot(2,1,2);
            hold on
            if ~exist('dataset_legend','var')
                if ~isnan(hist_nbins)
                    histogram(cumsum_out_array,hist_nbins)
                else
                    histogram(cumsum_out_array)
                end
            else
                if ~isnan(hist_nbins)
                    histogram(cumsum_out_array,hist_nbins, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                else
                    histogram(cumsum_out_array, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                end
            end
            title('From output current')
            xlabel('Cumsum 50% time [ns]')
            %linkaxes([ax1,ax2],'x')
            if ~exist('dataset_legend', 'var')
                saveas(fig11,[plotFolder, '/', 'timeCumsum50_', num2str(dataset_id), '.png']);
            end
            refresh(fig11)
        end

        if sum(isnan(showPlotsList)) || ismember(12,showPlotsList)
            fig12=figure(12);
            if exist('holdOn', 'var') && ~holdOn
                clf(12);
            end
            ax1=subplot(2,1,1);
            hold on
            if ~exist('dataset_legend','var')
                if ~isnan(hist_nbins)
                    histogram(max_in_array,hist_nbins)
                else
                    histogram(max_in_array)
                end
                title(['From input current (dataset ', num2str(dataset_id), ')'])
            else
                if ~isnan(hist_nbins)
                    hHist = histogram(max_in_array, hist_nbins, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                else
                    hHist = histogram(max_in_array, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                end
                if isempty(hHistPeak)
                    hHistPeak = [hHist];
                else
                    hHistPeak(length(hHistPeak)+1) = hHist;
                end

                title('From input current')
                legend(hHistPeak,hAllLegends, 'Location', 'best')
            end
            ax2=subplot(2,1,2);
            hold on;
            if ~exist('dataset_legend','var')
                if ~isnan(hist_nbins)
                    histogram(max_out_array,hist_nbins)
                else
                    histogram(max_out_array)
                end
            else
                if ~isnan(hist_nbins)
                    histogram(max_out_array,hist_nbins, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                else
                    histogram(max_out_array, 'FaceColor', dataSetColor, 'FaceAlpha', 0.3);
                end
            end
            title('From output current')
            xlabel('Peak current (raw max) [A]')
            %linkaxes([ax1,ax2],'x')
            if ~exist('dataset_legend', 'var')
                saveas(fig12,[plotFolder, '/', 'peakCurrent_', num2str(dataset_id), '.png']);
            end
            refresh(fig12)
        end
        
        %%% ShotNum VS value plots
        
        if sum(isnan(showPlotsList)) || ismember(13,showPlotsList)
            fig13=figure(13);
            if exist('holdOn', 'var') && ~holdOn
                clf(13);
            end
            ax1=subplot(1,2,1);
            hold on
            if ~exist('dataset_legend', 'var')
                plot(max_in_array, '+')
            else
                hPlot = plot(max_in_array, 's', 'Color', dataSetColor, 'MarkerFaceColor', dataSetColor);
                if isempty(hPlotPeak)
                    hPlotPeak = hPlot;
                else
                    hPlotPeak(length(hPlotPeak)+1) = hPlot;
                end
                legend(hPlotPeak,hAllLegends, 'Location', 'best', 'NumColumns', 2)
            end
            if ~exist('holdOn', 'var')
                sgtitle(['Peak current (raw max) (dataset ', num2str(dataset_id), ')'])
            else
                sgtitle('Peak current (raw max)')
            end
            ylabel('I_{in} [A]')
            ax2=subplot(1,2,2);
            hold on
            if ~exist('dataset_legend', 'var')
                plot(max_out_array, '+')
            else
                plot(max_out_array, 's', 'Color', dataSetColor, 'MarkerFaceColor', dataSetColor)
            end
            ylabel('I_{out} [A]')
            xlabel('Shot idx')
            %linkaxes([ax1,ax2],'xy')
            if ~exist('dataset_legend', 'var')
                saveas(fig13,[plotFolder, '/', 'peakCurrent_shots_', num2str(dataset_id), '.png']);
            end
            refresh(fig13)
        end
        
        if sum(isnan(showPlotsList)) || ismember(14,showPlotsList)
            fig14=figure(14);
            if exist('holdOn', 'var') && ~holdOn
                clf(14);
            end
            ax1=subplot(1,2,1);
            hold on
            if ~exist('dataset_legend', 'var')
                plot(thresh_in_array, '+')
            else
                hPlot = plot(thresh_in_array, 's', 'Color', dataSetColor, 'MarkerFaceColor', dataSetColor);
                if isempty(hPlotThresh)
                    hPlotThresh = hPlot;
                else
                    hPlotThresh(length(hPlotThresh)+1) = hPlot;
                end
                legend(hPlotThresh,hAllLegends, 'Location', 'best', 'NumColumns', 2)
            end
            sgtitle(['Threshold time (dataset ', num2str(dataset_id), ')'])
            ylabel('t_{in} [ns]')
            xlabel('Shot idx')
            ax2=subplot(1,2,2);
            hold on
            if ~exist('dataset_legend', 'var')
                plot(thresh_out_array, '+')
            else
                plot(thresh_out_array, 's', 'Color', dataSetColor, 'MarkerFaceColor', dataSetColor)
            end
            ylabel('t_{out} [ns]')
            xlabel('Shot idx')
            %linkaxes([ax1,ax2],'xy')
            if ~exist('dataset_legend', 'var')
                saveas(fig14,[plotFolder, '/', 'timeThreshold_shots_', num2str(dataset_id), '.png']);
            end
            refresh(fig14)
        end
        
        if sum(isnan(showPlotsList)) || ismember(15,showPlotsList)
            fig15=figure(15);
            if exist('holdOn', 'var') && ~holdOn
                clf(15);
            end
            ax1=subplot(1,2,1);
            hold on;
            if ~exist('dataset_legend', 'var')
                plot(cumsum_in_array,'+')
            else
                hPlot = plot(cumsum_in_array, 's', 'Color', dataSetColor, 'MarkerFaceColor', dataSetColor);
                if isempty(hPlotCumsum)
                    hPlotCumsum = hPlot;
                else
                    hPlotCumsum(length(hPlotCumsum)+1) = hPlot;
                end
                legend(hPlotCumsum,hAllLegends, 'Location', 'best', 'NumColumns', 2)
            end
            sgtitle(['Cumsum 50% time (dataset ', num2str(dataset_id), ')'])
            ylabel('t_{in} [ns]')
            xlabel('Shot idx')
            ax2=subplot(1,2,2);
            hold on
            if ~exist('dataset_legend', 'var')
                plot(cumsum_out_array,'+')
            else
                plot(cumsum_out_array, 's', 'Color', dataSetColor, 'MarkerFaceColor', dataSetColor)
            end
            ylabel('t_{out} [ns]')
            xlabel('Shot idx')
            %linkaxes([ax1,ax2],'xy')
            if ~exist('dataset_legend', 'var')
                saveas(fig15,[plotFolder, '/', 'timeCumsum50_', num2str(dataset_id), '.png']);
            end
            refresh(fig15)
        end
    end
    
    %Outputs
    var_thresh_in  = cleanvar(thresh_in_array,time_varwindow);
    var_thresh_out = cleanvar(thresh_out_array,time_varwindow);
    var_cumsum_in  = cleanvar(cumsum_in_array,time_varwindow);
    var_cumsum_out = cleanvar(cumsum_out_array,time_varwindow);
    
    mean_max_in = nanmean(max_in_array);
    mean_max_out = nanmean(max_out_array);
    
    var_max_in  = cleanvar(max_in_array,  mean_max_in/3);
    var_max_out = cleanvar(max_out_array,  mean_max_out/3);
    
    mean_max_in_smooth = nanmean(max_in_smooth_array);
    mean_max_out_smooth = nanmean(max_out_smooth_array);
    
    var_max_in_smooth  = cleanvar(mean_max_in_smooth,  mean_max_in_smooth/3);
    var_max_out_smooth = cleanvar(mean_max_out_smooth, mean_max_out_smooth/3);
    
    max_max_in         = max(max_in_array);
    max_max_out        = max(max_out_array);
    max_max_in_smooth  = max(max_in_smooth_array);
    max_max_out_smooth = max(max_out_smooth_array);
    
    med_max_in         = median(max_in_array,  'omitnan');
    med_max_out        = median(max_out_array, 'omitnan');
    
    %Printouts
    disp(' ')
    disp(['num_shots      = ', num2str(num_shots)])
    disp(['num_in         = ', num2str(num_in)])
    disp(' ')
    disp(['var_thresh_in  = ', num2str(var_thresh_in,  '%.2f'), ' [ns^2], sigma = ', num2str(sqrt(var_thresh_in),  '%.2f'), ' [ns]'])
    disp(['var_thresh_out = ', num2str(var_thresh_out, '%.2f'), ' [ns^2], sigma = ', num2str(sqrt(var_thresh_out), '%.2f'), ' [ns]'])
    disp(['var_cumsum_in  = ', num2str(var_cumsum_in,  '%.2f'), ' [ns^2], sigma = ', num2str(sqrt(var_cumsum_in),  '%.2f'), ' [ns]'])
    disp(['var_cumsum_out = ', num2str(var_cumsum_out, '%.2f'), ' [ns^2], sigma = ', num2str(sqrt(var_cumsum_out), '%.2f'), ' [ns]'])
    disp(' ')
    disp(['mean_max_in          = ', num2str(mean_max_in,         '%.2f'), ' [A]'])
    disp(['mean_max_out         = ', num2str(mean_max_out,        '%.2f'), ' [A]'])
    disp(['mean_max_in_smooth   = ', num2str(mean_max_in_smooth,  '%.2f'), ' [A]'])
    disp(['mean_max_out_smooth  = ', num2str(mean_max_out_smooth, '%.2f'), ' [A]'])
    disp(' ')
    disp(['var_max_in          = ', num2str(var_max_in,  '%.4f'),         ' [A^2], sigma = ', num2str(sqrt(var_max_in),  '%.4f'), ' [A]'])
    disp(['var_max_out         = ', num2str(var_max_out,  '%.4f'),        ' [A^2], sigma = ', num2str(sqrt(var_max_out),  '%.4f'), ' [A]'])
    disp(['var_max_in_smooth   = ', num2str(var_max_in_smooth,  '%.4e'),  ' [A^2], sigma = ', num2str(sqrt(var_max_in_smooth),  '%.4e'), ' [A]'])
    disp(['var_max_out_smooth  = ', num2str(var_max_out_smooth,  '%.4e'), ' [A^2], sigma = ', num2str(sqrt(var_max_out_smooth),  '%.4e'), ' [A]'])
    disp(' ')
    disp(['max_max_in         = ', num2str(max_max_in,         '%.2f'), ' [A]'])
    disp(['max_max_out        = ', num2str(max_max_out,        '%.2f'), ' [A]'])
    disp(['max_max_in_smooth  = ', num2str(max_max_in_smooth,  '%.2f'), ' [A]'])
    disp(['max_max_out_smooth = ', num2str(max_max_out_smooth, '%.2f'), ' [A]'])
    disp(' ')
    disp(['med_max_in         = ', num2str(med_max_in,         '%.2f'), ' [A]'])
    disp(['med_max_out        = ', num2str(med_max_out,        '%.2f'), ' [A]'])
    
    %Dump to external file
    if doDump
        disp(' ')
        disp('Dumping data to file...')
        
        %Transpose
        dumpMatrix_in  = dumpMatrix_in';
        dumpMatrix_out = dumpMatrix_out';
        
        %Write ASCII
        writematrix(dumpMatrix_in,  [plotFolder, '/', 'currentDataIn_',  num2str(dataset_id), '.txt'], 'Delimiter','tab')
        writematrix(dumpMatrix_out, [plotFolder, '/', 'currentDataOut_', num2str(dataset_id), '.txt'], 'Delimiter','tab')
        
        %Write MATLAB
        save([plotFolder, '/', 'currentData_',  num2str(dataset_id), '.mat'], 'dumpMatrix_*');
    end
    
    %Sound!
    if doPlot
        load('splat', 'y', 'Fs')
        sound(y,Fs)
    end
    
end

function [cleanvar] = cleanvar(vector,relabslim)
    %Remove outliers (more than realbslim from the mean) and NaNs before calculating the variance

    vector_nonans = vector(~isnan(vector));
    
    vector_mean = mean(vector_nonans);
    
    vector_scrubbed = vector_nonans(vector_nonans > vector_mean - relabslim);
    vector_scrubbed = vector_nonans(vector_scrubbed < vector_mean + relabslim);
    
    cleanvar = var(vector_scrubbed);
end
