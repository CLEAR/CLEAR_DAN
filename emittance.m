function [] = emittance(dataset_id, energy_MeV, visu, camName, dataset_id_bg)
    
    % visualize if desired
    if ~exist('visu', 'var')
        visu = false;
    end

    % Set backgrounDatasetNumber
    if ~exist('dataset_id_bg', 'var')
        dataset_id_bg = 10398;
    end
    
    % fundamental constants
    SI_e = 1.60217662e-19; % [C] electron charge
    SI_c = 299792458; % [m/s] speed of light
    SI_me = 9.10938356e-31; % [kg] electron mass
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset); 
        return; 
    end
    datasetInfo(dataset);
    if ~dataset.metadata.isScan
       disp('Not a scan at all!')
       return
    end
    
    % prepare figure
    if visu 
        figure(1);
        clf(1);
        set(gcf, 'color', 'w');
        colormap(mod_jet());
    end
    
    % select measurement screen (OTR, spectrometer)
    camFields = fields(dataset.images);
    if numel(camFields) > 1
        if ~exist('camName','var')
            disp('Please provide camera name as argument');
        end
    else
        camName = camFields{1};
    end
    camStruct = dataset.images.(camName);
    camDesc = camStruct.desc;
    
    % get common camera variables
    xaxis = camStruct.xaxis;
    yaxis = camStruct.yaxis;
    xinds = 1:numel(xaxis);
    yinds = 1:numel(yaxis);
    if strcmp(camName, 'FAR_OTR')
        calib_x = 0.0192; % mm/px
        calib_y = 0.0208; % mm/px
    elseif strcmp(camName, 'DNS_SPC')
        calib_x = 0.1524; % mm/px
        calib_y = 0.1524; % mm/px
    elseif strcmp(camName, 'ACC_OUT')
        %High magnification
        calib_x = 1/135.0; %mm/px
        calib_y = 0.0069;  %mm/px
    elseif strcmp(camName, 'ELT_OTR')
        calib_x = 0.0124;  %mm/px
        calib_y = 0.0124;  %mm/px
    elseif strcmp(camName, 'PLE_OTR')
        calib_x = 0.0278;  %mm/px
        calib_y = 0.0302;  %mm/px
    elseif strcmp(camName, 'UPS_OTR')
        calib_x = 0.0210;  %mm/px
        calib_y = 0.0240;  %mm/px
    end
    
    % color scheme
    hcol = [0 0.447 0.741];
    vcol = [0.85 0.325 0.098];
    
    % upstream or downstream quad used in scan
    if ~isempty(strfind(dataset.metadata.scanName,'Emittance quad scan vertical'))
        upstream_scan = true;
        downstream_scan = false;
        general_scan = false;
        scanned_magnet = 0;
        
    elseif ~isempty(strfind(dataset.metadata.scanName, 'Emittance quad scan horizontal'))
        upstream_scan = false;
        downstream_scan = true;
        general_scan = false;
        scanned_magnet = 0;
        
    % These are more useful for any quad used in a scan
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(350)'))
        general_scan = true;
        scanned_magnet = 350;
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(355)'))
        general_scan = true;
        scanned_magnet = 355;
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(360)'))
        general_scan = true;
        scanned_magnet = 360;
        
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(510)'))
        general_scan = true;
        scanned_magnet = 510;
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(515)'))
        general_scan = true;
        scanned_magnet = 515;
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(520)'))
        general_scan = true;
        scanned_magnet = 520;
        
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(760)'))
        general_scan = true;
        scanned_magnet = 760;
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(765)'))
        general_scan = true;
        scanned_magnet = 765;
    elseif strcmp(dataset.metadata.scanType, 'Scan') && ~isempty(strfind(dataset.metadata.scanName, '(770)'))
        general_scan = true;
        scanned_magnet = 770;
    else
        disp('Not a quad scan!');
        return
    end
    
    
     %% PLASMA LIGHT BACKGROUND DATASET
    
     % zero background
    bg_image = zeros(numel(yinds), numel(xinds));
    
    if (dataset_id_bg ~= -1) && strcmp(camName, 'FAR_OTR') && dataset.state.DISCHARGE_ENABLED.dat
        % load the reference dataset
        %dataset_id_bg = 10398;
        dataset_bg = getDataset(dataset_id_bg);
        if ~isstruct(dataset_bg);
            disp('Loading of dataset_bg failed');
            return; 
        end
        datasetInfo(dataset_bg);

        % grab same image structure from the reference dataset
        camStruct_bg = dataset_bg.images.(camName);

        % cycle all scan shots
        numShots_ref = dataset_bg.metadata.numShots;
        bg_image = zeros(numel(yinds), numel(xinds));
        for i = 1:numShots_ref
            bg_image = bg_image + double(camStruct_bg.dat{i});
        end
        bg_image = bg_image / numShots_ref;
        
    end
    
    % gaussian model for plotting
    gaussmod = @(f, xs) f.a1*exp(-((xs-f.b1)/f.c1).^2);
    
    % cycle all shots
    numShots = dataset.metadata.numShots;
    [sigxs, sigys] = deal(nan(1,numShots));
    for i = 1:numShots
        
        % progress text
        if mod(i,50)==0
            disp(' ');
        end
        
        
        %% IMAGE ANALYSIS
        
        % get image
        image = double(camStruct.dat{i});
        
        % subtract the background
        image = image - bg_image;
        
        % start cleaning
        image_clean = image;
        mfo2 = 5;
        image_mf2 = medfilt2(image_clean, mfo2*[1 1]);
        
        edge = 20;
        
        % remove background
        %bgval = median(mean(image_clean, 2));
        bgval = mean([mean(mean(image_clean(1:edge, 1:edge))), ...
                      mean(mean(image_clean(1:edge, (end-edge):end))), ...
                      mean(mean(image_clean((end-edge):end, 1:edge))), ...
                      mean(mean(image_clean((end-edge):end, (end-edge):end)))]);
        image_clean = image_clean - bgval;
        
        %image_clean(image_clean < 0) = 0;
        
        image_bg = image_clean;
        
        % projections
        xproj0 = max(image_mf2);
        yproj0 = max(image_mf2');
        
        xproj0 = xproj0 - mean(xproj0([1:edge ((end-edge):end)]));
        yproj0 = yproj0 - mean(yproj0([1:edge ((end-edge):end)]));
        
        % perform first gaussian fits
        try
            fx0 = fit(xinds', xproj0', 'gauss1');
        catch
            fprintf('x');
            continue;
        end
        mux0 = fx0.b1;
        sigx0 = fx0.c1/sqrt(2);
        
        try
            fy0 = fit(yinds', yproj0', 'gauss1');
        catch
            fprintf('x');
            continue;
        end
        muy0 = fy0.b1;
        sigy0 = fy0.c1/sqrt(2);
        
        % define ROI (region of interest)
        nsig = 3; % number of sigmas to include
        xroi = max(1, floor(mux0-nsig*sigx0)):min(numel(xinds), ceil(mux0+nsig*sigx0));
        yroi = max(1, floor(muy0-nsig*sigy0)):min(numel(yinds), ceil(muy0+nsig*sigy0));
        
        % apply ROI
        image_roi = image_clean(yroi, xroi);
        
        % remake projections
        xproj = mean(image_roi, 1);
        yproj = mean(image_roi, 2)';
        
        % apply median filter
        mfo = 3;
        xproj_mf = medfilt1(xproj, mfo);
        yproj_mf = medfilt1(yproj, mfo);
        
        
        % perform second gaussian fits
        try
            fx = fit(xroi', xproj_mf', 'gauss1');
        catch
            fprintf('x');
            continue;
        end
        sigx = fx.c1/sqrt(2); % [px]
        
        try
            fy = fit(yroi', yproj_mf', 'gauss1');
        catch
            fprintf('x');
            continue;
        end
        sigy = fy.c1/sqrt(2); % [px]
        
        % calibrated beam sizes
        sigxs(i) = sigx*calib_x; % [mm]
        sigys(i) = sigy*calib_y; % [mm]
        
        
        
        %% PLOT ANALYSIS RESULTS
        
        %visu = true;
        if visu
    
            % show raw image
            subplot(3,1,1);
            imagesc(xinds, yinds, image_bg);
            pos = [min(xroi), min(yroi), max(xroi)-min(xroi), max(yroi)-min(yroi)];
            rectangle('Position', pos, 'EdgeColor', 'r', 'LineWidth', 2);
            colorbar;
            caxis([0 max(max(medfilt2(image_bg, [4 4])))]);
            xlabel('x (px)');
            ylabel('y (px)');
            title(['Original image (dataset ' dataset_id ', shot ' num2str(i) ')']);

            % analysis
            subplot(3,2,3);
            %plot(xinds, mean(image_clean(yroi,:)), '.', xroi, gaussmod(fx, xroi), '-', 'Color', hcol);
            plot(xinds, xproj0, '.', xinds, gaussmod(fx0, xinds), '--', 'Color', hcol);
            hold on;
            plot(yinds, yproj0, '.', yinds, gaussmod(fy0, yinds), '--', 'Color', vcol);
            hold off;
            xlim([min(xinds) max(xinds)]);
            %ylim([-0.1 1.1]*abs(max(max(fx.a1), max(fy.a1))));
            xlabel('x (px)');
            ylabel('Pixel count');
            title('Maximums');
            
            % analysis
            subplot(3,2,4);
            plot(xroi, xproj, '.', xroi, gaussmod(fx, xroi), '-', 'Color', hcol);
            hold on;
            plot(yroi, yproj, '.', yroi, gaussmod(fy, yroi), '-', 'Color', vcol);
            hold off;
            xlim([min(xinds) max(xinds)]);
            %ylim([-0.1 1.1]*abs(max(max(fx.a1), max(fy.a1))));
            xlabel('x (px)');
            ylabel('Pixel count');
            title('ROI projection');

            % show cleaned image
            subplot(3,2,5);
            imagesc(xroi, yroi, image_roi);
            xlabel('x (px)');
            ylabel('y (px)');
            title('Cleaned image');

            % overall progress
            subplot(3,2,6);
            Is = dataset.scalars.step_value.dat;
            plot(Is, sigxs, 'o', 'Color', hcol);
            hold on;
            plot(Is, sigys, 'o', 'Color', vcol);
            hold off;
            title('Scan overview');
            xlabel('I (A)');
            ylabel('\sigma (mm)');
            xlim([min(Is)-0.5, max(Is)+0.5]);
            legend('x', 'y');

            drawnow;
        end
        
        % display progress
        fprintf('.');
        
    end
    disp(' ');
    
    
    %% DEFINE BEAM LINE
    
    % transfer matrices
    Md = @(d) [1 d 0 0; 0 1 0 0; 0 0 1 d; 0 0 0 1];
    function quadrupole_matrix = Mq(k,l)
        if abs(k) < 1e-10
            quadrupole_matrix = Md(l);
            return;
        end
        
        quadrupole_matrix = ...
            [        cos(l*sqrt(k)), sin(l*sqrt(k))/sqrt(k), 0, 0; ...
            -sqrt(k)*sin(l*sqrt(k)), cos(l*sqrt(k)),         0, 0; ...
                                  0,                      0, cosh(l*sqrt(k)),         sinh(l*sqrt(k))/sqrt(k); ...
                                  0,                      0, sqrt(k)*sinh(l*sqrt(k)), cosh(l*sqrt(k))             ];
    end
              
    if general_scan
        Lq = 0.226; %[m] Magnetic length of quadrupole
        if scanned_magnet == 350 || scanned_magnet == 355 || scanned_magnet == 360
            d_quad = 0.224;   % [m] Distance between quadrupoles (magnetic stop to magnetic start)
            d_screen = 1.127; % [m] Distance from last quad (magnetic stop) to screen 390
            positionString = 'at QFD350 entrance';

        elseif scanned_magnet == 510 || scanned_magnet == 515 || scanned_magnet == 520
            d_quad = 0.274;   % [m] Distance between quadrupoles (magnetic stop to magnetic start)
            d_screen = 6.264; % [m] Distance from last quad (magnetic stop) to screen 390
            positionString = 'at QF510 entrance';
            
        elseif scanned_magnet == 760 || scanned_magnet == 765 || scanned_magnet == 770
            d_quad = 0.224; % [m] Distance between quadrupoles (magnetic stop to magnetic start)
            if strcmp(camName, 'ELT_OTR')
                d_screen = 1.056;
            elseif strcmp(camName, 'PLE_OTR')
                d_screen = 1.056 + 15e-3 + 282e-3;
            end
            positionString = 'at QFD760 entrance';
            
        else
            disp('Scanned_magnet unknown when setting up the beamline')
        end
        % transfer matrix from beginning of first quad to the screen
        Mtot = @(k1, k2, k3) Md(d_screen) * Mq(k3,Lq) * Md(d_quad) * Mq(k2, Lq) * Md(d_quad) * Mq(k1, Lq);
        
    else
        positionString = 'at plasma lens exit';
        Lq = 0.226;
        d1 = 1.458; % [m]
        d2 = 0.224; % [m]
        if strcmp(camName, 'FAR_OTR')
            d3 = 1.120; % [m]
        elseif strcmp(camName, 'DNS_SPC')
            d3 = 1.843; % [m]
        end
    
        % transfer matrix back to exit of plasma lens
        Mtot = @(k1, k2) Md(d3) * Mq(k2, Lq) * Md(d2) * Mq(k1, Lq) * Md(d1);
        
    end
    
    % magnet current conversion factor
    EC_QD = 0.056;
    F_QD = SI_c * 1e-6 * EC_QD;
    ItoK = @(I) I * F_QD / energy_MeV;
    
    % cleaning helper function
    prc_cut = 90;
    prc_mask = @(m) m < prctile(m, prc_cut) & m > prctile(m, 100-prc_cut);
    
    %% DO STATISTICS
    steps = unique(dataset.scalars.step_num.dat);
    [sigxs_mean, sigxs_err, sigys_mean, sigys_err, Is_scan] = deal(zeros(1, numel(steps)));
    [scan_matrix_x, scan_matrix_y] = deal(zeros(numel(steps), 3));
    for j = 1:numel(steps)
        step_num = steps(j);
        step_mask = (dataset.scalars.step_num.dat == step_num);
        
        % get beam sizes
        step_sigxs = sigxs(step_mask & ~isnan(sigxs));
        prcmask_x = prc_mask(step_sigxs);
        sigxs_mean(j) = mean(step_sigxs(prcmask_x)) / 1000; % [m]
        sigxs_err(j) = std(step_sigxs(prcmask_x)) / 1000; % [m]
        
        step_sigys = sigys(step_mask & ~isnan(sigys));
        prcmask_y = prc_mask(step_sigys);
        sigys_mean(j) = mean(step_sigys(prcmask_y)) / 1000; % [m]
        sigys_err(j) = std(step_sigys(prcmask_y)) / 1000; % [m]
        
        % currents and scan matrix
        if general_scan
            if strcmp(camName, 'ACC_OUT')
                if scanned_magnet == 350
                    Iq1 = median(dataset.scalars.step_value.dat(step_mask)); % [A] first magnet  (QFD350)
                    Iq2 = dataset.state.VESPER_TRI_Q2.dat;                   % [A] second magnet (QDD355)
                    Iq3 = dataset.state.VESPER_TRI_Q3.dat;                   % [A] second magnet (QFD360)
                    Is_scan(j) = Iq1;
                elseif scanned_magnet == 355
                    Iq1 = dataset.state.VESPER_TRI_Q1.dat;                   % [A] first magnet  (QFD350)
                    Iq2 = median(dataset.scalars.step_value.dat(step_mask)); % [A] second magnet (QDD355)
                    Iq3 = dataset.state.VESPER_TRI_Q3.dat;                   % [A] second magnet (QFD360)
                    Is_scan(j) = Iq2;
                elseif scanned_magnet == 360
                    Iq1 = dataset.state.VESPER_TRI_Q1.dat;                   % [A] first magnet  (QFD350)
                    Iq2 = dataset.state.VESPER_TRI_Q2.dat;                   % [A] second magnet (QDD355)
                    Iq3 = median(dataset.scalars.step_value.dat(step_mask)); % [A] second magnet (QFD360)
                    Is_scan(j) = Iq3;
                else
                    disp(['Unknown magnet' num2str(scanned_magnet) 'for camera ACC_OUT'])
                end
            elseif strcmp(camName, 'UPS_OTR')
                if scanned_magnet == 510
                    Iq1 = median(dataset.scalars.step_value.dat(step_mask));  % [A] first magnet  (QFD510)
                    Iq2 = dataset.state.CLICMOD_TRI_Q2.dat;                   % [A] second magnet (QDD515)
                    Iq3 = dataset.state.CLICMOD_TRI_Q3.dat;                   % [A] second magnet (QFD520)
                    Is_scan(j) = Iq1;
                elseif scanned_magnet == 515
                    Iq1 = dataset.state.CLICMOD_TRI_Q1.dat;                   % [A] first magnet  (QFD510)
                    Iq2 = median(dataset.scalars.step_value.dat(step_mask));  % [A] second magnet (QDD515)
                    Iq3 = dataset.state.CLICMOD_TRI_Q3.dat;                   % [A] second magnet (QFD520)
                    Is_scan(j) = Iq2;
                elseif scanned_magnet == 520
                    Iq1 = dataset.state.CLICMOD_TRI_Q1.dat;                   % [A] first magnet  (QFD510)
                    Iq2 = dataset.state.CLICMOD_TRI_Q2.dat;                   % [A] second magnet (QDD515)
                    Iq3 = median(dataset.scalars.step_value.dat(step_mask));  % [A] second magnet (QFD520)
                    Is_scan(j) = Iq3;
                else
                    disp(['Unknown magnet' num2str(scanned_magnet) 'for camera ACC_OUT'])
                end
            elseif strcmp(camName, 'ELT_OTR') || strcmp(camName, 'PLE_OTR')
                if scanned_magnet == 760
                    Iq1 = median(dataset.scalars.step_value.dat(step_mask)); % [A] first magnet  (QFD760)
                    Iq2 = dataset.state.PL_TRI_Q2.dat;                       % [A] second magnet (QDD765)
                    Iq3 = dataset.state.PL_TRI_Q3.dat;                       % [A] second magnet (QFD770)
                    Is_scan(j) = Iq1;
                elseif scanned_magnet == 765
                    Iq1 = dataset.state.PL_TRI_Q1.dat;                       % [A] first magnet  (QFD760)
                    Iq2 = median(dataset.scalars.step_value.dat(step_mask)); % [A] second magnet (QDD765)
                    Iq3 = dataset.state.PL_TRI_Q3.dat;                       % [A] second magnet (QFD770)
                    Is_scan(j) = Iq2;
                elseif scanned_magnet == 770
                    Iq1 = dataset.state.PL_TRI_Q1.dat;                       % [A] first magnet  (QFD760)
                    Iq2 = dataset.state.PL_TRI_Q2.dat;                       % [A] second magnet (QDD765)
                    Iq3 = median(dataset.scalars.step_value.dat(step_mask)); % [A] second magnet (QFD770)
                    Is_scan(j) = Iq3;
                else
                    disp(['Unknown magnet' num2str(scanned_magnet) 'for camera ELT_OTR and PLE_OTR'])
                end
            else
                disp(['Uknown camera ', camName, ' for scanned_magnet ', num2str(scanned_magnet)])
                return
            end
            
            % transfer matrix
            M = Mtot(ItoK(Iq1), -ItoK(Iq2), ItoK(Iq3)); % Middle one is defocusing in horizontal plane
        else
            if upstream_scan
                % get currents
                Iq1 = median(dataset.scalars.step_value.dat(step_mask)); % [A] first magnet (currently called BHS0890)
                Iq2 = dataset.state.DN_DBL_Q2.dat; % [A] second magnet (currently called QFS0880)
                Is_scan(j) = Iq1;
            elseif downstream_scan
                % get currents
                Iq1 = dataset.state.DN_DBL_Q2.dat; % [A] first magnet (currently called BHS0890)
                Iq2 = median(dataset.scalars.step_value.dat(step_mask)); % [A] second magnet (currently called QFS0880)
                Is_scan(j) = Iq2;
            end
            
            % transfer matrix
            M = Mtot(-ItoK(Iq1), ItoK(Iq2)); % downstream is focusing in horizontal
        end
        
        % calculate scan matrix in X
        R11 = M(1,1);
        R12 = M(1,2);
        scan_matrix_x(j,:) = [R11^2, -2*R11*R12, R12^2];
        
        % calculate scan matrix in Y
        R33 = M(3,3);
        R34 = M(3,4);
        scan_matrix_y(j,:) = [R33^2, -2*R33*R34, R34^2];
    end
    
    
    %% CALCULATE EMITTANCE
    
    %% HORIZONTAL
    
    % initial calculation of beta vector
    betax_vec0 = pinv(scan_matrix_x)*(sigxs_mean.^2)';
    
    % find suitable ROI (out to max 3 times minimum size)
    sigx_sim0 = sqrt(scan_matrix_x*betax_vec0);
    nsize = 5;
    [sigx_min, locx_min] = min(sigx_sim0);
    [~, locx_n] = min(abs(sigx_sim0/sigx_min-nsize));
    dlocx = abs(locx_n-locx_min);
    locx1 = locx_min-dlocx;
    locx2 = locx_min+dlocx;
    locx_roi = max(1,locx1):min(numel(sigx_sim0),locx2);
    Is_xroi = Is_scan(locx_roi);
    scan_matrix_xroi = scan_matrix_x(locx_roi,:);
    
    % remake beta vector
    betax_vec_roi = pinv(scan_matrix_x(locx_roi,:))*(sigxs_mean(locx_roi).^2)';
    sigx_sim_roi = sqrt(scan_matrix_xroi*betax_vec_roi);
    
    % calcalate emittance and Twiss parameters
    gamma = energy_MeV*1e6*SI_e/(SI_me*SI_c^2);
    emitgx0 = sqrt(betax_vec_roi(1)*betax_vec_roi(3) - betax_vec_roi(2)^2);
    emitNx0 = emitgx0 * gamma ; % [m rad]
    betax0 = betax_vec_roi(1) / emitgx0; % [m]
    alphax0 = betax_vec_roi(2) / emitgx0;
    sigx_PL = sqrt(betax_vec_roi(1)); % [m]
    
    
    %% VERTICAL
    
    % initial calculation of beta vector
    betay_vec0 = pinv(scan_matrix_y)*(sigys_mean.^2)';
    
    % find suitable ROI (out to max 3 times minimum size)
    sigy_sim0 = sqrt(scan_matrix_y*betay_vec0);
    [sigy_min, locy_min] = min(sigy_sim0);
    [~, locy_n] = min(abs(sigy_sim0/sigy_min-nsize));
    dlocy = abs(locy_n-locy_min);
    locy1 = locy_min-dlocy;
    locy2 = locy_min+dlocy;
    locy_roi = max(1,locy1):min(numel(sigy_sim0),locy2);
    Is_yroi = Is_scan(locy_roi);
    scan_matrix_yroi = scan_matrix_y(locy_roi,:);
    
    % remake beta vector
    betay_vec_roi = pinv(scan_matrix_y(locy_roi,:))*(sigys_mean(locy_roi).^2)';
    sigy_sim_roi = sqrt(scan_matrix_yroi*betay_vec_roi);
    
    % calcalate emittance and Twiss parameters
    emitgy0 = sqrt(betay_vec_roi(1)*betay_vec_roi(3) - betay_vec_roi(2)^2);
    emitNy0 = emitgy0 * gamma ; % [m rad]
    betay0 = betay_vec_roi(1) / emitgy0; % [m]
    alphay0 = betay_vec_roi(2) / emitgy0;
    sigy_PL = sqrt(betay_vec_roi(1)); % [m]
    
    
    
    % print numbers
    if general_scan
        disp(['QUAD SCAN WITH MAGNET ', num2str(scanned_magnet), ' CAMERA ', camName])
    else
        if downstream_scan
            disp('DOWNSTREAM QUAD SCAN');
        elseif upstream_scan
            disp('UPSTREAM QUAD SCAN');
        end
    end
    disp(['Assuming energy =      ' num2str(energy_MeV)     ' MeV']);
    disp(['Norm. emittance x:     ' num2str(emitNx0*1e6, 4) ' mm mrad']);
    disp(['Beta function x:       ' num2str(betax0*100, 4)  ' cm (' positionString ')']);
    disp(['Alpha function x:      ' num2str(alphax0, 4)     '    (' positionString ')']);
    disp(['Predicted beam size x: ' num2str(sigx_PL*1e6, 4) ' um (' positionString ')']);
    disp('')

    if general_scan
        disp(['QUAD SCAN WITH MAGNET ', num2str(scanned_magnet), ' CAMERA ', camName])
    else
        if downstream_scan
            disp('DOWNSTREAM QUAD SCAN');
        elseif upstream_scan
            disp('UPSTREAM QUAD SCAN');
        end
    end
    disp(['Assuming energy =      ' num2str(energy_MeV)     ' MeV']);
    disp(['Norm. emittance y:     ' num2str(emitNy0*1e6, 4) ' mm mrad']);
    disp(['Beta function y:       ' num2str(betay0*100, 4)  ' cm (' positionString ')']);
    disp(['Alpha function y:      ' num2str(alphay0, 4)     '    (' positionString ')']);
    disp(['Predicted beam size y: ' num2str(sigy_PL*1e6, 4) ' um (' positionString ')']);
    
    
    %% FINAL SCAN PLOT
    
    % plot limits
    Ilims = [min(Is_scan) - mean(diff(Is_scan))/2, max(Is_scan) + mean(diff(Is_scan))/2];
    sigxlims = [0, max(sigxs_mean+sigxs_err)*1.05]*1e3;
    sigylims = [0, max(sigys_mean+sigys_err)*1.05]*1e3;
    
    % prep figure
    figure(2);
    set(gcf, 'color', 'w');
    
    % errorbar plot HORIZONTAL
    subplot(2,1,1);
    errorbar(Is_scan, sigxs_mean*1e3, sigxs_err*1e3, '+', 'Color', hcol);
    hold on;
    plot(Is_scan, sigx_sim0*1e3, ':', 'Color', [1 1 1]*0.5)
    plot(Is_xroi, sigx_sim_roi*1e3, 'k-');
    hold off;
    str_results = ['\rm\epsilon_x = ' num2str(emitNx0*1e6, 4) ' mm mrad, \beta_x = ' num2str(betax0*100, 4) ' cm, \alpha_x = ' num2str(alphax0, 4)];
    if general_scan
        title({['Emittance measurement in X (E = ' num2str(energy_MeV) ' MeV)' ], str_results});
    else
        if downstream_scan
            title({'Quad scan (horizontal) emittance measurement in X', str_results});
        elseif upstream_scan
            title({'Quad scan (vertical) emittance measurement in X', str_results});
        end
    end
    xlabel('Quadrupole current, I (A)');
    ylabel('Beam size on screen, \sigma_x (mm)');
    xlim(Ilims);
    ylim(sigxlims);
    legend(camDesc, 'Full fit (ignored)', 'Narrow range fit');
    
    
    
    % errorbar plot VERTICAL
    subplot(2,1,2);
    errorbar(Is_scan, sigys_mean*1e3, sigys_err*1e3, '+', 'Color', vcol);
    hold on;
    plot(Is_scan, sigy_sim0*1e3, ':', 'Color', [1 1 1]*0.5)
    plot(Is_yroi, sigy_sim_roi*1e3, 'k-');
    hold off;
    str_results = ['\rm\epsilon_y = ' num2str(emitNy0*1e6, 4) ' mm mrad, \beta_y = ' num2str(betay0*100, 4) ' cm, \alpha_y = ' num2str(alphay0, 4)];
    if general_scan
        title({['Emittance measurement in Y (E = ' num2str(energy_MeV) ' MeV)' ], str_results});
    else
        if downstream_scan
            title({'Quad scan (horizontal) emittance measurement in Y', str_results});
        elseif upstream_scan
            title({'Quad scan (vertical) emittance measurement in Y', str_results});
        end
    end
    xlabel('Quadrupole current, I (A)');
    ylabel('Beam size on screen, \sigma_y (mm)');
    xlim(Ilims);
    ylim(sigylims);
    legend(camDesc, 'Full fit (ignored)', 'Narrow range fit');
    
    
end

