function [] = visualizer(dataset_id, cameras, scopes, delayTime)
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset)
        return; 
    end
    datasetInfo(dataset);
    
    % defaults
    if ~exist('delayTime','var')
        delayTime = NaN;
    end
    if ~exist('cameras','var')
        cameras = {};
    end
    if ~exist('scopes','var')
        scopes = {};
    end
    
    % skip if no requested plots
    if numel(scopes) + numel(cameras) == 0
        return;
    end
    
    % prepare figure
    figure(1);
    clf(1);
    set(gcf, 'color', 'w');
    colormap(mod_jet())
    
    % prepare subplots
    hasCameras = (numel(cameras)>0);
    hasScopes = (numel(scopes)>0);
    nrows = hasCameras + hasScopes;
    ncols = max(numel(cameras), numel(scopes));
    
    % cycle all shots
    numShots = dataset.metadata.numShots;
    for i = 1:numShots
        
        % titles
        if dataset.metadata.isScan
            numSteps = dataset.metadata.numSteps;
            step = dataset.scalars.step_num.dat(i);
            progress_title = ['(shot ' num2str(i) '/' num2str(numShots) ', step ' num2str(step) '/' num2str(numSteps) ', dataset ' esc_uscore(dataset_id) ')'];
        else
            progress_title = ['(shot ' num2str(i) '/' num2str(numShots) ', dataset ' esc_uscore(dataset_id) ')'];
        end
            
        % plot images
        for j = 1:numel(cameras)
            
            % get image
            camName = cameras{j}{1};
            camStruct = dataset.images.(camName);
            image = camStruct.dat{i};
            
            % get axes
            if isfield(camStruct, 'xaxis')
                xaxis = camStruct.xaxis;
                yaxis = camStruct.yaxis;
                unitx = camStruct.unitx;
                unity = camStruct.unity;
                disp(yaxis)
            else
                xaxis = 1:size(image,2);
                yaxis = 1:size(image,1);
                unitx = 'px';
                unity = 'px';
            end
            
            % plot
            subplot(nrows, ncols, j);
            imagesc(xaxis, yaxis, image);
            
            % labels
            xlabel(['x (' unitx ')']);
            ylabel(['y (' unity ')']);
            title({['\bf' esc_uscore(camStruct.desc)], ['\rm' progress_title]});
            
            % colorbar
            if numel(cameras{j}) >= 2
                cax = cameras{j}{2};
                if isempty(cax) || numel(cax) ~= 2
                    cax = [0 0];
                end
            else
                cax = [0 0];
            end
            caxis(cax);
            colorbar;
            
        end
        
        % plot scopes
        for j = 1:numel(scopes)
            
            % get scope
            scopeVar = scopes{j}{1};
            scopeStruct = dataset.scopes.(scopeVar);
            scopeDesc = scopeStruct.desc;
            scopeSignal = scopeStruct.dat{i};
            
            % find units
            if isfield(scopeStruct, 'unit')
                unit = scopeStruct.unit;
            else
                unit = 'a.u.';
            end
            if isfield(scopeStruct, 'time_ns')
                ts = scopeStruct.time_ns{i};
                timeUnit = 'ns';
            else
                ts = 0:(numel(scopeSignal)-1);
                timeUnit = 'a.u.';
            end
            
            % plot
            subplot(nrows, ncols, ncols*hasCameras + j);
            plot(ts, scopeSignal);
            
            % labels
            xlabel(['Time (' timeUnit ')']);
            ylabel([scopeDesc ' (' unit ')']);
            title({['\bf' esc_uscore(scopeDesc)], ['\rm' progress_title]});
            
            % set limits
            if numel(scopes{j}) >= 2
                ylimits = scopes{j}{2};
                if isempty(ylimits) || numel(ylimits) ~= 2
                    ylimits = get(gca, 'ylim');
                end
            else
                ylimits = get(gca, 'ylim');
            end
            ylim(ylimits);
            if ~isempty(ts)
                xlim([min(ts), max(ts)]);
            end
        end
        
        % wait for user or update
        commandwindow;
        if isnan(delayTime)
            pause();
        else
            if delayTime==0
                drawnow; % as fast as possible
            else
                pause(delayTime);
            end
        end
        
    end
    
end

