function [xf,xpf] = tracking_thick_JT(u0, R_offset, x0, xp0, E, Ilens,L_lens,L_otr, visu, nSlices, Rother, circ)
%TRACKING_THICK_JT Thick tracking through lens+drift using JT model for B(r)
%
%   Inputs:
%   - u0       : Center temperature as in expected_field
%                Minimum possible (cold wall) is just above 0.0675;
%                too low and we get imaginary numbers.
%   - R_offset : Lens radius [um]
%   - x0       : (Array of) initial positions [um]
%   - xp0      : (Array of) initial beam angles [mrad]
%                If length(1), assume all particles have same initial angle
%   - E        : Beam momentum [MeV/c]
%   - Ilens    : Total current in lens [A]
%   - L_lens   : Length of lens [m]
%   - L_otr    : Length of drift after lens [m]
%   - visu     : Make plot?
%                (default = false)
%   - nSlices  : How many slices to use for integration in lens?
%                If undefined or NaN, default is 1 slice pr. mm e.g. round(L_lens*1e3)
%
%   - Rother   : For elliptical capillaries, this is the other half-axis [um]
%                (unset or NaN for Rother = R)
%                Passed on to expected_field
%
%   - circ     : For elliptical capillaries, this is the circumference [um]
%                (unset or NaN for internal computation)
%                Passed on to expected_field
%
%   Outputs:
%   - xf       : Final beam position [um]
%   - xpf      : Final beam angle    [mrad]
%
%   Example:
%     figure(6); tracking_thick_JT(0.07, 500, -500:50:500, 1.0, 200, 010,15e-3,0.3,true)
%

    SI_c = 299792458; % [m/s] light speed

    if ~exist('Rother','var')
        Rother = NaN;
    end
    if ~exist('circ','var')
        circ = NaN;
    end
    
    if ~exist('visu','var')
        visu = false;
    end
    if visu
        clf()
        hold on
        title(sprintf('Trajectories: u0=%.3f, R=%.0f[µm], E=%.0f[MeV], I=%.0f[A]', u0,R_offset,E,Ilens))
        xlabel('s [m]')
        ylabel('x [mm]')
    end
    
    if ~exist('nSlices','var') || isnan(nSlices)
        nSlices = round(L_lens*1e3);
    end

    if length(xp0) == 1 && length(x0) > 1
        xp0 = xp0*ones(1,length(x0));
    end
    assert (length(x0) == length(xp0));
    
    rs_r = linspace(0,R_offset,100);
    [xf,xpf] = deal(zeros(1,length(x0)));

    [~, BI, ~] = expected_field(u0, R_offset*1e-6, rs_r*1e-6, false, Rother*1e-6, circ*1e-6);
    
    for i = 1:length(x0) % Loop over particles
        %Initialize
        [x,xp,s] = deal(zeros(1,nSlices+2));
        x (1)  = x0(i)*1e-6;   % [m]
        xp(1)  = xp0(i)*1e-3;  % [rad]
        s (1)  = 0.0;          % [m]

        hs = L_lens/nSlices; %step length
        for j = 1:nSlices  % Loop over slices of the lens (drift-kick-drift)
            %Drift
            x_tmp  = x(j) + xp(j)*hs/2.0;
            xp_tmp = xp(j);
            %Kick
            Bx     = -interp1(rs_r*1e-6, BI, abs(x_tmp))*Ilens*sign(x_tmp); %B(x) [T]
            xp_tmp = xp_tmp + hs * Bx * SI_c / (1e6 * E);
            %Drift
            x(j+1)  = x_tmp + xp_tmp*hs/2.0;
            xp(j+1) = xp_tmp;

            s(j+1) = s(j)+hs;
        end
        %Final drift to get to the OTR
        x(end)  = x(end-1) + xp(end-1)*L_otr;
        xp(end) = xp(end-1);
        s(end)  = s(nSlices+1)+L_otr;

        if visu
            plot(s,x*1e3,'-+');
        end
        
        %Populate return arrays
        xf(i)  = x(end)*1e6;  % [um]
        xpf(i) = xp(end)*1e3; % [mrad]
    end
end

