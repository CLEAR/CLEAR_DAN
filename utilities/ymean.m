function [ muy ] = ymean( image )
    
    % remove background
    image = image - min(median(image));
    
    % projection
    yaxis = 1:size(image, 1);
    yproj = sum(image, 2);
    
    % perform gaussian fit
    try
        fg = fit(yaxis', yproj, 'gauss1');
        % extract sigma
        muy = fg.b1;
    catch
        muy = 0;
    end
end

