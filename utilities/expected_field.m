function [grad_enhance, Bs_per_I_sample, u_wall] = expected_field(u0, R, rs_sample, visu, Rother, circ)
    % JT-model calculation
    % 2018, C.A.Lindstrom
    % 2020, K.N.Sjobak
    %
    % See J. van Tilborg et al, "Nonuniform discharge currents in active plasma lenses", PRAB, 2017
    % https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.20.032803
    %
    % Inputs:
    %
    %  - u0        : Normalized electron temperature on axis.
    %                Too small and things get imaginary near the wall (too cold).
    %                Minimum is just above 0.067, 0.0675 seems OK.
    %
    %  - R         : Radius of capillary [m]
    %                For elliptical capillaries, this is the half-axis
    %                in the direction of the scan.
    %                
    %  - rs_sample : Radiuses to sample for output [m]
    %
    %  - visu      : Plot the expected field B/I [mT/A] as function of r [um]?
    % 
    %  - Rother    : For elliptical capillaries, this is the other half-axis. [m]
    %                If NaN or unset -> Rother = R.
    %
    %  - circ      : For elliptical capillaries, this is the circumference [m]
    %                If NaN or unset -> circ is computed with Rajmanuran approximation.
    %
    % Outputs (makes a plot):
    %  - grad_enhance    : On-axis gradient enhancement
    %  - Bs_per_I_sample : B/I [T/A] on points defined by input rs_sample
    %  - u_wall          : Normalized wall electron temperature
    %
    % Example:
    %  expected_field(0.07, 500e-6, linspace(0, 500e-6, 100), 1)
    %  expected_field(0.07, 500e-6, linspace(0, 500e-6, 100), 1, 550e-6)

    
    if ~exist('visu', 'var')
        visu = false;
    end

    if ~exist('Rother','var') || isnan(Rother)
        Rother = R;
    end
    
    if ~exist('circ', 'var')
        circ = NaN;
    end
    if isnan(circ)
        % Rajmanuran approximation
        circ = pi*( 3*(R+Rother) - sqrt((3*R+Rother)*(R+3*Rother)) ); %2*pi*R;
    end
    
    % fundamental constants
    SI_mu0 = 4*pi*1e-7;
    
    % diff equation (ODE): heat diffusion
    function dudx = heatdiff(x, u)
        dudx = [u(2); -u(1).^(3/7)-u(2)./x];
    end

    % calculation of radial field dependence
    [xs, us] = ode45(@heatdiff, [1e-9 1], [u0, 0]);
    us = us(:,1);
    rs = xs*R;
    
    % uniform currents and fields
    %g0_by_I = SI_mu0/(2*pi*R^2); %circular capillary
    g0_by_I = SI_mu0/(R*circ);    %Elliptical cappilary
    B0_by_Is = g0_by_I*rs;
    
    % actual currents and fields
    mI = trapz(xs, abs(us).^(3/7).*xs);
    %Js_by_I = us.^(3/7)/(2*pi*R^2*mI);
    Js_by_I = us.^(3/7)/(2*pi*R*Rother*mI);
    %Bs_by_I = SI_mu0*cumtrapz(rs, Js_by_I.*rs)./rs;
    Bs_by_I = (2*pi*SI_mu0*Rother/circ) * cumtrapz(rs, Js_by_I.*rs)./rs;
    gs_by_I = diff(Bs_by_I)./diff(rs);
    
    % interpolation
    rs_both         = [-rs(2:end); rs];
    Bs_by_I_both    = [-Bs_by_I(2:end); Bs_by_I];
    Bs_per_I_sample = interp1(rs_both, Bs_by_I_both, rs_sample);
    
    % on-axis gradient enhancement
    grad_enhance = gs_by_I(1)/g0_by_I;
    u_wall = us(end);
    
    %% PLOTTING
    
    if visu
        figure(1);
        %set(gcf,'color','w');

        % magnetic field plot
        subplot(1,1,1);
        plot(rs*1e6, Bs_by_I*1e3, '-', rs*1e6, B0_by_Is*1e3, '-', rs_sample*1e6, Bs_per_I_sample*1e3, 'o');
        xlabel('r [\mum]');
        ylabel('B/I [mT/A]');
        title('Magnetic field');
        legend('Model','Uniform','location','northwest');
        
        drawnow;
    end
    
    
    
end

