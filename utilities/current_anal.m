function [Ibeam, Imax, t_Ithresh] = current_anal( currentStruct, shot, visu, t_beam, I_thresh )
    
    % visualize if desired
    if ~exist('visu', 'var')
        visu = false;
    end
        
    % currents
    current_profile = currentStruct.dat{shot};
    ts = currentStruct.time_ns{shot};
    
    % pick out beam time current
    %if ~exist('t_beam', 'var')
    %    t_beam = 8073;
    %elseif t_beam == -1
    %    t_beam = 8073;
    %end
    
    [~, beam_index] = min(abs(ts-t_beam));
    smooth_current = smooth(current_profile, 7);
    Ibeam = current_profile(beam_index);
    Imax  = max(current_profile);
    Imax_smooth = max(smooth_current);
    %For jitter estimates
    if exist('I_thresh','var')
        if Imax_smooth < I_thresh
            t_Ithresh = NaN;
        else
            idx_thresh = find(smooth_current > I_thresh);
            idx_thresh = idx_thresh(1);
            t_Ithresh = ts(idx_thresh);
        end
    else
        t_Ithresh = NaN;
    end
    
    % visualization
    if visu
        clf();
        
        plot(ts, current_profile, ts, smooth_current);
        xlabel('t (ns)');
        ylabel('I (A)');
        xline(t_beam);
        yline(Ibeam);
        
        drawnow();
    end

end

