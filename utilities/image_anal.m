function [ mux, sigx, muy, sigy ] = image_anal( image, visu, image_bg, toptitle )
        
    % visualize if desired
    if ~exist('visu', 'var')
        visu = false;
    end
    if ~exist('toptitle', 'var')
        toptitle = '';
    end
    
    % color scheme
    hcol = [0 0.447 0.741];
    vcol = [0.85 0.325 0.098];
    
    % axes
    xinds = 1:size(image, 2);
    yinds = 1:size(image, 1);
    
    % start cleaning
    image_clean = double(image);
    
    %Subtract background image
    if exist('image_bg', 'var')
        if ~isnan(image_bg)
            image_clean = image_clean - image_bg;
        end
    end
    
    %Apply 2D median filter
    mfo2 = 5;
    image_mf2 = medfilt2(image_clean, mfo2*[1 1]);

    % remove background
    edge = 20;
    bgval = mean([mean(mean(image_clean(1:edge, 1:edge))), ...
                  mean(mean(image_clean(1:edge, (end-edge):end))), ...
                  mean(mean(image_clean((end-edge):end, 1:edge))), ...
                  mean(mean(image_clean((end-edge):end, (end-edge):end)))]);
    image_clean = image_clean - bgval;

    % "projections" used to find ROI
    xproj0 = max(image_mf2);
    yproj0 = max(image_mf2'); %#ok<UDIM> -- don't bother changing & testing

    xproj0 = xproj0 - mean(xproj0([1:edge ((end-edge):end)]));
    yproj0 = yproj0 - mean(yproj0([1:edge ((end-edge):end)]));

    % perform first gaussian fits
    failed = false;
    try
        fx0 = fit(xinds', xproj0', 'gauss1');
        mux0 = fx0.b1;
        sigx0 = fx0.c1/sqrt(2);
    catch
        mux0 = NaN;
        sigx0 = NaN;
        fprintf('x');
        failed = true;
    end
    try
        fy0 = fit(yinds', yproj0', 'gauss1');
        muy0 = fy0.b1;
        sigy0 = fy0.c1/sqrt(2);
    catch
        muy0 = NaN;
        sigy0 = NaN;
        fprintf('y');
        failed = true;
    end

    % define ROI (region of interest)
    nsig = 3; % number of sigmas to include
    xroi = max(1, floor(mux0-nsig*sigx0)):min(numel(xinds), ceil(mux0+nsig*sigx0));
    yroi = max(1, floor(muy0-nsig*sigy0)):min(numel(yinds), ceil(muy0+nsig*sigy0));

    % apply ROI
    image_roi = image_clean(yroi, xroi);

    % remake projections
    xproj = mean(image_roi, 1);
    yproj = mean(image_roi, 2)';

    % apply median filter
    mfo = 3;
    xproj_mf = medfilt1(xproj, mfo);
    yproj_mf = medfilt1(yproj, mfo);


    % perform second gaussian fits
    try
        fx = fit(xroi', xproj_mf', 'gauss1');
        mux = fx.b1; % [px]
        sigx = fx.c1/sqrt(2); % [px]
    catch
        mux = NaN;
        sigx = NaN;
        fprintf('x');
        failed = true;
    end

    try
        fy = fit(yroi', yproj_mf', 'gauss1');
        muy = fy.b1; % [px]
        sigy = fy.c1/sqrt(2); % [px]
    catch
        muy = NaN;
        sigy = NaN;
        fprintf('y');
        failed = true;
    end


    %% plot images (if desired)
    if visu && ~failed
        clf();
        
        % gaussian model for plotting
        gaussmod = @(f, xs) f.a1*exp(-((xs-f.b1)/f.c1).^2);
        
        % show raw image
        subplot(3,1,1);
        imagesc(xinds, yinds, image_clean);
        pos = [min(xroi), min(yroi), max(xroi)-min(xroi), max(yroi)-min(yroi)];
        rectangle('Position', pos, 'EdgeColor', 'r', 'LineWidth', 2);
        colorbar;
        xlabel('x (px)');
        ylabel('y (px)');
        title(['Original image', toptitle]);

        % analysis plots
        subplot(3,1,2);
        %plot(xinds, xproj0, '.', xinds, gaussmod(fx0, xinds), '--', xroi, gaussmod(fx, xroi), '-', 'Color', hcol);
        plot(xinds,xproj0,               '.',  'Color',hcol, 'DisplayName','maxProj (x)');
        hold on;
        plot(xinds,gaussmod(fx0,xinds),  '--', 'Color',hcol, 'DisplayName','maxProj\_fit (x)');
        plot(xroi, xproj_mf,             '*',  'Color',hcol, 'DisplayName','proj (x)');
        plot(xroi, gaussmod(fx,xroi),    '-',  'Color',hcol, 'DisplayName','proj\_fit (x)');
        
        %plot(yinds, yproj0, '.', yinds, gaussmod(fy0, yinds), '--', yroi, gaussmod(fy, yroi), '-', 'Color', vcol);
        plot(yinds,yproj0,              '.',  'Color',vcol, 'DisplayName','maxProj (y)');
        plot(yinds,gaussmod(fy0,yinds), '--', 'Color',vcol, 'DisplayName','maxProj\_fit (y)');
        plot(yroi, yproj_mf,             '*', 'Color',vcol, 'DisplayName','proj (y)');
        plot(yroi, gaussmod(fy,yroi),    '-', 'Color',vcol, 'DisplayName','proj\_fit (y)');
        
        legend({}, 'Location','best','NumColumns',2, 'FontSize',8);
        
        hold off;
        xlim([min([min(xinds),min(yinds)]), max([max(xinds),max(yinds)])]);
        xlabel('x (px)');
        ylabel('Pixel count');
        title('Projections and fits');

        % show cleaned image
        subplot(3,1,3);
        imagesc(xroi, yroi, image_roi);
        xlabel('x (px)');
        ylabel('y (px)');
        title('Cleaned image');
        
        drawnow();

    end
    
end

