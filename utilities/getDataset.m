function [dataset, dataset_id, dataset_path] = getDataset(dataset_id,loadDataset, rawFile)
    % Locate and load a processed dataset, identified by a numeric dataset_id OR by filename.
    % Arguments:
    %  dataset_id   : The numerical ID of a dataset,
    %                 or the filename of the processed file.
    %  loadDataset  : If false, don't actually load the dataset
    %                 (saving time), just identify the filename.
    %                 Default: true
    %  rawFile      : If true, return the raw dataset file,
    %                 not the processed one.
    %                 Default: false
    % Returns:
    %  dataset      : The loaded dataset (or NaN if not found or loadDataset is false)
    %  dataset_id   : The input dataset_id, as a string
    %  dataset_path : The full path to the datset (or NaN if not found)

    if ~exist('loadDataset','var')
        loadDataset = true;
    end
    if loadDataset == false
        dataset = NaN;
    end

    if ~exist('rawFile','var')
        rawFile = false;
    end

    if ~exist('dataset_id')
        error('Must provide a dataset_id')
    end

    if ischar(dataset_id)
        if rawFile
            error('Setting rawFile only makes sense together with numeric dataset_id')
        end
        dataset_path = dataset_id;
        if loadDataset
            load(dataset_path,'dataset');
        end
        return;
    else
        % make sure the dataset number is a string
        dataset_id = num2str(dataset_id);
    end

    path = strrep(mfilename('fullpath'), mfilename, '');

    % load the dataset index
    try
        if rawFile
            load([path 'dataset_index_raw.mat'], 'dataset_map');
        else
            load([path 'dataset_index.mat'],     'dataset_map');
        end
    catch
        disp('No dataset index: making a new one.');
        dataset_map = containers.Map;
    end
    % search the dataset index file
    if isKey(dataset_map, dataset_id)
        dataset_path = dataset_map(dataset_id);
        if loadDataset
            load(dataset_path, 'dataset');
        end
        return;

    else % if not found, search the file system and save in index
        
        % get list of folders to search (from DAQ config)
        addpath('..')
        danconfig = DANCONFIG();
        addpath(danconfig.DAQPATH)
        config = CONFIG();
        expt_folders = config.expts(:,3);
        
        % run through all experiments
        for i = 1:numel(expt_folders)
            if rawFile
                expt_folder = [expt_folders{i} 'raw'];
            else
                expt_folder = [expt_folders{i} 'processed'];
            end
            disp(['Searching ' expt_folder]);
            
            % get date folders
            date_struct = dir(expt_folder);
            dates = {date_struct.name};
            dates = dates(3:end);
            
            % look through all date folders
            for j = 1:numel(dates)
                date = dates{j};
                if rawFile
                    search_path = [expt_folder '/' date '/' dataset_id '_raw.mat'];
                else
                    search_path = [expt_folder '/' date '/' dataset_id '.mat'];
                end
                % if found, set path and save it
                if ~~numel(dir(search_path))
                    dataset_path = search_path;
                    dataset_map(dataset_id) = dataset_path;
                    if rawFile
                        save([path 'dataset_index_raw.mat'], 'dataset_map');
                        disp('Added to the RAW dataset index!');
                    else
                        save([path 'dataset_index.mat'], 'dataset_map');
                        disp('Added to the dataset index!');
                    end
                    
                    if loadDataset
                        load(dataset_path, 'dataset');
                    end
                    return;
                end
            end
            
        end
        
    end
    
    % notify if not found
    disp('Dataset not found!');
    dataset = NaN;
    dataset_path = NaN;

end

