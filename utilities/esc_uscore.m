function [ str_esc ] = esc_uscore( str )

    % function to put backslashes before underscores in strings
    str_esc = strrep(str, '_', '\_');

end

