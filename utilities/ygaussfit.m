function [ sigmay ] = ygaussfit( image, visu )
    
    if ~exist('visu','var')
        visu = false;
    end
    [ ~, ~, ~, sigmay ] = image_anal( image, visu );
    
    %{
    % remove background
    image = image - min(median(image));
    
    % projection
    yaxis = 1:size(image, 1);
    yproj = sum(image, 2);
    
    % perform gaussian fit
    fg = fit(yaxis', yproj, 'gauss1');
    
    % extract sigma
    sigmay = fg.c1/sqrt(2);
    %}
    
end

