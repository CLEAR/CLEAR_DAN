function [calib_x,calib_y] = getCameraCalib_fromDataset(dataset,camname)
    %GETCAMERACALIB_FROMDATASET Summary of this function goes here
    %   Detailed explanation goes here
    
    %Loop over cameras to find the right index
    camIdx = NaN;
    for i=1:length(dataset.metadata.camVars)
        if strcmp(dataset.metadata.camVars{i}, camname)
            camIdx = i;
            break;
        end
    end
    if isnan(camIdx)
        disp('Valid camnames')
        for i=1:length(dataset.metadata.camVars)
            disp(['''' dataset.metadata.camVars{i}, ''''])
        end
        error(['Camera ' camname ' not found in dataset, see '])
        
    end

    calib_x = dataset.metadata.camCalibrationsX{camIdx};
    calib_y = dataset.metadata.camCalibrationsY{camIdx};
    
end

