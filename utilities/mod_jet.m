function mjet = mod_jet()
    
    % modified jet with white at low values
    % (MODIFIED BY CARL LINDSTROM, JUNE 2017)
    D = [1 1 1;
         0 0 0.6;
         0 0.8 0.8;
         1 0.95 0;
         1 0 0;
         0.5 0 0;];
    F = [0 0.20 0.35 0.50 0.80 1];
    G = linspace(0, 1, 256);
    mjet = interp1(F,D,G);
    
end