function [ sigmax ] = xgaussfit( image, visu )
    
    if ~exist('visu','var')
        visu = false;
    end
    [ ~, sigmax, ~, ~ ] = image_anal( image, visu );
    
    %{
    % remove background
    image = image - min(median(image));
    
    % projection
    xaxis = 1:size(image, 2);
    xproj = sum(image, 1);
    
    % perform gaussian fit
    fg = fit(xaxis', xproj', 'gauss1');
    
    % extract sigma
    sigmax = fg.c1/sqrt(2);
    %}
end