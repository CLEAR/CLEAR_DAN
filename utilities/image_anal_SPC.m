function [mux_SI, muy_SI, sigx_SI, sigy_SI, charge] = image_anal_SPC(dataset, shot, visu, screenID)
    
    %% EXTRACT IMAGES
    
    % get image
    if screenID == 930
        image0 = double(dataset.images.DNS_SPC.dat{shot});
    elseif screenID == 420
        image0 = double(dataset.images.UPS_SPC.dat{shot});
    elseif screenID == 440
        image0 = double(dataset.images.VSP_SPC.dat{shot});
    else
        error('Unknown screenID')
    end
    
    % axes
    margin = 0;
    xinds = (1+margin):(size(image0, 2)-margin);
    yinds = (1+margin):(size(image0, 1)-margin);
    
    % get background image
    %BG = load('backgrounds/DNS_SPC_BG_IMAGE.mat', 'bg_image');
    %bg_image = BG.bg_image;
    
    
    %% IMAGE ANALYSIS
    
    % subtract background image
    %image = image0 - bg_image;
    image = image0;
    
    % threshold at zero
    image(image < 0) = 0;
    
    % apply general ROI
    image_cut = image(yinds, xinds);
    
    
    %% FIND ROI
    
    % 2D median filter
    mfo2 = 5;
    image_mf2 = medfilt2(image_cut, mfo2*[1 1]);
    
    % maximum projection
    xmaxs = max(image_mf2)';
    ymaxs = max(image_mf2')';
    
    % subtract floor
    xmaxs = xmaxs - median(xmaxs);
    ymaxs = ymaxs - median(ymaxs);
    
    % gaussian fits of maximum projections
    fmx0 = fit(xinds', xmaxs, 'gauss1');
    mux0 = fmx0.b1;
    sigx0 = fmx0.c1/sqrt(2);
    
    fmy0 = fit(yinds', ymaxs, 'gauss1');
    muy0 = fmy0.b1;
    sigy0 = fmy0.c1/sqrt(2);
    
    % new ROI (narrow)
    nsig = 3;
    xroi = max(1,ceil(mux0-nsig*sigx0)):min(max(xinds),floor(mux0+nsig*sigx0));
    yroi = max(1,ceil(muy0-nsig*sigy0)):min(max(yinds),floor(muy0+nsig*sigy0));
    
    % additional ROI (wide, for fits)
    nsig_w = 7;
    xroi_w = max(1,ceil(mux0-nsig_w*sigx0)):min(max(xinds),floor(mux0+nsig_w*sigx0));
    yroi_w = max(1,ceil(muy0-nsig_w*sigy0)):min(max(yinds),floor(muy0+nsig_w*sigy0));
    
    % updated, real projections
    xproj0 = mean(image(yroi, :), 1)';
    yproj0 = mean(image(:, xroi), 2);
    % subtract background and apply ROI
    try
        bgx = median(xproj0(xinds < min(xroi) | xinds > max(xroi)));
        bgy = median(yproj0(yinds < min(yroi) | yinds > max(yroi)));
    catch
        bgx = 0;
        bgy = 0;
    end
    xproj = xproj0(xroi_w) - bgx;
    yproj = yproj0(yroi_w) - bgy;
    
    % perform final gaussian fit
    fx = fit(xroi_w', xproj, 'gauss1');
    mux = fx.b1;
    sigx = fx.c1/sqrt(2);
    
    fy = fit(yroi_w', yproj, 'gauss1');
    muy = fy.b1;
    sigy = fy.c1/sqrt(2);
    
    % charge from pixel count
    Q_calib = 1; % [pC/count]
    charge = sum(sum(image(yroi, xroi)))*Q_calib; % TODO Q-calibration
    
    
    %% ADD CALIBRATIONS
    if screenID == 930
    
        % calibrations
        calib_x = 152.4e-6; % [m/px]
        calib_y = 152.4e-6; % [m/px]
    
        % offsets
        x0 = 200; % [px]
        y0 = 150; % [px]
    else
        error(['No callibrations for screen ', num2str(screenID), ' found.'])
    end
    
    % find offset and beam size in x
    mux_SI = (mux-x0)*calib_x; % [m]
    sigx_SI = sigx*calib_x; % [m]
    
    % find offset and beam size in y
    muy_SI = (muy-y0)*calib_y; % [m]
    sigy_SI = sigy*calib_y; % [m]
    
    %% PLOT IMAGE
    if ~exist('visu', 'var')
        visu = false;
    end
    if visu
        
        gaussmodel = @(f, xs) f.a1*exp(-((xs - f.b1)/f.c1).^2);
        
        subplot(4,2,1:4);
        pos = [min(xroi), min(yroi), max(xroi)-min(xroi), max(yroi)-min(yroi)];
        imagesc(xinds, yinds, image);
        rectangle('Position', pos, 'EdgeColor', 'r', 'LineWidth', 2);
        hold on;
        plot(mux, muy, '+', 'Color', 'r', 'LineWidth', 2);
        hold off;
        title('Near-field OTR image');
        xlabel('x (px)');
        ylabel('y (px)');
        caxis([0 (fmx0.a1+fmy0.a1)/2*1.6]);
        
        subplot(4,2,5);
        plot(xinds, xmaxs, '.', xinds, gaussmodel(fmx0, xinds));
        title('Horizontal maximum projection');
        xlabel('x (px)');
        ylabel('Count');
        xlim([min(xinds) max(xinds)]);
        
        subplot(4,2,6);
        plot(yinds, ymaxs, '.', yinds, gaussmodel(fmy0, yinds));
        title('Vertical maximum projection');
        xlabel('y (px)');
        ylabel('Count');
        xlim([min(yinds) max(yinds)]);
        
        subplot(4,2,7);
        plot(xroi_w, xproj, '.', xroi_w, gaussmodel(fx, xroi_w));
        title('Horizontal projection');
        xlabel('x (px)');
        ylabel('Count');
        xlim([min(xroi_w) max(xroi_w)]);
        
        subplot(4,2,8);
        plot(yroi_w, yproj, '.', yroi_w, gaussmodel(fy, yroi_w));
        title('Vertical projection');
        xlabel('y (px)');
        ylabel('Count');
        xlim([min(yroi_w) max(yroi_w)]);
        
        drawnow;
    end
    
end

