function [ mux ] = xmean( image )
    
    % remove background
    image = image - min(median(image));
    
    % projection
    xaxis = 1:size(image, 2);
    xproj = sum(image, 1);
    
    % perform gaussian fit
    try
        fg = fit(xaxis', xproj', 'gauss1');
        % extract sigma
        mux = fg.b1;
    catch
        mux = 0;
    end
    
end