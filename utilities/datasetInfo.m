function [] = datasetInfo(dataset)
    
    % shortcut name for metadata
    MD = dataset.metadata;

    % dataset name and size
    if isfield(MD, 'startTime')
        disp(['Dataset ' num2str(MD.dataset_id) ' (taken ' datestr(MD.startTime) ')']);
    elseif isfield(MD, 'dateTime')
        disp(['Dataset ' num2str(MD.dataset_id) ' (taken ' datestr(MD.dateTime) ')']);
    end
    
    % comment
    disp(MD.comment);
    
    % scan info
    if MD.isScan
        if ~MD.isScan2D
            disp(['Scan of "' MD.scanName '" (' num2str(MD.numSteps) ' steps)']);
        else
            disp(['2D scan of "' MD.scanName '" and "' MD.scanName '"']); % TODO: change to second scan name
            disp([num2str(MD.numSteps) ' steps by ' num2str(MD.numSteps) ' steps, ' num2str(MD.shotsPerStep) ' shots per step']);
        end
    else
        disp('Simple DAQ');
    end    
    disp([num2str(MD.numShots) '/' num2str(MD.numShotsRequested) ' shots collected']);
    
    % available cameras
    cams = fieldnames(dataset.images);
    fprintf('Available images: {')
    for i = 1:numel(cams)
        if i < numel(cams)
            fprintf(['{''' cams{i} '''},']);
        else
            fprintf(['{''' cams{i} '''}']);
        end 
    end
    disp('}');
    
    % available scopes
    scopes = fieldnames(dataset.scopes);
    fprintf('Available scopes: {')
    for i = 1:numel(scopes)
        if i < numel(scopes)
            fprintf(['{''' scopes{i} '''},']);
        else
            fprintf(['{''' scopes{i} '''}']);
        end
    end
    disp('}');
end