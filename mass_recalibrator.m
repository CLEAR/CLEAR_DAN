function [filenames] = mass_recalibrator( dataset_ids, recal_name )
    % Mass convert an array of datasets using DAQ_datasetConverter.
    % Arguments:
    %  dataset_ids : Cell array or numerical vector of dataset_id,
    %                 as accepted by getDataset()
    %  recal_name  : Tag to append at the end of the dataset filename
    %                by DAQ_datasetConverter.
    %                For no tag (i.e. overwrite mode) set to NaN.
    %                Default = 'recal'
    % 
    % Returns:
    %  filenames   : Cell array of character arrays containing the
    %                successfully converted file names.
    %
    % A warning is raised whenever a dataset could not be located.
    %  At the end of the run, the number of not-located datasets
    %  are issued as a warning, if this number is > 0.

    if ~exist('recal_name','var')
        recal_name = 'recal';
    end

    % add DAQ folder with DAQ_datasetConverter to the PATH
    danconfig = DANCONFIG();
    addpath(danconfig.DAQPATH)
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);

    if isvector(dataset_ids)
        %Convert vector to cellarray
        di = {};
        for i=1:length(dataset_ids)
            di{i} = dataset_ids(i); %#ok<AGROW> 
        end
        dataset_ids = di;
    end

    filenames = {};
    j = 0; % index for filenames etc., skipping missing data
    
    for i = 1:numel(dataset_ids)
        [~,dataset_id,dataset_filename] = getDataset(dataset_ids{i}, false, true);
        if isnan(dataset_filename)
            warning(['Dataset with id=', dataset_id, ' NOT FOUND. Skipping.'])
        else
            disp(['Loading dataset_id=', dataset_id, ', filename=', dataset_filename]);
            try
                [filename,  ~] = DAQ_datasetConverter(dataset_filename,recal_name,true);
                j = j+1;
                filenames{j} = filename; %#ok<AGROW> 
            catch e
                warning(['Conversion of dataset ', dataset_id, ' failed'])
                eText = getReport(e);
                disp(eText)
                disp(' ')
            end
        end
    end
    
    missingData = i-j;
    if missingData > 0
        warning(['Number of listed datasets unconverted: ', num2str(missingData)])
    end
end