function addPointToGradientTime(t,gEnh,label)
%ADDPOINTTOGRADIENTTIME
% Adds a point of measured gradient enhancement (from a mover scan)
% to an active figure(10) gradient enhancement plot.
%   
% Input:
%  t    : The time on the x-axis of the point
%  gEnh : The enhanced gradient
%  label: For the legend
%

    figure (10)
    hold on
    global hObjs;
    global hLegends;
    assert (~isempty(hLegends));
    assert (length(hObjs) == length(hLegends))
    
    global scatterMarkerProgression
    if isempty(scatterMarkerProgression)
        scatterMarkerProgression = 0;
    end
    %scatterMarkers = {'o','+','*','x','s','d','^','v','>','<','p','h'};
    scatterMarkers = {'o','s','d','^','v','>','<','p','h'};
    scatterMarker = scatterMarkers{mod(scatterMarkerProgression,length(scatterMarkers))+1};
    scatterMarkerProgression = scatterMarkerProgression + 1;
    
    hScatter = scatter(t,gEnh, 100, scatterMarker, 'MarkerFaceColor', hObjs(1).Color);
    
    
    hObjs(length(hObjs)+1) = hScatter;
    hLegends(length(hLegends)+1) = {label};
    legend(hObjs, hLegends, 'Location', 'best', 'NumColumns',2);
end

