function [dataset_id, Ibeams_mean, Ibeams_err, gExp, gFit, gEnh] = ...
    offsetscan(dataset_id, dataset_id_ref, E_MeV, visu, I_uniform, t_beam, R, holdLegend, inOut, dataset_id_bg, cropY, cropX, negativeI, removeSteps, RfitFrac, beamAngle, nMC, offsetsScaling)
    % Function to plot the data from an offset scan
    %
    % Input parameters:
    %  - dataset_id     : The id of the main dataset // int
    %  - dataset_id_ref : The id of the dataset to use for
    %                     position reference // int
    %  - E_MeV          : Beam energy in MeV,
    %                     to convert deflection to field // float
    %  - t_beam         : Scope-time of beam arrival //int
    %                     as found using findTbeam.m
    % Optional input parameters:
    %  - visu           : Visualize the data analysis step-by-step // bool
    %                     Default = false
    %
    %  - I_uniform      : Current to use for the gradient plot // int
    %                     If  > 0 : Use the given current.
    %                     If  < 0 : Take the current from I(t_beam), and scale by -1*I_uniform.
    %                     If == 0 : Like <=0, but leave the scaling free for JT fits.
    %                     Default = -1
    %
    %  - R              : Capillary radius in um // float
    %                     Default = 500.0
    %                     Also possible to specify [Rx, Ry]
    %
    %  - holdLegend     : Legend for this data in the final plot // str;
    %                     if not present or NaN then make a new plot.
    %  - inOut          : Which current probe to use, 'in' or 'out. // str;
    %                     Defaults to 'out' (for reproducibility reasons)
    %
    %  - dataset_id_bg  : The id of the dataset with the background images
    %                     to subtrack from the beam images with discharge.
    %                     Set to NaN to ignore.
    %  - cropY, cropX   : Range of pixels to not throw away for the analysis.
    %                     Default = NaN (disable cropping)
    %  - negativeI      : Flag to multiply the current with -1
    %                     Default = false
    %  - removeSteps    : List of position-points to skip (beam off etc.).
    %                     Default: []
    %  - RfitFrac       : Fraction of the aperture to use for fitting
    %                     the central gradient. Default = 0.3 (NaN -> default).
    %  - beamAngle      : Beam angle for thick lens JT fit
    %                     Default = 0
    %                     NaN to try and fit it (0 used for plotting)
    %                     If an imaginary value is given, the imaginary
    %                     part is used for plotting linear expected kick,
    %                     otherwise treated as NaN. Zero real value is
    %                     then expected.
    % - nMC             : Monte Carlo samples to use for error estimation
    %                     of JT gradient and enhancement.
    %                     Default = 0 => Skip this error estimate.
    % - offsetsScaling  : Scaling of beam offsets (mover callibration).
    %                     Default = 1.0
    % Returns:
    % - Average current at t_beam [A]
    % - Sigma of current at t_beam [A]
    % - Expected gradient [T/m]
    % - Fitted central gradient [T/m]
    % - Gradient enhancement
    
    % parfor setup
    numWorkers = gcp().NumWorkers;
    
    % visualize if desired
    if ~exist('visu', 'var')
        visu = false;
    else
        if visu
            numWorkers = 0;
        end
    end
    
    disp(['numWorkers = ', num2str(numWorkers)]);
    
    if ~exist('t_beam','var')
        t_beam = -1;
    end
    if ~exist('I_uniform','var')
        I_uniform = -1;
    end
    if ~exist('R','var')
        R = 500.0; % [um]
    end
    if length(R) == 1
        Rx = R;
        Ry = R;
    elseif length(R) == 2
        Rx = R(1);
        Ry = R(2);
    else
        error('R must be a scalar or a length 2 array [Rx,Ry]');
    end

    if ~exist('negativeI', 'var')
        negativeI = false;
    end
    if ~exist('removeSteps', 'var')
        removeSteps = [];
    end
    
    if (exist('cropY','var') && ~exist('cropX','var')) || (~exist('cropY','var') && exist('cropX','var'))
        error('Must specify both cropY and cropX, or none of them.')
    end
    if ~exist('cropX','var')
        cropX = NaN;
    else
        if (length(cropX) == 1 && ~isnan(cropX))
            error('CropX must be an array or NaN')
        end 
    end
    if ~exist('cropY','var')
        cropY = NaN;
    else
        if (length(cropY) == 1 && ~isnan(cropY))
            error('CropY must be an array or NaN')
        end 
    end
    
    if ~exist('beamAngle','var')
        beamAngle = 0.0;
        beamAnglePlot = 0.0;
    else
        if isreal(beamAngle)
            if isnan(beamAngle)
                beamAnglePlot = 0.0;
            else
                beamAnglePlot = beamAngle;
            end
        else
            beamAnglePlot = imag(beamAngle);
            beamAngle = NaN;
        end
    end
    
    if ~exist('nMC', 'var')
        nMC = 0;
    end
    
    if ~exist('offsetsScaling', 'var')
        offsetsScaling = 1.0;
    end
    
    camName = 'PLE_DIG';
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    %Attempt to create folder for plots
    plotFolder = 'offsetscanPlots';
    mkdir(plotFolder)
    
    % cleaning helper function
    prc_cut = 90;
    prc_mask = @(m) m < prctile(m, prc_cut) & m > prctile(m, 100-prc_cut);
    
    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset)
        return; 
    end
    datasetInfo(dataset);
    disp(' ');
    
    % select measurement screen (OTR, spectrometer)
    camFields = fields(dataset.images);
    if numel(camFields) > 1
        if ~exist('camName','var')
            disp('Please provide camera name as argument');
            return;
        end
    else
        camName = camFields{1};
    end
    camStruct = dataset.images.(camName);
    %camDesc = camStruct.desc;
    
    % select current waveform
    if ~exist('inOut','var')
        inOut = 'out';
    end
    if strcmp(inOut, 'in')
        scopeName = 'DIS_CUR_IN';
    elseif strcmp(inOut, 'out')
        scopeName = 'DIS_CUR_OUT';
    else
        disp(inOut)
        error("Expected inOut either 'in' or 'out'");
    end

    fprintf('Using scopeName = %s\n\n', scopeName);
    currentStruct = dataset.scopes.(scopeName);
    
    I_thresh = 300.0;
    
    beam_off_current_limit = 400; % [A]

    % get common camera variables
    if strcmp(camName, 'FAR_OTR')
        calib_x = 0.0153; % mm/px
        calib_y = 0.0167; % mm/px
    elseif strcmp(camName, 'PLE_OTR')
        %Valid until MAY19
        %calib_x = 0.0286; % mm/px
        %calib_y = -0.0305; % mm/px
        
        %From MAY19 ->
        %calib_x = 0.0368;
        %calib_y = -0.0389;
        
        %From OCT19 ->
        calib_x = 0.0395;
        calib_y = -0.0413;
        
        L_otr = 0.2895; % [m]
    
    elseif strcmp(camName, 'PLE_DIG')
        %Rough assumption
        %calib_x = 0.02;
        %calib_y = 0.02;
        
        %Load from datafile
        calib_x = NaN;
        calib_y = NaN;

        L_otr = 0.2895; % [m]
    end
    
    % color scheme (vertical / horizontal)
    figure(99)
    MY_COLORS = colororder('default');
    MY_COLORS_ = MY_COLORS(4,:);
    MY_COLORS(4,:) = MY_COLORS(5,:);
    MY_COLORS(5,:) = MY_COLORS_;
    clear MY_COLORS_;
    close(99)
    
    %hcol  = [0 0.447 0.741];
    %vcol  = [0.85 0.325 0.098];
    %fcol  = [0.5, 1.0, 0.5];
    %fcol2 = [1.0, 0.0, 1.0];
    
    hcol  = MY_COLORS(1,:);
    vcol  = MY_COLORS(2,:);
    fcol2 = MY_COLORS(3,:); %JT fit
    fcol  = MY_COLORS(4,:); %Central gradient
    
    %How much of the aperture to use for gradient fits?
    if ~exist('RfitFrac','var') || isnan(RfitFrac)
        RfitFrac = 0.3;
    end
    
    % horizontal or vertical scan
    if ~isempty(strfind(dataset.metadata.scanName,'Plasma lens mover horizontal'))
        horscan = true;
        verscan = false;
        xy = 'x';
        R_offset = Rx;
        R_other  = Ry;
    elseif ~isempty(strfind(dataset.metadata.scanName, 'Plasma lens mover vertical'))
        horscan = false;
        verscan = true;
        xy = 'y';
        R_offset = Ry;
        R_other  = Rx;
    else
        disp('Not an offset scan!');
        return;
    end
    
    
    %% POSITION REFERENCE OFF DATASET
    
    % load the reference dataset
    dataset_ref = getDataset(dataset_id_ref);
    if ~isstruct(dataset_ref)
        return; 
    end
    datasetInfo(dataset_ref);
    disp(' ');
    
    % grab same image structure from the reference dataset
    camStruct_ref = dataset_ref.images.(camName);
    if isnan(calib_x)
        [calib_x,calib_y] = getCameraCalib_fromDataset(dataset_ref,camName);
    end

    % cycle all scan shots in the reference dataset
    numShots_ref = dataset_ref.metadata.numShots;
    [muxs_ref, muys_ref, sigxs_ref, sigys_ref] = deal(nan(1,numShots_ref));
    %Better to split variables outside of PARFOR
    camStruct_ref_dat = camStruct_ref.dat;

    parfor (i = 1:numShots_ref, numWorkers)
        
        %% IMAGE ANALYSIS
        
        % progress text (not parfor)
        %if mod(i-1,50)==0 && i ~= 1
        %    disp(' ');
        %end
        
        % get image
        image_ref = camStruct_ref_dat{i};
        
        %crop image
        if (~isnan(cropY(1)) && ~isnan(cropX(1))) && ~isempty(image_ref) %#ok<PFBNS>
            %assert (length(cropX) == 1)
            %assert (length(cropY) == 1)
            image_ref = image_ref(cropY,cropX);
        end
        
        % analyse image
        if visu
            figure(1);
            %set(gcf, 'color', 'w');
            %colormap(mod_jet());
        end
        if ~isempty(image_ref)
            [mux_ref, sigx_ref, muy_ref, sigy_ref] = image_anal(image_ref, visu);
        else
            disp(['BadCam!', num2str(i)]);
            mux_ref = NaN;
            muy_ref = NaN;
            sigx_ref = NaN;
            sigy_ref = NaN;
        end
        
        % calibrated beam positions
        muxs_ref(i)  = mux_ref*calib_x;  % [mm]
        muys_ref(i)  = muy_ref*calib_y;  % [mm]
        sigxs_ref(i) = sigx_ref*abs(calib_x); % [mm]
        sigys_ref(i) = sigy_ref*abs(calib_y); % [mm]
        
        % display progress
        fprintf('.');
    end

    disp(' ');
    clear camStruct_ref_dat;
    
    % find the reference beam positions
    mux0  = mean(muxs_ref(prc_mask(muxs_ref))); % [mm]
    muy0  = mean(muys_ref(prc_mask(muys_ref))); % [mm]
    sigx0 = mean(sigxs_ref(prc_mask(muxs_ref))); % [mm]
    sigy0 = mean(sigys_ref(prc_mask(muys_ref))); % [mm]
    fprintf('mux0  = %d (mm), muy0  = %d (mm)\n', mux0, muy0);
    fprintf('sigx0 = %d (mm), sigy0 = %d (mm)\n\n', sigx0, sigy0);
    
    %find the mean image
    if exist('dataset_id_bg','var') && ~isnan(dataset_id_bg)
        dataset_bg = getDataset(dataset_id_bg);
        if ~isstruct(dataset_bg)
            disp('Loading of dataset_bg failed');
            return;
        end
        datasetInfo(dataset_bg);
        disp(' ');
        
        
        % grab the image structure
        camStruct_bg = dataset_bg.images.(camName);
        % cycle all scan shots
        numShots_ref = dataset_bg.metadata.numShots;
        for i = 1:numShots_ref
            if i == 1
                bg_image = double(camStruct_bg.dat{i});
            else
                bg_image = bg_image + double(camStruct_bg.dat{i});
            end
        end
        bg_image = bg_image / numShots_ref;
        
        if exist('cropY','var') && exist('cropX','var')
            bg_image = bg_image(cropY,cropX);
        end
        
    else
        bg_image = NaN;
    end
    
    
        
    %% ANALYZE THE SCAN
    
    % cycle all scan shots
    numShots = dataset.metadata.numShots;
    [muxs, muys, sigxs, sigys, Ibeams, Imax, t_Ithresh] = deal(nan(1,numShots));
    %Better to split variables outside of PARFOR
    camStruct_dat = camStruct.dat;
    step_num_dat = dataset.scalars.step_num.dat;
    currentStruct_dat = currentStruct.dat;

    parfor (i = 1:numShots, numWorkers)
        
        %% IMAGE ANALYSIS
        
        % progress text (not parfor)
        %if mod(i-1,50)==0 && i ~= 1
        %    disp(' ');
        %end
        
        % get image
        if visu
            figure(4);
        end
        %image = camStruct.dat{i};
        image = camStruct_dat{i};
        
        % analyse image
        if ~isempty(image)
            if (~isnan(cropY(1)) && ~isnan(cropX(1))) %#ok<PFBNS>
                image = image(cropY,cropX);
            end
            toptitle = sprintf(', step = %d, shot = %d', step_num_dat(i), i);
            [mux, sigx, muy, sigy] = image_anal(image, visu, bg_image, toptitle);
        else
            disp(['BadCam!', num2str(i)]);
            mux = NaN;
            muy = NaN;
            sigx = NaN;
            sigy = NaN;
        end

        % calibrated beam positions
        muxs(i) = mux*calib_x; % [mm]
        muys(i) = muy*calib_y; % [mm]
        
        % calibrated beam sizes
        sigxs(i) = sigx*abs(calib_x); % [mm]
        sigys(i) = sigy*abs(calib_y); % [mm]

        % current analysis
        if visu
            figure(3);
        end
        if i < length(currentStruct_dat) && ~isempty(currentStruct_dat{i})
            [Ibeams(i), Imax(i), t_Ithresh(i)] = current_anal(currentStruct, i, visu, t_beam, I_thresh); %#ok<PFOUS>
        else
            disp(['NoScope!' num2str(i)]);
            Ibeams(i) = NaN;
            Imax(i) = 0.0;
            t_Ithresh(i) = NaN;
        end
        
        % display progress
        fprintf('.');
        
    end

    disp(' ');
    clear camStruct_dat step_num_dat current_struct_dat;
    
    
    %% DO STATISTICS
    
    % Position on screen
    steps = unique(dataset.scalars.step_num.dat);
    offsets = unique(dataset.scalars.step_value.dat) * offsetsScaling;
    [muxs_mean, muxs_err, muys_mean, muys_err, sigxs_mean, sigxs_err, sigys_mean, sigys_err, nxs, nys] = deal(zeros(1, numel(steps)));
    keepPoints = ones(1,length(dataset.scalars.step_num.dat));
    for j = 1:numel(steps)
        
        step_num = steps(j);
        step_numShots = sum(dataset.scalars.step_num.dat == j);
        
        if ismember(j,removeSteps)
            fprintf('Skipping step # %i, position = %g [um]\n', j, offsets(j))
            keepPoints(dataset.scalars.step_num.dat == step_num) = 0;
            continue
        end
        
        %Select the shots that correspond to our step and are of OK quality
        step_mask = (dataset.scalars.step_num.dat == step_num & ~isnan(Ibeams) & Imax > beam_off_current_limit);
        
        % get beam centroid positions in x,
        % filtering out bad shots (NaN) and outliers (percentile mask)
        step_muxs = muxs(~isnan(muxs) & step_mask);
        prcmask_x = prc_mask(step_muxs);
        muxs_mean(j) = mean(step_muxs(prcmask_x)); % [mm]
        muxs_err(j)  = std( step_muxs(prcmask_x)); % [mm]
        
        if (sum(prcmask_x) ~= step_numShots)
            fprintf(' In step # %i, position %g [um] : Removed %i steps in X\n', j, offsets(j), step_numShots-sum(prcmask_x));
        end
        
        step_sigxs    = sigxs(~isnan(sigxs) & step_mask);
        sigxs_mean(j) = mean(step_sigxs(prcmask_x));
        sigxs_err(j)  = std (step_sigxs(prcmask_x));

        nxs(j) = sum(~isnan(muxs) & step_mask); %number of shots for each position

        % get beam centroid positions in y,
        % filtering out bad shots (NaN) and outliers (percentile mask)
        step_muys = muys(~isnan(muys) & step_mask);
        prcmask_y = prc_mask(step_muys);
        muys_mean(j) = mean(step_muys(prcmask_y)); % [mm]
        muys_err(j)  = std( step_muys(prcmask_y)); % [mm]

        if (sum(prcmask_y) ~= step_numShots)
            fprintf(' In step # %i, position %g [um] : Removed %i steps in Y\n', j, offsets(j), step_numShots-sum(prcmask_y));
        end
        
        step_sigys    = sigys(~isnan(sigys) & step_mask);
        sigys_mean(j) = mean(step_sigys(prcmask_y));
        sigys_err(j)  = std (step_sigys(prcmask_y));

        nys(j) = sum(~isnan(muys) & step_mask); %number of shots for each position
    end
    disp(' ')
    
    %remove to-be-removed points
    if ~isempty(removeSteps)
        steps    (removeSteps) = [];
        offsets  (removeSteps) = [];
        
        muxs_mean (removeSteps) = [];
        muxs_err  (removeSteps) = [];
        sigxs_mean(removeSteps) = [];
        sigxs_err (removeSteps) = [];
        nxs       (removeSteps) = [];
        
        muys_mean (removeSteps) = [];
        muys_err  (removeSteps) = [];
        sigys_mean(removeSteps) = [];
        sigys_err (removeSteps) = [];
        nys       (removeSteps) = [];
    end
    
    % Variation in beam position
    std_x = sqrt(mean(muxs_err.^2));
    std_y = sqrt(mean(muys_err.^2));
    fprintf('std_x = %f (mm)\n',   std_x);
    fprintf('std_y = %f (mm)\n\n', std_y);
    
    % Beam size
    fprintf('Average horizontal size = %f (mm)\n',   mean(sigxs_mean));
    fprintf('Average vertical   size = %f (mm)\n\n', mean(sigys_mean));

    % Current at t_beam
    currentMask = (~isnan(Ibeams) & Imax > beam_off_current_limit & keepPoints);
    Ibeams_masked = Ibeams(currentMask);
    Imax_masked   = Imax(currentMask);
    prcmask_I   = prc_mask(Ibeams_masked);
    
    Ibeams_mean = mean(Ibeams_masked(prcmask_I)); %[A]
    Ibeams_err  = std(Ibeams_masked(prcmask_I));  %[A]
    fprintf('Ibeams_mean = %f.1 [A], Ibeams_err = %f.1 [A]\n', Ibeams_mean, Ibeams_err);

    Imax_mean = mean(Imax_masked(prcmask_I)); %[A]
    Imax_err  =  std(Imax_masked(prcmask_I)); %[A]
    fprintf('Imax_mean = %f.1 [A], Imax_err = %f.1 [A]\n\n', Imax_mean, Imax_err);
    
    %% MAGNETIC FIELD CALIBRATION
    
    SI_c = 299792458; % [m/s] light speed
    SI_mu0 = 4*pi*1e-7; 
    
    % calculate the magnetic field calibration
    E = E_MeV;
    %L_lens = 0.015;%2017-2023
    L_lens = 0.020; %2024
    B_per_offset = 1e-3*E*1e6/(L_lens*L_otr*SI_c); % [T/mm]
    
    %% PREPARE FINAL PLOTTING
    
    % Current jitter
    %figure(11)
    %histogram(t_Ithresh)
    %xlabel('Time of I_{thresh}')
    
    %figure(12)
    %histogram(Ibeams)
    %xlabel(['I_{beam} [A] (',inOut,')'])
    %ylabel('Counts')
    %if ~isnan(Ibeams_mean) || ~isnan(Ibeams_err)
    %    xline(Ibeams_mean);
    %    xline(Ibeams_mean - Ibeams_err);
    %    xline(Ibeams_mean + Ibeams_err);
    %end
    
    % prep figure 10 [ B(r) ]
    figure(10)
    global hErrObjs;
    global hErrLegends;
    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend))
        clf();
        clear global hErrObjs
        clear global hErrLegends
        lcol2 = fcol;
        lcol3 = fcol2;
    else
        hcol = '';
        vcol = '';
        lcol2 = '';
        lcol3 = '';
        if isempty(hErrObjs)
            clf();
        end
    end
    hold on;
    set(gca(),'TickLabelInterpreter', 'latex')
    box on;
    
    % prep figure 13 [ rFinal(rInitial) - rInitial ]
    figure(13)
    global hErrObjs_offset;
    global hErrLegends_offset;
    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend))
        clf();
        clear global hErrObjs_offset
        clear global hErrLegends_offset
    else
        if isempty(hErrObjs_offset)
            clf();
        end
    end
    hold on;
    set(gca(),'TickLabelInterpreter', 'latex')
    box on;
    
    % prep figure 14 [ std_y as a function of rInitial ]
    figure(14)
    clf() % no holdLegend support here
    hold on
    set(gca(),'TickLabelInterpreter', 'latex')
    
    % Prep figure 15 [ Beam size ]
    figure(15)
    clf() % no holdlegend support here
    hold on
    set(gca(),'TickLabelInterpreter', 'latex')

    % mask for finding the central offsets
    mid_mask = offsets > -RfitFrac*R_offset & offsets < RfitFrac*R_offset;
    
    %% Plot the data

    if horscan
        dxys     = (muxs_mean-mux0);
        dxys_err = muxs_err;
        nxys     = nxs;
        col1 = hcol;
        col2 = vcol;
    elseif verscan
        dxys     = (muys_mean-muy0);
        dxys_err = muys_err;
        nxys     = nys;
        col1 = vcol;
        col2 = hcol;
    end
    %Find center (no kick) assuming no beam angle / thin lens
    pxy = polyfit(offsets(mid_mask), dxys(mid_mask), 1);
    offset0 = -pxy(2)/pxy(1);
    fprintf('offset0 = %f [um]\n\n', offset0);
        
    %Fit the central part
    offsets_shifted  = offsets-offset0;
    mid_mask2 = offsets_shifted > -RfitFrac*R_offset & offsets_shifted < RfitFrac*R_offset;
    %Fit for linear gradient (B(r) plot)
    pxy2 = polyfit(offsets_shifted(mid_mask2), dxys(mid_mask2)*B_per_offset,1);
    gFit = pxy2(1); % [T/um]
    
    %Fit for displacement on screen
    %pxy3 = polyfit(offsets(mid_mask), dxys(mid_mask),1);
    M3 = [(offsets(mid_mask))', ones(length(offsets(mid_mask)),1)];
    pxy3 = lscov(M3,dxys(mid_mask)', 1./((dxys_err(mid_mask)./sqrt(nxys(mid_mask))).^2));
    gFit_off  = pxy3(1); % [mm/um]
    gFit0_off = pxy3(2); % [mm]
    
    
    %Plot errorbars (without/with holdlegend; thin and thick)
    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend))
        figure(10)
        herr = errorbar(offsets_shifted, dxys*B_per_offset, dxys_err*B_per_offset./sqrt(nxys(mid_mask)), '+', 'Color', col1, 'LineWidth', 1.5);
        lcol = col2; % To be used for expected gradient plot (make it opposite)
        
        figure(13)
        herr_off = errorbar(offsets, dxys, dxys_err./sqrt(nxys(mid_mask)), '+', 'Color', col1, 'LineWidth', 1.5);

    else
        figure(10)
        herr = errorbar(offsets_shifted, dxys*B_per_offset, dxys_err*B_per_offset, '+');
        lcol = herr.Color; %To be used for expected gradient plot
        lcol2 = lcol;      %To be used for central gradient fit
        lcol3 = lcol;      %To be used for JT fit
        if isempty(hErrObjs)
            hErrObjs    = [herr]; %#ok<NBRAK>
            hErrLegends = {holdLegend};
        else
            hErrObjs(length(hErrObjs)+1)       = herr;
            hErrLegends(length(hErrLegends)+1) = {holdLegend};
        end

        figure(13)
        herr_off = errorbar(offsets, dxys, dxys_err, '+', 'Color', lcol);
        if isempty(hErrObjs_offset)
            hErrObjs_offset = [herr_off];
            hErrLegends_offset = {holdLegend};
        else
            hErrObjs_offset(length(hErrObjs_offset)+1)       = herr_off;
            hErrLegends_offset(length(hErrLegends_offset)+1) = {holdLegend};
        end
    end


    figure(14)
    plot(offsets, dxys_err);

    % Beam size
    figure(15)
    h_sigx = errorbar(offsets, sigxs_mean, sigxs_err, 'Color', hcol);
    h_sigy = errorbar(offsets, sigys_mean, sigys_err, 'Color', vcol);

    %% Plot the expected gradient from current probes
    rs = linspace(-R_offset, R_offset, 100);
    rs_endpoints = [-R_offset, R_offset]*1.0;
    if Rx ~= Ry
        %Ramanujan approx of ellipse circumference
        circumference = pi*( 3*(Rx+Ry)-sqrt((3*Rx+Ry)*(Rx+3*Ry)) ); %[um]
    else
        circumference = 2*pi*R;
    end
    if I_uniform <= 0
        if I_uniform == 0 %Sort out flags
            I_uniform_ = -1;
        else
            I_uniform_ = I_uniform;
        end
        if ~negativeI     %Also flip the sign convention away from "negative means scaling factor"
            I_uniform_ = -I_uniform_;
        else
            I_uniform_ =  I_uniform_; %#ok<ASGSL>
        end
        
        gExp       = SI_mu0 *  Ibeams_mean*I_uniform_                   / (R_offset*circumference*1e-12); % (2 * pi * (R_offset*1e-6)^2);
        
        gExpPlot   = SI_mu0 *  Ibeams_mean*sign(I_uniform_)             / (R_offset*circumference*1e-12);
        gExpPlot_H = SI_mu0 * (Ibeams_mean+Ibeams_err)*sign(I_uniform_) / (R_offset*circumference*1e-12);
        gExpPlot_L = SI_mu0 * (Ibeams_mean-Ibeams_err)*sign(I_uniform_) / (R_offset*circumference*1e-12);
        
        I_legend = Ibeams_mean*sign(I_uniform_);
    else
        if ~negativeI
            I_uniform_ = I_uniform;
        else
            I_uniform_ = -I_uniform;
        end
        gExp = SI_mu0 * I_uniform_ / (R_offset*circumference*1e-12);
        gExpPlot = gExp;
        
        I_legend = I_uniform_;
    end
    fprintf('Expected gradient = %.1f [T/m] (scaled current)  \n', gExp);
    fprintf('Expected gradient = %.1f [T/m] (unscaled current)\n', gExpPlot);
    figure(10)
    BsPlot = gExp*rs*1e-6;
    hExp = plot(rs, BsPlot, 'Color', lcol);
        
    figure(13)
    
    gExp_sK = sqrt(gExp*SI_c/(E_MeV*1e6)); %Used for plotting and as a starting point for solving
    %xf_thick = cos(gExp_sK*L_lens) - gExp_sK*(L_otr-L_lens/2)*sin(gExp_sK*L_lens) - 1; %mm/mm
    %xf_thick_angleOffset = ( sin(gExp_sK*L_lens)/gExp_sK + (L_otr-L_lens/2)*cos(gExp_sK*L_lens) ) * beamAnglePlot; %mm
    %Remember, screen is "inverted" because the lens is moving, not beam.
    
    
    gExpPlot_sK = sqrt(gExpPlot*SI_c/(E_MeV*1e6));
    xfPlot_thick = cos(gExpPlot_sK*L_lens) - gExpPlot_sK*(L_otr-L_lens/2)*sin(gExpPlot_sK*L_lens) - 1; %mm/mm
    xfPlot_thick_angleOffset = ( sin(gExpPlot_sK*L_lens)/gExpPlot_sK + (L_otr-L_lens/2)*cos(gExpPlot_sK*L_lens) ) * beamAnglePlot; %mm
    hExp_off = plot(rs_endpoints, -rs_endpoints/1e3*xfPlot_thick - xfPlot_thick_angleOffset, '-o', ...
        'Color', lcol,'MarkerFaceColor',lcol, 'LineWidth',1.0);
    
    %Plot a shaded area for the error of the expected gradient
    %Disabled for now (almost invisible)
%     figure(10)
%     if I_uniform <= 0
%         BsPlotH = gExpPlot_H*rs*1e-6;
%         BsPlotL = gExpPlot_L*rs*1e-6;
%         fill([rs, flip(rs)], [BsPlotH, flip(BsPlotL)],lcol,'LineStyle','none','FaceAlpha',.3);
%     end
% 
%     figure(13)
%     if I_uniform <= 0
%         gExpPlot_sK_H  = sqrt(gExpPlot_H*SI_c/(E_MeV*1e6));
%         xfPlot_thick_H = cos(gExpPlot_sK_H*L_lens) - gExpPlot_sK_H*(L_otr-L_lens/2)*sin(gExpPlot_sK_H*L_lens) - 1;
%     
%         gExpPlot_sK_L  = sqrt(gExpPlot_L*SI_c/(E_MeV*1e6));
%         xfPlot_thick_L = cos(gExpPlot_sK_L*L_lens) - gExpPlot_sK_L*(L_otr-L_lens/2)*sin(gExpPlot_sK_L*L_lens) - 1;
%         
%         fill([rs_endpoints, flip(rs_endpoints)], [-xfPlot_thick_H*rs_endpoints/1e3, flip(-xfPlot_thick_L*rs_endpoints/1e3)] - xf_thick_angleOffset, ...
%             lcol,'LineStyle','none','FaceAlpha',.3);
%     end
    
    %% Compute and plot the central gradient fit
    % 29/6/2020: Disable plotting, rather just use the JT fit
    figure(10)
    hFit = plot([-RfitFrac*R_offset,RfitFrac*R_offset], [-gFit*RfitFrac*R_offset, gFit*RfitFrac*R_offset], 'Color', lcol2);
    gFit = gFit*1e6; % [T/m]
    gEnh = gFit/gExp;
    fprintf('Central gradient (inside +/- %f*R = %.0f [um]) = %f [T/m]\n', RfitFrac, RfitFrac*R_offset, gFit);
    fprintf('Gradient enhancement = %.3f\n', gEnh);
    fprintf('\n')
    %fprintf('Expect enhancement = 1.0 if R = %g [um]\n', sqrt(1/gEnh)*R);
    
    %How much is the apparent gradient reduced due to the thick-lens effect?
    g_sK = sqrt(gExp * SI_c / (E_MeV*1e6)); % Square root of magnet K
    gEnh_thick = (cos(g_sK*L_lens) - g_sK*(L_otr-L_lens/2)*sin(g_sK*L_lens) - 1) / ...
        (-gExp * L_otr * L_lens * SI_c / (E_MeV * 1e6));
    
    fprintf('Expected thick-lens enhancement if g = %f [T/m]: %f (scaled current)\n' , gExp,  gEnh_thick);
    
    figure(13)
    %Fit to central gradient
    hFit_off = plot([-RfitFrac*R_offset,RfitFrac*R_offset], [-gFit_off*RfitFrac*R_offset, gFit_off*RfitFrac*R_offset]+gFit0_off, ...
        '-+', 'Color', lcol2, 'MarkerSize', 18, 'LineWidth', 1);
    function rem = rootLinearThick(sK,gFit_off_in)
        rem = -gFit_off_in*1e3 - cos(sK*L_lens) + sK*(L_otr-L_lens/2)*sin(sK*L_lens) + 1;
    end
    sK_fit = fsolve(@(sK)rootLinearThick(sK,gFit_off), gExp_sK);
    g_thickFit = sK_fit^2 * E_MeV * 1e6 / SI_c;
    gEnhThick = g_thickFit/gExp;
    gEnhPlotThick = g_thickFit/gExpPlot;
    fprintf('\n');
    fprintf('Gradient from thick-lens fit: %f [T/m]\n', g_thickFit);
    fprintf('Gradient enhancement compared to expectation: %f (scaled current)\n', gEnhThick);
    fprintf('Gradient enhancement compared to expectation: %f (unscaled current)\n\n', gEnhPlotThick);
    deltaR = (cos(gExp_sK*L_lens)-1)*R_offset;
    fprintf('Passing through DeltaR = %f [um] assuming g= %f [T/m] and at starting position R = %f [um] \n', deltaR, gExp, R_offset);
    %fprintf('Expect enhancement = 1.0 if R = %g [um]\n', sqrt(1/gEnhThick)*R);
    P_g0 = gExp * SI_c / (1e6 * sK_fit^2 );
    fprintf('Expect enhancement = 1.0 if P = %g [MeV/c]\n', P_g0);
    
    gFit_off_scaling_fit = fsolve(@(offsetsScaling_fit_tmp)rootLinearThick(sqrt((gExp*SI_c)/(E_MeV*1e6)),gFit_off*offsetsScaling_fit_tmp), 1.0);
    fprintf('Scaling of gFit_off for gEnhThick = 1.0: %f\n', gFit_off_scaling_fit)

    
    %% Compute and plot the JT-model fit
    
    % Thin %
    if visu
        figure(5)
    end
    function rem = jtFit_thin_resFun(fit_in)
        %TODO: expected_field has it's own interpolation, so we could
        %probably get away with a single call...

        u0     = fit_in(1);
        Iscale = fit_in(2);
        
        idx_l = offsets_shifted  < 0 & offsets_shifted > -R_offset;
        [~, Bs_per_I_sample_l, ~] = expected_field(u0, R_offset*1e-6, -offsets_shifted(idx_l)*1e-6, visu, R_other*1e-6, circumference*1e-6);
        Bs_per_I_sample_l = -Bs_per_I_sample_l;
        
        idx_r = offsets_shifted >= 0 & offsets_shifted <  R_offset;
        [~, Bs_per_I_sample_r, ~] = expected_field(u0, R_offset*1e-6, offsets_shifted(idx_r)*1e-6, visu, R_other*1e-6, circumference*1e-6);
        
        Bs_per_I_sample = [Bs_per_I_sample_l, Bs_per_I_sample_r];
        idx_lr = idx_l + idx_r;
        assert(sum(idx_lr >= 2) == 0)
        idx_lr = logical(idx_lr);
        
        rem = Bs_per_I_sample*Ibeams_mean*Iscale - dxys(idx_lr)*B_per_offset;
        rem = rem ./ ((dxys_err(idx_lr)*B_per_offset)./sqrt(nxys));
        if visu
            disp([num2str(u0), ' ', num2str(sum(rem.^2)) ]) % , ' : ', num2str(Bs_per_I_sample)])
        end
    end
    if I_uniform < 0 %Current scaling is fixed
        jtFit_thin_fitParams = lsqnonlin(@(fit_in)jtFit_thin_resFun([fit_in(1),I_uniform_]), 0.5, 0.0675, 500.0);
        jtFit_thin_fitParams = [jtFit_thin_fitParams(1), I_uniform_];
    else
        jtFit_thin_fitParams = lsqnonlin(@jtFit_thin_resFun, [0.5, 1.0], [0.0675, 0.5], [500.0, 1.5]);
    end
    %disp(jtFit_thin_fitParams);
    
    rs_r = linspace(0,R_offset,50);
    rs_l = -fliplr(rs_r);
    rs_b = [rs_l(1:end-1),rs_r];
    
    [jtFit_thin_grad_enhance, jtFit_thin_Bs_per_I_sample, jtFit_thin_u_wall] = ...
        expected_field(jtFit_thin_fitParams(1), R_offset*1e-6, rs_r*1e-6, false, R_other*1e-6, circumference*1e-6);
    
    fprintf('\n')
    fprintf('Central gradient enhancement from JT model    (thin lens fit) : %f\n',     jtFit_thin_grad_enhance);
    fprintf('Temperature at center and at wall in JT model (thin lens fit) : %f, %f\n', jtFit_thin_fitParams(1), jtFit_thin_u_wall);
    fprintf('Iscale from JT model                          (thin lens fit) : %f\n',     jtFit_thin_fitParams(2) );

    figure(10)
    hFit_JT = plot(rs_b, [-fliplr(jtFit_thin_Bs_per_I_sample(2:end)), jtFit_thin_Bs_per_I_sample]*Ibeams_mean*jtFit_thin_fitParams(2), 'Color', lcol3);
    
    % Thick %
    if visu
        figure(6);
    end

    function rem = jtFit_thick_resFun2(fit_in,dxys_toFit) %Fit both temp 'u0', 'Iscale', and angle 'ang'
        u0     = fit_in(1);
        Iscale = fit_in(2);
        ang    = fit_in(3);
        
        [xf,~] = tracking_thick_JT(u0, R_offset, offsets, ang, E, Ibeams_mean*Iscale, L_lens, L_otr-L_lens/2, visu, NaN, R_other, circumference);
        idx_nans = isnan(xf); %out of aperture
        
        rem = -(xf-offsets)*1e-3 - dxys_toFit; %[mm]; Note that the screen is "upside-down"!
        rem = rem ./ (dxys_err./sqrt(nxys));
        rem = rem(~idx_nans); %remove out-of-aperture
        
        if visu
            disp([num2str(u0), ' ', num2str(sum(rem.^2)), ' : ', num2str(xf)])
        end
    end
    
    if ~isnan(beamAngle)
        if I_uniform < 0 %Current scaling is fixed
            jtFit_thick_fitParams = lsqnonlin(@(fit_in) jtFit_thick_resFun2([fit_in(1),I_uniform_,beamAngle],dxys), 0.5, 0.0675, 500.0);
            jtFit_thick_fitParams = [jtFit_thick_fitParams, I_uniform_];
        else %Current scaling is free
            jtFit_thick_fitParams = lsqnonlin(@(fit_in) jtFit_thick_resFun2([fit_in(1),fit_in(2),beamAngle],dxys), [0.5,1.0], [0.0675,0.5], [500.0,1.5]);
        end
    else
        if I_uniform < 0
            jtFit_thick_fitParams = lsqnonlin(@(fit_in) jtFit_thick_resFun2([fit_in(1),I_uniform_, fit_in(2)],dxys), [0.5,0.0], [0.0675,-20], [500.0,20]);
            jtFit_thick_fitParams = [jtFit_thick_fitParams(1), I_uniform_, jtFit_thick_fitParams(2)];
        else
            jtFit_thick_fitParams = lsqnonlin(@(fit_in) jtFit_thick_resFun2(fit_in,dxys), [0.5,1.0,0.0], [0.0675,0.5,-20], [500.0,1.5,20]);
        end
        beamAngle = jtFit_thick_fitParams(3);
    end
    
    [jtFit_thick_grad_enhance, jtFit_thick_fieldSample, jtFit_thick_u_wall] = ...
        expected_field(jtFit_thick_fitParams(1), R_offset*1e-6, rs_r*1e-6, false, R_other*1e-6, circumference*1e-6);
    jtFit_thick_grad_center = Ibeams_mean * jtFit_thick_fitParams(2) * (jtFit_thick_fieldSample(2)-jtFit_thick_fieldSample(1)) / ( (rs_r(2)-rs_r(1))*1e-6 );
    
    fprintf('\n')
    fprintf('Central gradient enhancement from JT model    (thick lens fit) : %f\n',        jtFit_thick_grad_enhance);
    fprintf('Temperature at center and at wall in JT model (thick lens fit) : %f, %f\n',    jtFit_thick_fitParams(1), jtFit_thick_u_wall);
    fprintf('Central gradient in JT model                  (thick lens fit) : %f [T/m]\n',  jtFit_thick_grad_center);
    fprintf('Beam angle for JT model tracking              (thick lens fit) : %f [mrad]\n', beamAngle);
    fprintf('Iscale from JT model                          (thick lens fit) : %f \n',       jtFit_thick_fitParams(2) );

    
    %Error estimation with MC
    if nMC > 0
        fprintf('Running MC error estimation...\n');

        optimset_quiet_options = optimset('Display','off');
        
        %Variables to sample
        g_thickFit_MC = zeros(1,nMC);
        gEnhThick_MC  = zeros(1,nMC);
        
        jtFit_thick_grad_enhance_MC = zeros(1,nMC);
        jtFit_thick_fitParams_1_MC  = zeros(1,nMC);
        jtFit_thick_fitParams_2_MC  = zeros(1,nMC);
        jtFit_thick_u_wall_MC       = zeros(1,nMC);
        jtFit_thick_grad_center_MC  = zeros(1,nMC);

        for i=1:nMC
            dxys_MC = normrnd(dxys,dxys_err); %Sample with actual distribution; estimated-error-on-mean is used when fitting
            
            %Thick linear fit

            pxy3_MC_tmp = lscov(M3,dxys_MC(mid_mask)', 1./((dxys_err(mid_mask)./sqrt(nxys(mid_mask))).^2));
            gFit_off_MC_tmp  = pxy3_MC_tmp(1); % [mm/um]

            sK_fit_MC_tmp = fsolve(@(sK)rootLinearThick(sK,gFit_off_MC_tmp), gExp_sK, optimset_quiet_options);
            g_thickFit_MC(i) = sK_fit_MC_tmp^2 * E_MeV * 1e6 / SI_c;
            gEnhThick_MC(i) = g_thickFit_MC(i)/gExp;
            
            %Thick JT-model fit

            %We always know beam angle at this point.
            % Start fit from previously fitted u0

            if I_uniform < 0 %Current scaling is fixed
                [jtFit_thick_fitParams_MC_tmp,~,~,exitflag] = ...
                    lsqnonlin(@(fit_in)jtFit_thick_resFun2([fit_in,I_uniform_,beamAngle],dxys_MC), jtFit_thick_fitParams(1), 0.0675, 500.0, optimset_quiet_options);
                jtFit_thick_fitParams_MC_tmp = [jtFit_thick_fitParams_MC_tmp, I_uniform_]; %#ok<AGROW>
            else %Current scaling is free
                [jtFit_thick_fitParams_MC_tmp,~,~,exitflag] = ...
                    lsqnonlin(@(fit_in)jtFit_thick_resFun2([fit_in,beamAngle],dxys_MC), jtFit_thick_fitParams(1:2), [0.0675,0.5], [500.0,1.5], optimset_quiet_options);
            end
            jtFit_thick_fitParams_1_MC(i) = jtFit_thick_fitParams_MC_tmp(1);
            jtFit_thick_fitParams_2_MC(i) = jtFit_thick_fitParams_MC_tmp(2);
            if exitflag > 0
                [jtFit_thick_grad_enhance_MC(i), jtFit_thick_fieldSample_MC, jtFit_thick_u_wall_MC(i)] = ...
                    expected_field(jtFit_thick_fitParams_1_MC(i), R_offset*1e-6, rs_r*1e-6, false, R_other*1e-6, circumference*1e-6);
                jtFit_thick_grad_center_MC(i) = Ibeams_mean * jtFit_thick_fitParams_2_MC(i) * (jtFit_thick_fieldSample_MC(2)-jtFit_thick_fieldSample_MC(1)) / ( (rs_r(2)-rs_r(1))*1e-6 );
            else
                disp(['Bad solve at i=', num2str(i), ', exitflag=',num2str(exitflag)])
                jtFit_thick_fitParams_1_MC(i) = NaN;
                jtFit_thick_fitParams_2_MC(i) = NaN;
                jtFit_thick_grad_enhance(i)   = NaN;
                jtFit_thick_fieldSample_MC(i) = NaN;
                jtFit_thick_u_wall_MC(i)      = NaN;
                jtFit_thick_grad_center_MC(i) = NaN;
            end

            fprintf('.');
            if (mod(i,50) == 0)
                fprintf([' ',num2str(i),'/',num2str(nMC),'\n']);
            end
        end
        fprintf('\n')
        
        %Compute stats for sampled variables
        fprintf('With MC sampling of beam position -- written as mean (sigma) :\n')

        fprintf('Gradient from thick-lens fit:                 %f (%f) [T/m]\n', ...
            mean(g_thickFit_MC), std(g_thickFit_MC) );
        fprintf('Gradient enhancement compared to expectation: %f (%f)\n\n', ...
            mean(gEnhThick_MC),std(gEnhThick_MC) );

        fprintf('Central gradient enhancement from JT model    (thick lens fit) : %f ( %f )\n', ...
            mean(jtFit_thick_grad_enhance_MC), std(jtFit_thick_grad_enhance_MC) );
        fprintf('Temperature at center and at wall in JT model (thick lens fit) : %f ( %f ), %f ( %f )\n', ...
            mean(jtFit_thick_fitParams_1_MC), std(jtFit_thick_fitParams_1_MC), ...
            mean(jtFit_thick_u_wall_MC), std(jtFit_thick_u_wall_MC) );
        fprintf('Central gradient in JT model                  (thick lens fit) : %f ( %f ) [T/m]\n', ...
            mean(jtFit_thick_grad_center_MC), std(jtFit_thick_grad_center_MC) );
        fprintf('Iscale from JT model                          (thick lens fit) : %f ( %f )\n', ...
            mean(jtFit_thick_fitParams_2_MC), std(jtFit_thick_fitParams_2_MC) );

    end
    
    jtFit_thick_dxys = tracking_thick_JT(jtFit_thick_fitParams(1), R_offset, rs_b, beamAngle, E, Ibeams_mean*jtFit_thick_fitParams(2), L_lens, L_otr-L_lens/2, visu, NaN, R_other, circumference);
    figure(13)
    hFit_JT_thick = plot(rs_b, -(jtFit_thick_dxys - rs_b)*1e-3, 'Color', lcol3, 'LineStyle', '-.', 'LineWidth',2.0);
    
    figure(6) % Just for the figure
    tracking_thick_JT(jtFit_thick_fitParams(1), R_offset, offsets, beamAngle, E, Ibeams_mean*jtFit_thick_fitParams(2), L_lens, L_otr-L_lens/2, true, NaN, R_other, circumference);
    
    %% Prettify the plots        
    %  Make the legend and axis titles
    
    % FIGURE 10 (THIN)
    fig10 = figure(10);

    hLine = xline(R_offset);
    xline(-R_offset);
    yline(0.0, ':');

    if horscan
        xlabel('Horizontal lens offset ($\mu$m)', 'Interpreter','latex');
    elseif verscan
        xlabel('Vertical lens offset ($\mu$m)', 'Interpreter','latex');
    end

    ylabel('Magnetic field, $B_{\phi}$ (T)', 'Interpreter','latex');

    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend))
        if horscan
            title(['Horizontal transverse offset scan \rm(dataset ' dataset_id ')']);
        else
            title(['Vertical transverse offset scan \rm(dataset ' dataset_id ')']);
        end

        legend([herr, hFit, hExp, hFit_JT, hLine], ... % [herr, hExp, hFit, hFit_JT, hLine], ... %29/6/2020 disable ploting of central gradient
               {'Measurement', ...
               sprintf('Expected gradient (I = %.0f [A])', I_legend), ...
               sprintf('Linear fit (enh. = %.3f)', gEnh), ...
               sprintf('JT fit (enh. = %.2f)', jtFit_thin_grad_enhance), ...
               'Aperture'}, ...
               'Location', 'best')
        legend boxoff
        saveas(fig10, [plotFolder, '/', 'offsetscan_Bfield_', num2str(dataset_id), '.png']);
    else
        legend(hErrObjs, hErrLegends, 'Location', 'northwest');
    end

    % FIGURE 13 (THICK)
    fig13 = figure(13);

    hLine_off = xline(R_offset);
    xl = xlim();
    yl = ylim();
    patch( [R_offset,R_offset,R_offset*2,R_offset*2], [yl,flip(yl)]*2, 'black', 'FaceAlpha', 0.1, 'EdgeColor', 'none');
    patch(-[R_offset,R_offset,R_offset*2,R_offset*2], [yl,flip(yl)]*2, 'black', 'FaceAlpha', 0.1, 'EdgeColor', 'none');
    xline(-R_offset);
    %yline(0.0, ':');
    
    xlim(xl);
    ylim(yl);

    if horscan
        xlabel('Horizontal lens offset ($\mu$m)', 'Interpreter','latex');
        ylabel('Displacement on screen $\Delta x$ (mm)', 'Interpreter','latex');
    elseif verscan
        xlabel('Vertical lens offset ($\mu$m)', 'Interpreter','latex');
        ylabel('Displacement on screen $\Delta y$ (mm)', 'Interpreter','latex');
    end

    uistack(herr_off,'top')
    
    if ~exist('holdLegend','var') || (length(holdLegend)==1 && isnan(holdLegend))
        if horscan
            title(['Horizontal transverse offset scan \rm(dataset ' dataset_id ')']);
        else
            title(['Vertical transverse offset scan \rm(dataset ' dataset_id ')']);
        end

        h = legend([hExp_off, hFit_off, hFit_JT_thick, herr_off, hLine_off], ...
               {sprintf('Expected from current, linear\nI = %.0f (A), $\\mathrm{d}B/\\mathrm{d}%s$ = %.2f (kT/m)', I_legend, xy, gExpPlot/1e3), ...
               sprintf('Linear fit to offsets\n($\\mathrm{d}B/\\mathrm{d}%s$ = %.2f [kT/m], $g_{enh} = %.2f$)', xy, g_thickFit/1e3, gEnhPlotThick)...
               sprintf('JT-model fit\n$\\mathrm{d}B/\\mathrm{d}%s|_{%s=0}$ = %.2f (kT/m), $g_{enh}$ = %.2f', xy,xy, jtFit_thick_grad_center/1e3 ,jtFit_thick_grad_enhance), ...
               'Measurement', 'Aperture'}, ...
               'Location', 'northwest', 'Interpreter','latex');
        h.Box = 'off';
        h.Position(1) = h.Position(1)+0.02;
        h.Position(2) = h.Position(2)-0.02;
        saveas(fig13, [plotFolder, '/', 'offsetscan_offset_', num2str(dataset_id), '.png']);
    else
        legend(hErrObjs_offset, hErrLegends_offset, 'Location', 'best');
    end

    % FIGURE 14 (ERROR CORRELATION)
    figure(14)
    if horscan
        title(['Horizontal transverse offset scan errors \rm(dataset ' dataset_id ')']);
        ylabel('Horizontal beam final position RMS (mm)', 'Interpreter','latex')
        xlabel('Horizontal lens offset ($\mu$m)', 'Interpreter','latex');
    elseif verscan
        title(['Vertical transverse offset scan errors \rm(dataset ' dataset_id ')']);
        ylabel('Vertical beam final position RMS (mm)', 'Interpreter','latex')
        xlabel('Vertical lens offset ($\mu$m)', 'Interpreter','latex');
    end

    % FIGURE 15 (BEAM SIZE)
    figure(15)
    title(['Beam size \rm(dataset ' dataset_id ')']);
    ylabel('Beam size (mm)', 'Interpreter','latex')
    if horscan
        xlabel('Horizontal lens offset ($\mu$m)', 'Interpreter','latex');
    elseif verscan
        xlabel('Vertical lens offset ($\mu$m)', 'Interpreter','latex');
    end
    legend([h_sigx, h_sigy], {'Horizontal', 'Vertical'}, 'Interpreter','latex')

    %% Preparation for return
    dataset_id = str2num(dataset_id); %#ok<ST2NM>
    
    %Sound!
    load('splat', 'y', 'Fs')
    sound(y,Fs)
end

