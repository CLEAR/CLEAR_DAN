function [] = waterfall(dataset_id, cameras, scopes,  filename, fignum)
    
    % USAGE INSTRUCTIONS and EXAMPLES:
    % 
    % Description:
    %    A quick tool for converting many images and scopes to a single
    %    waterfall plot. A function (typically a projection) must be
    %    applied to images (2D -> 1D) in order to make a single line per
    %    shot in the waterfall. Many different signals can be shown
    %    simultaneously. Datasets are found automatically based on their
    %    ID. The use of anonymous functions (e.g. @(x)sin(2*x) ) allows
    %    very versatile and fast handling of the data.
    %
    % Input parameters:
    %    dataset_id = five digit dataset id number // int
    %    cameras = {{'NAME_OF_CAM1',@projectionFunction}, {...}} // cell of cells
    %    scopes = {{'NAME_OF_SCOPE1'}, {...}}
    % Optional input parameters:
    %    fileame = filename of alternative path,
    %    not using the automatic path finder // string.
    %    Defaults to '' -> Automatic pathfinder.
    %    fignum = Figure number to plot to // int. Defaults to 2.
    %
    %  Note that both cameras and scopes default to {}, however at least of
    %  them must be given.
    %
    % Examples:
    %    Show the capillary cam x-projection for dataset 10200:
    %        >> waterfall(10200, {{'DIS_CAP', @(img) mean(img,2)}})
    %    Show the capillary cam y-projection for dataset 10200:
    %        >> waterfall(10200, {{'DIS_CAP', @(img) mean(img,1)}})
    %    Show only one scope:
    %        >> waterfall(10200, {}, {{'DIS_CUR_OUT'}})
    %    Show two scopes, inverting one:
    %        >> waterfall(10200, {}, {{'DIS_CUR_IN'}, {'DIS_CUR_OUT', @(x) -x}})
    %    Show the capillary cam x-projection and a current scope for
    %    dataset 10200:
    %        >> waterfall(10200, {{'DIS_CAP',@(img) mean(img,2)}}, {{'DIS_CUR_OUT'}})
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % load the dataset
    if exist('filename','var') && ~strcmp(filename,'')
        load(filename, 'dataset');
    else
        [dataset, dataset_id] = getDataset(dataset_id);
    end
    if ~isstruct(dataset); return; end
    datasetInfo(dataset);
    
    % defaults
    if ~exist('cameras','var')
        cameras = {};
    end
    if ~exist('scopes','var')
        scopes = {};
    end
    if ~exist('fignum','var')
        fignum = 2;
    end
    
    % skip if no requested plots
    if numel(scopes) + numel(cameras) == 0
        return;
    end
    
    % prepare figure
    figure(fignum);
    clf(fignum);
    set(gcf, 'color', 'w');
    colormap(mod_jet());
    
    % make subplot setup
    ncams = numel(cameras);
    nscopes = numel(scopes);
    nplots = ncams + nscopes;
    
    % find maximum number of shots
    numShots_max = 0;
    for j = 1:ncams
        numShots_max = max(numShots_max, numel(dataset.images.(cameras{j}{1}).dat));
    end
    for j = 1:nscopes
        numShots_max = max(numShots_max, numel(dataset.scopes.(scopes{j}{1}).dat));
    end
    numShots = numShots_max;
    shots = 1:numShots;

    % make list of good shots
    goodShots = [];
    for i = 1:numShots
        shotIsOK = true;
        for j = 1:ncams
            try
                testImage = dataset.images.(cameras{j}{1}).dat{i};
                shotIsOK = ~isempty(testImage); % discard shot if not all images saved
            catch
                disp('missed camera shot!')
                shotIsOK = false;
            end
        end
        for j = 1:nscopes
            scope = dataset.scopes.(scopes{j}{1}).dat{i};
            shotIsOK = ~isnan(scope); % discard shot if not all scopes saved
        end
        if shotIsOK
            goodShots = [goodShots, i];
        end
    end
    numGoodShots = numel(goodShots);
    
    % cycle cameras and scopes
    for i = 1:nplots
        
        % make camera or scope structures
        if i <= ncams % camera
            name = cameras{i}{1};
            % get 2D to 1D function (mandatory)
            if numel(cameras{i}) >= 2
                fun = cameras{i}{2};
            else
                error('Cameras need a projection function! e.g. {''NAME'',@(x)mean(x,1)}');
            end
            % get signal structure
            signalStruct = dataset.images.(name);
            signal_title = [signalStruct.desc ' : ' func2str(fun)];
            
            % define color axis
            if numel(cameras{i}) >= 3
                cax = cameras{i}{3};
            else 
                cax = [0 0];% default autoscale
            end
            clabel = 'Counts';
            
            % define roi
            if numel(cameras{i}) >= 4
                yroi = cameras{i}{4}{1};
                if isempty(yroi); yroi = []; end
                if cameras{i}{4}
                    xroi = cameras{i}{3}{2};
                    if isempty(xroi); xroi = []; end
                end
            else
                yroi = [];
                xroi = [];
            end
            ylbl = 'Projection (px)';
            testLine = fun(signalStruct.dat{1});
            
            % set y axis
            yaxis = 1:numel(testLine);
            
        else % scope
            i_eff = i - ncams;
            name = scopes{i_eff}{1};
            
            % get signal struct
            signalStruct = dataset.scopes.(name);
            
            % define color axis
            if numel(scopes{i_eff}) >= 2
                cax = scopes{i_eff}{2};
                if isempty(cax) || numel(cax)~=2
                    cax = [0 0];% default autoscale
                end
            else 
                cax = [0 0];% default autoscale
            end
            
            % optional function;
            if numel(scopes{i_eff}) >= 3
                fun = scopes{i_eff}{3};
                signal_title = [signalStruct.desc ' : ' func2str(fun)];
            else
                fun = @(x) x; % dummy function (as scopes are already 1D)
                signal_title = signalStruct.desc;
            end
            
            % scope color axes
            if isfield(signalStruct, 'unit')
                cunit = signalStruct.unit;
            else
                cunit = 'a.u.';
            end
            clabel = [signalStruct.desc ' (' cunit ')'];
            
            % scope time axis
            if isfield(signalStruct, 'time_ns')
                ts = signalStruct.time_ns{i};
                timeUnit = 'ns';
            else
                ts = 0:(numel(signal)-1);
                timeUnit = 'a.u.';
            end
            yaxis = ts;
            ylbl = ['Time (' timeUnit ')'];
            
        end
        
        % test function and prepare waterfall image
        wfall_img = zeros(1, numGoodShots);
        for j = 1:numShots
            testSignal = signalStruct.dat{j};
            %{
            % define ROI
            if numel(cameras{i}) >= 4
                yroi = cameras{i}{4}{1};
                if isempty(yroi) yroi = 1:size(testImage, 1); end
                if cameras{i}{4}
                    xroi = cameras{i}{3}{2};
                    if isempty(xroi) xroi = 1:size(testImage, 2); end
                end
            end
            testImage = testImage(yroi, xroi); % apply ROI
            yaxis = yroi;
            %}
            if numel(testSignal) <= 1
                continue;
            else
                wfall_img = zeros(numel(fun(testSignal)), numGoodShots);
                break;
            end
        end
        
        % cycle shots
        for j = 1:numGoodShots
            signal = signalStruct.dat{goodShots(j)};
            if numel(signal) <= 1
                wfall_img(:,j) = nan(size(wfall_img, 1),1);
            else
                % transpose if wrong way
                if size(signal,1) == 1
                    signal = signal';
                end
                wfall_img(:,j) = fun(signal);
            end
        end
        
        % plot
        sub_wf = subplot(nplots+dataset.metadata.isScan, 1, i);
        imagesc(1:numGoodShots, yaxis, wfall_img);
        if dataset.metadata.isScan % if a scan
            % add x label
            xlabel([dataset.metadata.scanName ' (' dataset.scalars.step_value.unit ')']);
            
            % add black lines between steps
            goodStepnums = dataset.scalars.step_num.dat(goodShots);
            uniq_stepnums = unique(goodStepnums);
            numSteps = numel(uniq_stepnums);
            for j = 1:numSteps
                
                % 1D step lines
                if j > 1
                    linepos = find(goodStepnums==j, 1, 'first');
                    %disp(linepos)
                    ln1D = line((linepos-0.5)*[1 1], get(gca, 'ylim'), 'Color', 'k');
                end
                
                % 2D step lines
                if dataset.metadata.isScan2D
                    goodStepnums2 = dataset.scalars.step_num2.dat(goodShots);
                    uniq_stepnums2 = unique(goodStepnums2);
                    numSteps2 = numel(uniq_stepnums2);
                    for k = 2:numSteps2
                        % 1D step lines
                        linepos = find(goodStepnums==j & goodStepnums2==k, 1, 'first');
                        line((linepos-0.5)*[1 1], get(gca, 'ylim'), 'Color', 'k');
                    end
                    %linepos = find(goodStepnums==j, 1, 'first');
                    %ln1D = line((linepos-0.5)*[1 1], get(gca, 'ylim'), 'Color', 'k');
                    %ln1D.LineWidth = 1.5;
                else
                    if j > 1
                        linepos = find(goodStepnums==j, 1, 'first');
                        %disp(linepos)
                        line((linepos-0.5)*[1 1], get(gca, 'ylim'), 'Color', 'k');
                    end
                end
            end
                        
            % make tickmarks for the steps
            goodStepvals = dataset.scalars.step_value.dat(goodShots);
            uniq_stepvals = unique(goodStepvals);
            [ticks, ticklabels] = deal(zeros(size(uniq_stepvals)));
            for j = 1:numSteps
                stepval = uniq_stepvals(j);
                shots1 = 1:numGoodShots;
                stepshots = shots1(goodStepvals==stepval);
                ticks(j) = median(stepshots);
                ticklabels(j) = stepval;
            end
            % reduce number of ticks if more than a threshold
            maxTicks = 12;
            while numel(ticks) > maxTicks
                ticks = ticks(1:2:end);
                ticklabels = ticklabels(1:2:end);
            end
            % Sort the ticks in increasing order in case we did the scan
            % backwards
            if ticks(1) > ticks(end)
                ticks = ticks(end:-1:1);
                ticklabels = ticklabels(end:-1:1);
            end
                
            set(gca, 'xtick', ticks, 'xticklabel', ticklabels)
            
            overview = ['(' num2str(numSteps) ' steps, ' num2str(numShots) ' shots, dataset ' esc_uscore(dataset_id) ')'];
        else
            imagesc(shots, yaxis, wfall_img);
            xlabel('Shots');
            overview = ['(' num2str(numShots) ' shots, dataset ' esc_uscore(dataset_id) ')'];
        end
        
        % set colorbar and axis
        cb = colorbar;
        caxis(cax);
        ylabel(cb, clabel);
        
        ylabel(ylbl);
        if i == 1 
            % also add dataset info on first waterfall
            title({['\bf' signal_title], ['\rm' overview]});
        else
            title(signal_title)
        end
        
    end
    
    % add scan step value plot
    if dataset.metadata.isScan
        
        % make two copies of each shot and step in order to have a sharp
        %   ladder in the plot (and not slanted lines between steps)
        shots_dual = reshape([goodShots; goodShots]-0.5, 1, 2*numel(goodShots));
        shots_dual = shots_dual(2:end);
        shots_dual = [shots_dual, shots_dual(end)+1];
        
        % step value 1
        goodStepvals = dataset.scalars.step_value.dat(goodShots);
        stepvals_dual = reshape([goodStepvals; goodStepvals], 1, 2*numel(goodStepvals));
        stepvals_dual = stepvals_dual(1:(end-1));
        stepvals_dual = [stepvals_dual, stepvals_dual(end)];
        
        % determine limits
        ypad = 0.05;
        ymin = min(goodStepvals);
        ymax = max(goodStepvals);
        ylimits = [ymin - (ymax-ymin)*ypad, ymax + (ymax-ymin)*ypad];
        xlimits = [min(shots), max(shots)];
        % plot
        sub_steps = subplot(nplots+1, 1, nplots+1);
        if ~dataset.metadata.isScan2D
            % if 1D scan
            
            % plot
            plot(shots_dual, stepvals_dual);
            title('Scan steps');
            ylabel(['Step value (' dataset.metadata.stepUnit ')']);
            ylim(ylimits);
            xlim(xlimits);
            
        else
            % if 2D scan
            
            % step value 2
            goodStepvals2 = dataset.scalars.step_value2.dat(goodShots);
            stepvals2_dual = reshape([goodStepvals2; goodStepvals2], 1, 2*numel(goodStepvals2));
            stepvals2_dual = stepvals2_dual(1:(end-1));
            stepvals2_dual = [stepvals2_dual, stepvals2_dual(end)];
            
            % determine limits 2
            ymin2 = min(goodStepvals2);
            ymax2 = max(goodStepvals2);
            ylimits2 = [ymin2 - (ymax2-ymin2)*ypad, ymax2 + (ymax2-ymin2)*ypad];
            
            % plot
            [hAx, hl1, hl2] = plotyy(shots_dual, stepvals_dual, shots_dual, stepvals2_dual);
            title('2D scan steps');
            ylim(hAx(1), ylimits);
            ylim(hAx(2), ylimits2);
            xlim(hAx(1), xlimits);
            xlim(hAx(2), xlimits);
            hl1.LineStyle = '-';
            hl2.LineStyle = '--';
            ylabel(hAx(1), [dataset.metadata.scanName ' (' dataset.metadata.stepUnit ')']);
            ylabel(hAx(2), [dataset.metadata.scanName2 ' (' dataset.metadata.stepUnit2 ')']);
            
        end
        
        % common between 1D and 2D plots
        xlim([min(shots), max(shots)]);
        xlabel('Shots');

        % set the width of this plot equal to 
        pos_steps = get(sub_steps, 'Position');
        pos_waterfalls = get(sub_wf, 'Position');
        set(gca, 'Position', [pos_steps(1:2) pos_waterfalls(3) pos_steps(4)]);
        
    end
end

