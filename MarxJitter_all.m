clear;

firstDataset = 10321;
lastDataset  = 10442;

isArgon = firstDataset:10409;
isHelium = 10410:lastDataset;
isTimingScan = [10335,10362,10364,10365,10388,10389,10405,10417,10421];

dayTues = firstDataset:10342;
dayWed  = 10343:10348;
dayThur = 10349:10409;
dayFri  = 10410:lastDataset;

plotFolder = 'marxBankJitterPlots';
%Attempt to create folder for plots
mkdir(plotFolder)

%For quick debug run
%firstDataset = 10420;
%lastDataset = 10422;

num_shots_total = 0;

csvfile = fopen([plotFolder,'/', 'MarxJitter_all.csv'], 'w');
fprintf(csvfile, ...
    ['"dataset", "num_shots", "num_shots_total", ' ...
    '"var_thresh_in", "var_thresh_out", "var_cumsum_in", "var_cumsum_out", ' ...
    '"mean_max_in", "mean_max_out", "mean_max_in_smooth", "mean_max_out_smooth", ' ...
    '"datetime", "argon", "helium", "timingscan", "day", ' ...
    '"comment"\n'] ...
    );

average_var_thresh_in = zeros(4,1);
average_var_thresh_out = zeros(4,1);
average_var_cumsum_in = zeros(4,1);
average_var_cumsum_out = zeros(4,1);
average_mean_max_in = zeros(4,1);
average_mean_max_out = zeros(4,1);
average_mean_max_in_smooth = zeros(4,1);
average_mean_max_out_smooth = zeros(4,1);

num_shots_day = zeros(4,1);
num_shots_day_all = zeros(4,1); %Also including timing scans

hist_var_thresh_in = cell(4,1);
hist_var_thresh_out = cell(4,1);
hist_var_cumsum_in = cell(4,1);
hist_var_cumsum_out = cell(4,1);
hist_mean_max_in = cell(4,1);
hist_mean_max_out = cell(4,1);
hist_mean_max_in_smooth = cell(4,1);
hist_mean_max_out_smooth = cell(4,1);

hist_thresh_in = cell(4,1);
hist_thresh_out = cell(4,1);
hist_cumsum_in = cell(4,1);
hist_cumsum_out = cell(4,1);

hist_max_in  = cell(4,1);
hist_max_out = cell(4,1);
hist_max_in_smooth  = cell(4,1);
hist_max_out_smooth = cell(4,1);

for i = firstDataset:lastDataset
    [num_shots, var_thresh_in, var_thresh_out, var_cumsum_in, var_cumsum_out, ...
        mean_max_in, mean_max_out,  mean_max_in_smooth, mean_max_out_smooth, ...
        thresh_in_array, thresh_out_array, cumsum_in_array, cumsum_out_array, ...
        max_in_array, max_out_array, max_in_smooth_array, max_out_smooth_array, ...
        dataset] ...
        = marxBankJitter(i, true);
    
    num_shots_total = num_shots_total + num_shots;
    
    if ismember(i,isArgon)
        argon = 1;
    else
        argon = 0;
    end
    if ismember(i,isHelium)
        helium = 1;
    else
        helium = 0;
    end
    
    if ismember(i,isTimingScan)
        timingscan = 1;
    else
        timingscan = 0;
    end
    
    if ismember(i,dayTues)
        day = 1;
    elseif ismember(i,dayWed)
        day = 2;
    elseif ismember(i,dayThur)
        day = 3;
    elseif ismember(i,dayFri)
        day = 4;
    else
        day = 0;
    end
    
    if ~isstruct(dataset) 
        comment = '-- Dataset not found --';
        date    = char(datetime(1970,1,1));
        
        var_thresh_in = NaN;
        var_thresh_out = NaN;
        var_cumsum_in = NaN;
        var_cumsum_out = NaN;
        mean_max_in = NaN;
        mean_max_out = NaN;
        mean_max_in_smooth = NaN;
        mean_max_out_smooth = NaN;
        
    else
        comment = dataset.metadata.comment;
        %comment = ''; % Often too long to display well
        date = char(dataset.metadata.endTime);
    
    end
    
    goodData = 1;
    if isnan(var_thresh_in) || isnan(var_thresh_out)
       goodData = 0; 
    end
    
    fprintf(csvfile,'%i, %i, %i, %d, %d, %d, %d, %d, %d, %d, %d, "%s", %i, %i, %i, %i, "%s"\n', ...
        i, num_shots, num_shots_total, var_thresh_in, var_thresh_out, var_cumsum_in, var_cumsum_out, ...
        mean_max_in, mean_max_out,  mean_max_in_smooth, mean_max_out_smooth, ...
        date, argon, helium, timingscan, day, comment);
    fseek(csvfile, 0, 'eof'); %Force a flushing of csvfile
    
    if goodData
        num_shots_day_all(day) = num_shots_day_all(day) + num_shots;

        if ~timingscan
            average_var_thresh_in(day)       = average_var_thresh_in(day) + var_thresh_in;
            average_var_thresh_out(day)      = average_var_thresh_out(day) + var_thresh_out;
            average_var_cumsum_in(day)       = average_var_cumsum_in(day) + var_cumsum_in;
            average_var_cumsum_out(day)      = average_var_cumsum_out(day) + var_cumsum_out;
            
            hist_var_thresh_in{day}  = [hist_var_thresh_in{day}, var_thresh_in];
            hist_var_thresh_out{day} = [hist_var_thresh_out{day}, var_thresh_out];
            hist_var_cumsum_in{day}  = [hist_var_cumsum_in{day}, var_cumsum_in];
            hist_var_cumsum_out{day} = [hist_var_cumsum_out{day}, var_cumsum_out];
        
            num_shots_day(day) = num_shots_day(day) + num_shots;
        end
        average_mean_max_in(day)         = average_mean_max_in(day) + mean_max_in;
        average_mean_max_out(day)        = average_mean_max_out(day) + mean_max_out;
        average_mean_max_in_smooth(day)  = average_mean_max_in_smooth(day) + mean_max_in_smooth;
        average_mean_max_out_smooth(day) = average_mean_max_out_smooth(day) + mean_max_out_smooth;
        
        hist_mean_max_in{day}    = [hist_mean_max_in{day}, mean_max_in];
        hist_mean_max_out{day}   = [hist_mean_max_out{day}, mean_max_out];
        hist_mean_max_in_smooth{day}    = [hist_mean_max_in_smooth{day}, mean_max_in_smooth];
        hist_mean_max_out_smooth{day}   = [hist_mean_max_out_smooth{day}, mean_max_out_smooth];
        
        hist_thresh_in{day} = [hist_thresh_in{day}, thresh_in_array'];
        hist_thresh_out{day} = [hist_thresh_out{day}, thresh_out_array'];
        hist_cumsum_in{day} = [hist_cumsum_in{day}, cumsum_in_array'];
        hist_cumsum_out{day} = [hist_cumsum_out{day}, cumsum_out_array'];
        
        hist_max_in{day} = [hist_max_in{day}, max_in_array'];
        hist_max_out{day} = [hist_max_out{day}, max_out_array'];
        hist_max_in_smooth{day} = [hist_max_in_smooth{day}, max_in_smooth_array'];
        hist_max_out_smooth{day} = [hist_max_out_smooth{day}, max_out_smooth_array'];
    end
end

fclose(csvfile);

for day=1:4
    average_var_thresh_in(day)       = average_var_thresh_in(day)       / num_shots_day(day);
    average_var_thresh_out(day)      = average_var_thresh_out(day)      / num_shots_day(day);
    average_var_cumsum_in(day)       = average_var_cumsum_in(day)       / num_shots_day(day);
    average_var_cumsum_out(day)      = average_var_cumsum_out(day)      / num_shots_day(day);
    average_mean_max_in(day)         = average_mean_max_in(day)         / num_shots_day_all(day);
    average_mean_max_out(day)        = average_mean_max_out(day)        / num_shots_day_all(day);
    average_mean_max_in_smooth(day)  = average_mean_max_in_smooth(day)  / num_shots_day_all(day);
    average_mean_max_out_smooth(day) = average_mean_max_out_smooth(day) / num_shots_day_all(day);
end

cmap = colormap('lines');
nbins = 20;

fig20=figure(20);
clf(20)
binedges = 0:1:40;
for i = 1:4
    ax1=subplot(4,1,1);
    histogram(sqrt(hist_var_thresh_in{i}), binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax2=subplot(4,1,2);
    histogram(sqrt(hist_var_thresh_out{i}), binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax3=subplot(4,1,3);
    histogram(sqrt(hist_var_cumsum_in{i}), binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax4=subplot(4,1,4);
    histogram(sqrt(hist_var_cumsum_out{i}), binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
end
linkaxes([ax1,ax2,ax3,ax4],'x')
xlabel('Trigger time sqrt(variance) per run [ns]')
legend(ax1, {'Tue','Wed','Thur','Fri'})
title(ax1, 'Threshold (I_{in})')
title(ax2, 'Threshold (I_{out})')
title(ax3, 'Cumsum 50% (I_{in})')
title(ax4, 'Cumsum 50% (I_{out})')
saveas(fig20,[plotFolder, '/', 'TriggerTimeVariance', '.png']);

fig21=figure(21);
clf(21)
binedges = 420:5:510;
for i = 1:4
    ax1=subplot(4,1,1);
    histogram(hist_mean_max_in{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax2=subplot(4,1,2);
    histogram(hist_mean_max_out{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax3=subplot(4,1,3);
    histogram(hist_mean_max_in_smooth{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax4=subplot(4,1,4);
    histogram(hist_mean_max_out_smooth{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
end
linkaxes([ax1,ax2,ax3,ax4],'x')
xlabel('Dataset average peak current [A]')
legend(ax1, {'Tue','Wed','Thur','Fri'})
title(ax1, 'I_{in}')
title(ax2, 'I_{out}')
title(ax3, 'Smoothed I_{in}')
title(ax4, 'Smoothed I_{out}')
saveas(fig21,[plotFolder, '/', 'PeakCurrent_DatsetAverage', '.png']);

fig22=figure(22);
clf(22)
ax1=subplot(4,1,1);
histogram([hist_mean_max_in{1},hist_mean_max_in{2},hist_mean_max_in{3},hist_mean_max_in{4}], binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    
ax2=subplot(4,1,2);
histogram([hist_mean_max_out{1},hist_mean_max_out{2},hist_mean_max_out{3},hist_mean_max_out{4}], binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
   
ax3=subplot(4,1,3);
histogram([hist_mean_max_in_smooth{1},hist_mean_max_in_smooth{2},hist_mean_max_in_smooth{3},hist_mean_max_in_smooth{4}], binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    
ax4=subplot(4,1,4);
histogram([hist_mean_max_out_smooth{1},hist_mean_max_out_smooth{2},hist_mean_max_out_smooth{3},hist_mean_max_out_smooth{4}], nbins, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')

linkaxes([ax1,ax2,ax3,ax4],'x')
xlabel('Dataset average peak current [A]')
title(ax1, 'I_{in}')
title(ax2, 'I_{out}')
title(ax3, 'Smoothed I_{in}')
title(ax4, 'Smoothed I_{out}')
saveas(fig22,[plotFolder, '/', 'PeakCurrent_DatasetAverage_combined', '.png']);

% Pointless, since the intended time is often different...
% figure(23)
% clf(23)
% for i = 1:4
%     ax1=subplot(4,1,1);
%     histogram(hist_thresh_in{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
%     hold on;
%     
%     ax2=subplot(4,1,2);
%     histogram(hist_thresh_out{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
%     hold on;
%     
%     ax3=subplot(4,1,3);
%     histogram(hist_cumsum_in{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
%     hold on;
%     
%     ax4=subplot(4,1,4);
%     histogram(hist_cumsum_out{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
%     hold on;
% end
% linkaxes([ax1,ax2,ax3,ax4],'x')
% xlabel('Trigger time [ns]')
% legend(ax1, {'Tue','Wed','Thur','Fri'})
% title(ax1, 'I_{in}')
% title(ax2, 'I_{out}')
% title(ax3, 'Smoothed I_{in}')
% title(ax4, 'Smoothed I_{out}')

fig24 = figure(24);
clf(24)
binedges = 420:5:550;
for i = 1:4
    ax1=subplot(4,1,1);
    histogram(hist_max_in{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax2=subplot(4,1,2);
    histogram(hist_max_out{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax3=subplot(4,1,3);
    histogram(hist_max_in_smooth{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
    
    ax4=subplot(4,1,4);
    histogram(hist_max_out_smooth{i}, binedges, 'facecolor',cmap(i,:),'facealpha',.5,'edgecolor','none')
    hold on;
end
linkaxes([ax1,ax2,ax3,ax4],'x')
xlabel('Peak current [A]')
legend(ax1, {'Tue','Wed','Thur','Fri'})
title(ax1, 'I_{in}')
title(ax2, 'I_{out}')
title(ax3, 'Smoothed I_{in}')
title(ax4, 'Smoothed I_{out}')
saveas(fig24,[plotFolder, '/', 'PeakCurrent_all', '.png']);


beep
pause(0.1)
beep
pause(0.1)
beep