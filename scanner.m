function [] = scanner(dataset_id, cameras, scopes)
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset); return; end
    datasetInfo(dataset);
    
    % determine type of scan
    if ~dataset.metadata.isScan
        disp('Not a scan!');
        return;
    end
    
    % defaults
    if ~exist('cameras','var')
        cameras = {};
    end
    if ~exist('scopes','var')
        scopes = {};
    end
    
    % skip if no requested plots
    if numel(scopes) + numel(cameras) == 0
        return;
    end
    
    % shots and steps
    numShots = dataset.metadata.numShots;
    numSteps = dataset.metadata.numSteps;
    step_values = dataset.scalars.step_value.dat;
    uniq_steps = unique(step_values);
    
    % prepare figure
    figure(3);
    clf(3);
    set(gcf, 'color', 'w');
    
    % make subplot setup
    ncams = numel(cameras);
    nscopes = numel(scopes);
    nplots = ncams + nscopes;
    nrows = ceil(sqrt(nplots));
    ncols = ceil(nplots/nrows);
    
    % cycle cameras and scopes
    for i = 1:nplots
        
        % make camera or scope structures
        if i <= ncams % camera
            name = cameras{i}{1};
            fun = cameras{i}{2};
            signalStruct = dataset.images.(name);
        else % scope
            i_eff = i - ncams;
            name = scopes{i_eff}{1};
            fun = scopes{i_eff}{2};
            signalStruct = dataset.scopes.(name);
        end
        desc = signalStruct.desc;
        
        % cycle shots
        values = zeros(1,numShots);
        for j = 1:numShots
            signal = signalStruct.dat{j};
            try
                values(j) = fun(signal);
            catch
            end
        end
        
        % do statistics
        [values_mean, values_std] = deal(zeros(1,numel(uniq_steps)));
        for j = 1:numel(uniq_steps)
            stepmask = (step_values==uniq_steps(j));
            vs = values(stepmask);
            if sum(stepmask)>8
                vs = vs(vs > prctile(vs, 25) &  vs < prctile(vs, 75));
                %legnd = 'Percentile 25-75';
            elseif sum(stepmask)>4
                vs = vs(vs > prctile(vs, 10) &  vs < prctile(vs, 90));
                %legnd = 'Percentile 10-90';
            else
                %legnd = NaN;
            end
            legnd = NaN;
            values_mean(j) = mean(vs);
            values_std(j) = std(vs);
        end
        
        % plot
        subplot(nrows, ncols, i);
        
        errorbar(uniq_steps, values_mean, values_std);
        if numel(dataset.metadata.stepUnit) > 0
            xlabel([dataset.metadata.scanName ' (' dataset.metadata.stepUnit ')']);
        else
            xlabel(dataset.metadata.scanName);
        end
        ylabel(func2str(fun));
        title({['\bf' esc_uscore(desc)], ...
               ['\rm (' num2str(numSteps) ' steps, ' num2str(numShots) ' shots, dataset ' esc_uscore(dataset_id) ')']});
        if numel(uniq_steps)>1
            xmin = min(uniq_steps);
            xmax = max(uniq_steps);
            xlim([xmin - (xmax-xmin)/numSteps*0.5, xmax + (xmax-xmin)/numSteps*0.5]);
        end
        if ~isnan(legnd)
            legend(legnd);
        end
    end
    
end

