clear all
%close all

% add utilities
path = strrep(mfilename('fullpath'), mfilename, '');
addpath([path 'utilities']);

%Virgin
%offsets_sh = [-207.0175 -157.0175 -132.0175 -107.0175  -82.0175  -57.0175  -32.0175   -7.0175   17.9825   42.9825   67.9825   92.9825  117.9825  142.9825, 167.9825  192.9825  217.9825];
%With some extra points
offsets_sh = [-300,-275,-274,-273,-272,-271,-270,-260,-250,-207.0175 -157.0175 -132.0175 -107.0175  -82.0175  -57.0175  -32.0175   -7.0175   17.9825   42.9825   67.9825   92.9825  117.9825  142.9825, 167.9825  192.9825  217.9825,250,260,270,271,272,273,274,275,300];

%beamAngle = -1.343100; %mrad
beamAngle = 2.942144; %mrad

figure(1)
clf;
off1 = tracking_thick_JT(0.1702, 271.5000, offsets_sh, beamAngle, 195, 1.2941e+03, 0.015, 0.282, true, 271, 271, 1.6777e3);
ylim([-6,6])

figure(2)
clf()
off2 = tracking_thick_JT(500, 271.5000, offsets_sh, beamAngle, 195, 1.2941e+03, 0.015, 0.282, true, 271, 271, 1.6777e3);
ylim([-6,6])

figure(3)
clf()
plot(offsets_sh,off1*1e-3,'-x')
hold on
plot(offsets_sh,off2*1e-3,'-+')
grid on

%How the plots are actually done
figure(4)
clf()
plot(offsets_sh,-(off1-offsets_sh)*1e-3,'-x')
hold on
plot(offsets_sh,-(off2-offsets_sh)*1e-3,'-+')
grid on

