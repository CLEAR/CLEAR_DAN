function [] = sendFigureToElog( dataset_id, figNum, comment )

    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    elogEvent = dataset.metadata.elogEvent;
    
    % save figure temporarily
    figure(figNum);
    filename = [path 'utilities/dataset_' dataset_id '_' datestr(now,'yyyymmdd_HHMMSS') '.png'];
    print(filename, '-dpng');
    
    % send file to the elog, with comment
    if exist('comment', 'var')
        elogEvent.addNewAttachment(filename, comment);
    else
        elogEvent.addNewAttachment(filename);
    end
    
    % delete the temporary file
    delete(filename);
    
end

