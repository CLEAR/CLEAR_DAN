function [] = scanner2D(dataset_id, cameras, scopes, plotIn3Ddefault)
    
   % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);

    % load the dataset
    [dataset, dataset_id] = getDataset(dataset_id);
    if ~isstruct(dataset); return; end
    datasetInfo(dataset);
    
    % determine type of scan
    if ~dataset.metadata.isScan2D
        disp('Not a 2D scan!');
        return;
    end
    
    % defaults
    if ~exist('plotIn3Ddefault', 'var')
        plotIn3Ddefault = false;
    end
    if ~exist('cameras', 'var')
        cameras = {};
    end
    if ~exist('scopes', 'var')
        scopes = {};
    end
    
    % skip if no requested plots
    if numel(scopes) + numel(cameras) == 0
        return;
    end
    
    % shots and steps
    numShots = dataset.metadata.numShots;
    
    numSteps1 = dataset.metadata.numSteps;
    step_values1 = dataset.scalars.step_value.dat;
    uniq_steps1 = unique(step_values1);
    
    numSteps2 = dataset.metadata.numSteps2;
    step_values2 = dataset.scalars.step_value2.dat;
    uniq_steps2 = unique(step_values2);
    
    % prepare figure
    figure(5);
    clf(5);
    set(gcf, 'color', 'w');
    colormap(mod_jet());
    set(gcf, 'Renderer', 'Painters');
    
    % make subplot setup
    ncams = numel(cameras);
    nscopes = numel(scopes);
    nplots = ncams + nscopes;
    nrows = ceil(sqrt(nplots));
    ncols = ceil(nplots/nrows);
    
    % cycle cameras and scopes
    for i = 1:nplots
        
        % make camera or scope structures
        if i <= ncams % camera
            name = cameras{i}{1};
            fun = cameras{i}{2};
            if numel(cameras{i}) > 2
                plotIn3D = cameras{i}{3};
            else
                plotIn3D = plotIn3Ddefault;
            end
            signalStruct = dataset.images.(name);
        else % scope
            i_eff = i - ncams;
            name = scopes{i_eff}{1};
            fun = scopes{i_eff}{2};
            if numel(scopes{i_eff}) > 2
                plotIn3D = scopes{i_eff}{3};
            else
                plotIn3D = plotIn3Ddefault;
            end
            signalStruct = dataset.scopes.(name);
        end
        
        % cycle shots
        values = zeros(1,numShots);
        for j = 1:numShots
            signal = signalStruct.dat{j};
            values(j) = fun(signal);
        end
        
        % do statistics
        [values_mean, values_std] = deal(zeros(numel(uniq_steps1), numel(uniq_steps2)));
        for j = 1:numel(uniq_steps1)
            for k = 1:numel(uniq_steps2)
                stepmask2D = (step_values1==uniq_steps1(j) & step_values2==uniq_steps2(k));
                vs = values(stepmask2D);
                if sum(stepmask2D)>10
                vs = vs(vs > prctile(vs, 25) &  vs < prctile(vs, 75));
                    legnd = 'From 25th to 75th percentile';
                elseif sum(stepmask2D)>4
                    vs = vs(vs > prctile(vs, 10) &  vs < prctile(vs, 90));
                    legnd = 'From 10th to 90th percentile';
                else
                    legnd = NaN;
                end
                values_mean(j, k) = mean(vs);
                values_std(j, k) = std(vs);
            end
        end
        
        % plot
        subplot(nrows, ncols, i);
        if plotIn3D
            surf(uniq_steps1, uniq_steps2, values_mean');
        else
            imagesc(uniq_steps2, uniq_steps1, values_mean);
            set(gca, 'ydir', 'normal');
            %h = pcolor(uniq_steps2, uniq_steps1, values_mean);
            %set(h, 'EdgeColor', 'none');
        end
        if numel(dataset.metadata.stepUnit2) > 0
            xlabel([dataset.metadata.scanName2 ' (' dataset.metadata.stepUnit2 ')']);
        else
            xlabel(dataset.metadata.scanName2);
        end
        if numel(dataset.metadata.stepUnit) > 0
            ylabel([dataset.metadata.scanName ' (' dataset.metadata.stepUnit ')']);
        else
            ylabel(dataset.metadata.scanName);
        end
        axis tight;
        cb = colorbar;
        ylabel(cb, func2str(fun));
        title({['\bf' esc_uscore(signalStruct.desc)], ...
               ['\rm (' num2str(numSteps1) ' x ' num2str(numSteps2) ' steps, ' num2str(numShots) ' shots, dataset ' esc_uscore(dataset_id) ')']});
    end
    
end

